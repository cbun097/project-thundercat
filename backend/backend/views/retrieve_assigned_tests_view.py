from rest_framework.response import Response
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.serializers.assigned_test_serializer import AssignedTestSerializer
from backend.views.utils import is_undefined


def retrieve_assigned_tests(username):
    query_list = AssignedTest.objects.filter(
        username=username,
        status__in=[AssignedTestStatus.ASSIGNED, AssignedTestStatus.ACTIVE],
    )

    data = AssignedTestSerializer(query_list, many=True)
    return Response(data.data)


# function to assign any test based on the test ID, the username and the scheduled date
def assign_test(request):
    test_id = request.query_params.get("test_id", None)
    username_id = request.query_params.get("username_id", None)
    scheduled_date = request.query_params.get("scheduled_date", None)
    # handling missing parameters
    if is_undefined(test_id):
        return {"error": "no 'test_id' parameter"}
    if is_undefined(username_id):
        return {"error": "no 'username_id' parameter"}
    if is_undefined(scheduled_date):
        return {"error": "no 'scheduled_date' parameter"}
    # setting status to 4 + associating the other parameters
    response = AssignedTest(
        status=4,
        scheduled_date=scheduled_date,
        test_id=test_id,
        username_id=username_id,
    )
    response.save()


def assigned_tests_historical(username, test_id):
    return Response(
        AssignedTest.objects.filter(username=username, test_id=test_id)
        .exclude(status__in=[AssignedTestStatus.ASSIGNED])
        .values()
    )

import LocalizedStrings from "react-localization";

let LOCALIZE = new LocalizedStrings({
  en: {
    //Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Home",
      homeTabTitleAuthenticated: "Home",
      dashboardTabTitle: "Dashboard",
      sampleTest: "Sample e-MIB",
      sampleTests: "Sample Tests",
      statusTabTitle: "Status",
      psc: "Public Service Commission of Canada",
      canada: "Government of Canada",
      skipToMain: "Skip to main content",
      menu: "MENU"
    },

    //HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "CAT - Sample Tests",
      eMIBOverview: "e-MIB Overview",
      eMIB: "e-MIB Assessment",
      status: "CAT - System Status",
      home: "CAT - Home",
      homeWithError: "Error - CAT - Home",
      testAdministration: "CAT - Test Sessions"
    },

    //authentication
    authentication: {
      login: {
        title: "Login",
        content: {
          title: "Login",
          description:
            "An account is required to proceed further. To log in, enter your credentials below.",
          inputs: {
            emailTitle: "Email Address:",
            passwordTitle: "Password:"
          }
        },
        button: "Login",
        invalidCredentials: "Invalid credentials!",
        passwordFieldSelected: "Password field selected."
      },
      createAccount: {
        title: "Create an account",
        content: {
          title: "Create an account",
          description:
            "An account is required to proceed further. To create an account, fill out the following.",
          inputs: {
            valid: "valid",
            firstNameTitle: "First name:",
            firstNameError: "Must be a valid first name",
            lastNameTitle: "Last name:",
            lastNameError: "Must be a valid last name",
            dobDayTitle: "Date of birth: (DD / MM / ---Y):",
            dobError: "Please fill out all fields related to the date of birth",
            dobTooltip: "You will only need to provide the last digit for your year of birth.",
            emailTitle: "Email address:",
            emailError: "Must be a valid email address",
            priOrMilitaryNbrTitle: "PRI or Service number (if applicable):",
            priOrMilitaryNbrError: "Must be a valid PRI or Service number",
            passwordTitle: "Password:",
            passwordErrors: {
              description: "Your password must satisfy the following:",
              upperCase: "At least one uppercase letter",
              lowerCase: "At least one lowercase letter",
              digit: "At least one digit",
              specialCharacter: "At least one special character",
              length: "Minimum of 8 characters and maximum of 15"
            },
            passwordConfirmationTitle: "Confirm password:",
            passwordConfirmationError: "Your password confirmation must match the Password"
          }
        },
        privacyNotice:
          "I have read and agreed to how the Public Service Commission collects, uses and discloses personal information, as set out in the {0}.",
        privacyNoticeLink: "Privacy Notice",
        privacyNoticeError: "You must accept the privacy notice by clicking on the checkbox",
        button: "Create account",
        accountAlreadyExistsError: "An account is already associated to this email address",
        passwordTooCommonError: "This password is too common",
        passwordTooSimilarToUsernameError: "The password is too similar to the username",
        passwordTooSimilarToFirstNameError: "The password is too similar to the first name",
        passwordTooSimilarToLastNameError: "The password is too similar to the last name",
        passwordTooSimilarToEmailError: "The password is too similar to the email",
        privacyNoticeDialog: {
          title: "Privacy and Security",
          privacyNoticeStatement: "Privacy Notice Statement – e-MIB Pilot Study",
          privacyParagraph1:
            "The personal information, including test results and supervisor ratings, pertaining to this research project will be used by the Public Service Commission of Canada (PSC) for research, analysis and test development purposes.  It may be possible to use the results for future staffing related matters with your consent. All information is collected under the authority of sections 11, 30, 35 and 36 of the Public Service Employment Act in accordance with the Privacy Act. The PSC is committed to protecting the privacy rights of individuals.",
          publicServiceEmploymentActLink: "Public Service Employment Act",
          privacyActLink: "Privacy Act",
          privacyParagraph2:
            "“Personal information” is any information, recorded in any form, about an identifiable individual.",
          privacyCommissionerLink: "Privacy Commissioner of Canada",
          privacyParagraph3:
            "The use and collection of test results is defined in Personal Information Bank numbers PSC-PPU-025. Personal information related to research, analysis and test development is retained for 10 years and then destroyed. Anonymous test responses and demographic information are retained in electronic format indefinitely.",
          privacyParagraph4:
            "Participation is voluntary. However, should you chose not to provide your personal information, you will not be able to participate in this study.",
          privacyParagraph5:
            "Individuals have the right to access to their personal information and to request corrections where the individual believes there is an error or omission. Individuals may contact the PSC’s Access to Information and Privacy Office to request access to your personal information and to request corrections.",
          privacyParagraph6:
            "Personal information collected by the PSC is protected from disclosure to unauthorized persons and/or departments and agencies subject to the provisions of the Privacy Act. However personal information may be disclosed without your consent in certain specific circumstances, per section 8 of the Privacy Act. ",
          accessToInformationLink: "Access to Information and Privacy Protection Division",
          privacyParagraph7:
            "Individuals have the right to file a complaint with the Privacy Commissioner of Canada regarding the department’s handling of their personal information.",
          infoSourceChapterLink: "Info Source Chapter",
          reproductionTitle: "Unauthorized Reproduction or Disclosure of Test Content",
          reproductionWarning:
            "This test and its contents are designated Protected B. Reproduction or recording in any form of the content of this test is strictly forbidden, and all material related to the test, including rough notes, must remain with the test administrator following the administration of the test. Any unauthorized reproduction, recording and/or disclosure of test content is in contravention of the Government Security Policy and the use of such improperly obtained or transmitted information could be found to contravene the provisions of the Public Service Employment Act (PSEA). Parties involved in the disclosure of or improper use of protected test content may be the subject of an investigation under the PSEA, where a finding of fraud may be punishable on summary conviction or may be referred to the Royal Canadian Mounted Police.",
          cheatingTitle: "Cheating",
          cheatingWarning:
            "Please note that suspected cheating will in all cases be referred to the responsible manager and the Personnel Psychology Centre for action. Suspected cheating may result in the invalidation of test results and may be the subject of an investigation under the PSEA, where a finding of fraud may be punishable on summary conviction or may be referred to the Royal Canadian Mounted Police."
        }
      }
    },

    //Menu Items
    menu: {
      etta: "System Administration",
      ppc: "PPC Administration",
      ta: "Test Administration",
      checkIn: "Check-in",
      profile: "Profile",
      incidentReport: "Incident Report",
      Notifications: "Notifications",
      MyTests: "My Tests",
      ContactUs: "Contact Us",
      logout: "Logout"
    },

    //Token Expired Popup Box
    tokenExpired: {
      title: "Session Expired",
      description: "Due to inactivity your login session has expired, you will need to login again."
    },

    //Home Page
    homePage: {
      welcomeMsg: "Welcome to the Candidate Assessment Tool",
      description:
        "This website is used to assess candidates for positions in the federal public service. To access your tests, you must login below. If you do not have an account, you may register for one using your email address."
    },

    //Sample Tests Page
    sampleTestDashboard: {
      title: "Sample Tests",
      description:
        'Listed below are sample tests that are available on the system. Sample tests are not scored and answers are not submitted at the end of a test. Click on "View" to access the sample test.',
      table: {
        columnOne: "Name of test",
        columnTwo: "Action",
        viewButton: "View"
      }
    },

    //Dashboard Page
    dashboard: {
      title: "Welcome, {0} {1}.",
      description:
        "You are logged into your account. If you are scheduled to take a test, please wait for the test administrator's instructions.",
      table: {
        columnOne: "Name of test",
        columnTwo: "Action",
        viewButton: "View",
        noTests: "You have no assigned tests."
      }
    },

    //Checkin action
    candidateCheckIn: {
      button: "Check-in",
      loading: "Searching for active tests...",
      checkedInText: "You checked-in with Test Access code: ",
      popup: {
        title: "Enter your Test Access Code",
        description:
          'Please type the Test Access Code provided by the test administrator in the box below. Then select "Check-in" to access your test.',
        textLabel: "Test Access Code:",
        textLabelError: "Please enter a valid test access code"
      }
    },

    //TA Check-in Page
    testAdministration: {
      title: "Welcome, {0} {1}.",
      checkedOutDescriptionPart1:
        "You are logged into your account. If you are scheduled to administer a test, you will need to generate a Test Access Code for the test session. The Test Access Code will provide candidates with access to the test for the duration of the test session.",
      checkedOutDescriptionPart2:
        "Only generate the Test Access Code immediately prior to a test session.",
      checkedInDescriptionPart1:
        "You have successfully generated a Test Access Code. Validate the session information provided below. If it is correct, provide candidates with the Test Access Code and instruct them to Check-in for the test.",
      checkedInDescriptionPart2:
        "If any of the session information is incorrect, disable the Test Access Code now and generate a new one.",
      checkedInDescriptionPart3: "Test Access Code: {0}",
      checkedInDescriptionPart4:
        "As candidates Check-in, you will see their names in the list of Active Candidates below. After all candidates in attendance are Checked-in, disable the Test Access Code.",
      checkInButton: "Generate Test Access Code",
      checkOutButton: "Disable Test Access Code",
      popupBox: {
        title: "Enter session information to Check-in",
        description:
          "Provide a staffing process number, the test session language and the test to administer.",
        form: {
          firstElement: {
            title: "Staffing process number:"
          },
          secondElement: {
            title: "Test session language:",
            dropdown: {
              placeholder: "Select",
              option1: "English",
              option2: "French"
            },
            invalidError: "Please select a valid test session language"
          },
          thirdElement: {
            title: "Test to administer:",
            dropdown: {
              placeholder: "Select"
            },
            invalidError: "Please select a valid test to administer"
          }
        }
      },
      activeCandidatesTable: {
        title: "Active Candidates",
        column1: "Select",
        column2: "Candidate",
        column3: "Status",
        column4: "Timer",
        column5: "Actions"
      }
    },

    //Profile Page
    profile: {
      title: "Welcome, {0} {1}.",
      sideNavItems: {
        personalInfo: "Personal Info",
        password: "Password",
        preferences: "Preferences",
        permissions: "Permissions",
        profileMerge: "Profile Merge"
      },
      personalInfo: {
        title: "Your Contact Information",
        nameSection: {
          title: "Name:",
          firstName: "First Name",
          lastName: "Last Name"
        },
        emailAddressesSection: {
          title: "Email Addresses:",
          primary: "Primary",
          secondary: "Secondary"
        },
        dateOfBirth: {
          title: "Date of Birth:",
          yearField: "Year",
          yearFieldSelected: "Year field selected.",
          monthField: "Month",
          monthFieldSelected: "Month field selected.",
          dayField: "Day",
          dayFieldSelected: "Day field selected.",
          currentValue: "Current value is:"
        },
        priOrMilitaryNbr: "PRI or Service Number:",
        optionalField: " (Optional)"
      },
      password: {
        newPassword: {
          title: "Password",
          updatedDate: "Your password was last updated on: {0}",
          currentPassword: "Current Password:",
          newPassword: "New Password:",
          confirmPassword: "Confirm Password:"
        },
        passwordRecovery: {
          title: "Password Recovery",
          secretQuestion: "Secret Question:",
          secretAnswer: "Secret Answer:"
        }
      },
      preferences: {
        title: "Preferences",
        description: "Modify your preferences.",
        notifications: {
          title: "Notifications",
          checkBoxOne:
            "Always send an email to my primary email address when I receive a notification",
          checkBoxTwo:
            "Always send an email to my secondary email address when I receive a notification"
        },
        display: {
          title: "Display",
          checkBoxOne: "Allow others to see my profile picture",
          checkBoxTwo: "Hide tooltip icons"
        }
      }
    },

    //Status Page
    statusPage: {
      title: "CAT Status",
      logo: "Thunder CAT Logo",
      welcomeMsg:
        "Internal status page to quickly determine the status / health of the Candidate Assessment Tool.",
      versionMsg: "Candidate Assessment Tool Version: ",
      gitHubRepoBtn: "GitHub Repository",
      serviceStatusTable: {
        title: "Service Status",
        frontendDesc: "Front end application built and serving successfully",
        backendDesc: "Back end application completing API requests successfully",
        databaseDesc: "Database completing API requests sccessfully"
      },
      systemStatusTable: {
        title: "System Status",
        javaScript: "JavaScript",
        browsers: "IE 10+, Chrome, Firefox",
        screenResolution: "Screen resolution minimum of 1024 x 768"
      },
      additionalRequirements:
        "Additionally, the following requirements must be met to use this application in a test centre.",
      secureSockets: "Secure Socket Layer (SSL) encryption enabled",
      fullScreen: "Full-screen mode enabled",
      copyPaste: "Copy + paste functionality enabled",
      colorOptions: "IE Internet Options > Colours enabled to users",
      fontsEnabled: "IE Internet Options > Fonts enabled to users",
      accessibilityOptions: "IE Internet Options > Accessibility enabled to users"
    },

    // Settings Dialog
    settings: {
      systemSettings: "Display Settings Using IE Browser Tools",
      zoom: {
        title: "Zoom (+/-)",
        instructionsListItem1: "Select the View button at the top left bar in Internet Explorer.",
        instructionsListItem2: "Select Zoom.",
        instructionsListItem3:
          "You can select a predefined zoom level, or a custom level by selecting Custom and entering a zoom value.",
        instructionsListItem4:
          "Alternatively, you can hold down CTRL and the + / - keys on your keyboard to zoom in or out."
      },
      textSize: {
        title: "Text size",
        instructionsListItem1: "Select the View button at the top left bar in Internet Explorer.",
        instructionsListItem2: "Select Text size.",
        instructionsListItem3: "Choose to make text larger or smaller than the size on the screen.",
        instructionsListItem4:
          "Select the Tools button, and select General tab, and then, under Appearance, select Accessibility.",
        instructionsListItem5:
          "Select the Ignore font sizes specified on webpages on the check box.",
        instructionsListItem6: "Select OK, and then select OK again.",
        notChanged: "If the text size has not changed:"
      },
      fontStyle: {
        title: "Font style",
        instructionsListItem1: "Select the Tools button at the top left bar in Internet Explorer.",
        instructionsListItem2: "Select Internet options.",
        instructionsListItem3: "In the General tab, under Appearance, select Accessibility.",
        instructionsListItem4:
          "Select the Ignore font styles specified on webpages on the check box.",
        instructionsListItem5: "Select OK.",
        instructionsListItem6: "In the General tab, under Appearance, select Fonts.",
        instructionsListItem7: "Select the fonts you want to use.",
        instructionsListItem8: "Select OK, and then select OK again."
      },
      color: {
        title: "Text and background colour",
        instructionsListItem1: "Select the Tools button and select Internet options.",
        instructionsListItem2: "In the General tab, under Appearance, select Accessibility.",
        instructionsListItem3: "Select the Ignore colors specified on webpages on the check box.",
        instructionsListItem4: "Select OK.",
        instructionsListItem5: "In the General tab, under Appearance, select Colors.",
        instructionsListItem6: "Uncheck the Use Windows colors check box.",
        instructionsListItem7:
          "For each color that you want to change, select the color box, select a new color, and then select OK.",
        instructionsListItem8: "Select OK, and then select OK again."
      }
    },

    //eMIB Test
    emibTest: {
      //Home Page
      homePage: {
        testTitle: "Sample e-MIB",
        welcomeMsg: "Welcome to the eMIB Sample Test"
      },

      //HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Tips on taking the e-MIB",
          part1: {
            description:
              "The e-MIB presents you with situations that will give you the opportunity to demonstrate the Key Leadership Competencies. Here are some tips that will help you provide assessors with the information they need to evaluate your performance on the Key Leadership Competencies:",
            bullet1:
              "Answer all the questions asked in the emails you received. Also, note that there may be situations that span several emails and for which no specific questions are asked, but that you can respond to. This will ensure that you take advantage of all the opportunities provided to demonstrate the competencies.",
            bullet2:
              "Don’t hesitate to provide your recommendations where appropriate, even if they are only initial thoughts.  If needed, you can then note other information you would need to reach a final decision.",
            bullet3:
              "For situations in which you feel you need to speak with someone in person before you can make a decision, you should indicate what information you need and how this would affect your decision one way or the other.",
            bullet4:
              "Use only the information provided in the emails and the background information. Do not make any inferences based on the culture of your own organization. Avoid making assumptions that are not reasonably supported by either the background information or the emails.",
            bullet5:
              "The e-MIB is set within a specific industry in order to provide enough context for the situations presented. However, effective responses rely on the competency targeted and not on specific knowledge of the industry."
          },
          part2: {
            title: "Other important information",
            bullet1:
              "You will be assessed on the content of your emails, the information provided in your task list and the reasons listed for your actions. Information left on your notepad will not be evaluated.",
            bullet2:
              "You will not be assessed on how you write. No points will be deducted for spelling, grammar, punctuation or incomplete sentences. However, your writing will need to be clear enough to ensure that the assessors understand which situation you are responding to and your main points.",
            bullet3: "You can answer the emails in any order you want.",
            bullet4: "You are responsible for managing your own time."
          }
        },
        testInstructions: {
          title: "Test instructions",
          hiddenTabNameComplementary: 'Under "Instructions"',
          onlyOneTabAvailableForNowMsg:
            "Note that there is only one main tab available for now. As soon as you start the test, the other main tabs will become available.",
          para1:
            "When you start the test, first read the background information which describes your position and the fictitious organization you work for. We recommend you take approximately 10 minutes to read it. Then proceed to the in-box where you can read the emails you received and take action to respond to them as though you were a manager within the fictitious organization.",
          para2: "While you are in the email in-box, you will have access to the following:",
          bullet1: "The test instructions;",
          bullet2:
            "The background information describing your job as the manager and the fictitious organization where you work;",
          bullet3: "A notepad to serve as scrap paper.",
          step1Section: {
            title: "Step 1 - Responding to emails",
            description:
              "You can respond to the emails you received in two ways: by writing an email or by adding tasks to your task list. A description of both ways of responding is presented below, followed by examples.",
            part1: {
              title: "Example of an email you have received:",
              para1:
                "Two options are provided below to demonstrate different ways of responding to the email. You can choose one of the two options presented or a combination of the two. The responses are only provided to illustrate how to use each of the two ways of responding. They do not necessarily demonstrate the Key Leadership Competencies assessed in this situation."
            },
            part2: {
              title: "Responding with an email response",
              para1:
                "You can write an email in response to one you received in your in-box. The written response should reflect how you would respond as a manager.",
              para2:
                "You can use the following options: reply, reply all or forward. If you choose to forward an email, you will have access to a directory with all of your contacts. You can write as many emails as you like to respond to an email or to manage situations you identified across several of the emails you received."
            },
            part3: {
              title: "Example of responding with an email response:"
            },
            part4: {
              title: "Adding a task to the task list",
              para1:
                "In addition to, or instead of, responding by email, you can add tasks to the task list. A task is an action that you intend to take to address a situation presented in the emails. For example, tasks could include planning a meeting or asking a colleague for information. You should provide enough information in your task description to ensure it is clear which situation you are addressing. You should also specify what actions you plan to take, and with whom. You can return to the email to add, delete or edit tasks at any time."
            },
            part5: {
              title: "Example of adding a task to the task list:"
            },
            part6: {
              title: "How to choose a way of responding",
              para1:
                "There are no right or wrong ways to respond. When responding to an email, you can:",
              bullet1: "send an email or emails; or",
              bullet2: "add a task or tasks to your task list; or",
              bullet3: "do both.",
              para2:
                "You will be assessed on the content of your responses, and not on your method of responding (i.e. whether you responded by email and/or by adding a task to your task list). As such, answers need to be detailed and clear enough for assessors to evaluate how you are addressing the situation. For example, if you plan to schedule a meeting, you will need to specify what will be discussed at that meeting.",
              para3Part1:
                "When responding to an email you received, if you decide to write an email ",
              para3Part2: "and",
              para3Part3:
                " to add a task to your task list, you do not need to repeat the same information in both places. For example, if you mention in an email that you will schedule a meeting with a co-worker, you do not need to add that meeting to your task list."
            }
          },
          step2Section: {
            title: "Step 2 - Adding reasons for your actions (optional)",
            description:
              "After writing an email or adding a task, if you feel the need to explain why you took a specific action, you can write it in the reasons for your actions section, located below your email response or tasks. Providing reasons for your actions is optional. Note that you may choose to explain some actions and not others if they do not require any additional explanations. Similarly, you may decide to add the reasons for your actions when responding to some emails and not to others. This also applies to tasks in the task list.",
            part1: {
              title: "Example of an email response with reasons for your actions:"
            },
            part2: {
              title: "Example of a task list with reasons for your actions:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (Manager, Quality Assurance Team)",
            from: "Geneviève Bédard (Director, Research and Innovation Unit)",
            subject: "Preparing Mary for her assignment",
            date: "Friday, November 4",
            body:
              "Hello T.C.,\n\nI was pleased to hear that one of your quality assurance analysts, Mary Woodside, has accepted a six-month assignment with my team, starting on January 2. I understand she has experience in teaching and using modern teaching tools from her previous work as a college professor. My team needs help developing innovative teaching techniques that promote employee productivity and general well-being. Therefore, I think Mary’s experience will be a good asset for the team.\n\nAre there any areas in which you might want Mary to gain more experience and which could be of value when she returns to your team? I want to maximize the benefits of the assignment for both our teams.\n\nLooking forward to getting your input,\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Hi Geneviève,\n\nI agree that we should plan Mary’s assignment so both our teams can benefit from it. I suggest that we train Mary in the synthesis of data from multiple sources. Doing so could help her broaden her skill set and would be beneficial to my team when she comes back. Likewise, your team members could benefit from her past experience in teaching. I’ll consult her directly as I would like to have her input on this. I’ll get back to you later this week once I have more information to provide you on this matter.\n\nThat being said, what are your expectations? Are there any current challenges or specific team dynamics that should be taken into account? I’d like to consider all factors, such as the current needs of your team, challenges and team dynamics before I meet with Mary to discuss her assignment.\n\nThanks,\n\nT.C.",
            reasonsForAction:
              "I am planning to meet with Mary to discuss her expectations regarding the assignment and to set clear objectives. I want to ensure she feels engaged and knows what is expected of her, to help her prepare accordingly. I will also check what her objectives were for the year to ensure that what I propose is consistent with her professional development plan."
          },
          exampleTaskResponse: {
            task:
              "- Reply to Geneviève’s email:\n     > Suggest training Mary in the synthesis of information from multiple sources so that she can broaden her skill set.\n     > Ask what her expectations are and what the challenges are on her team so I can consider all factors when determining how her team could benefit from Mary’s experience in providing training.\n     > Inform her that I will get more information from Mary and will respond with suggestions by the end of the week.\n- Schedule a meeting with Mary to discuss her assignment objectives and ensure she feels engaged and knows what is expected of her.\n- Refer to Mary’s past and current objectives to ensure that what I propose is in line with her professional development plan.",
            reasonsForAction:
              "Training Mary in the synthesis of information from multiple sources would be beneficial to my team which needs to consolidate information gathered from many sources. Asking Geneviève what her expectations and challenges are will help me better prepare Mary and ensure that the assignment is beneficial to both our teams."
          }
        },
        evaluation: {
          title: "Evaluation",
          bullet1:
            "Both the actions you take and the explanations you give will be considered when assessing your performance on each of the Key Leadership Competencies (described below). You will be assessed on the extent to which they demonstrate the Key Leadership Competencies.",
          bullet2:
            "Your actions will be evaluated on effectiveness. Effectiveness is measured by whether your actions would have a positive or a negative impact on resolving the situations presented and by how widespread that impact would be.",
          bullet3:
            "Your responses will also be evaluated on how well they meet the organizational objectives presented in the background information.",
          keyLeadershipCompetenciesSection: {
            title: "Key Leadership Competencies",
            para1Title: "Create Vision and Strategy: ",
            para1:
              "Managers help to define the future and chart a path forward. To do so, they take into account context. They leverage their knowledge and seek and integrate information from diverse sources to implement concrete activities. They consider different perspectives and consult as needed. Managers balance organizational priorities and improve outcomes.",
            para2Title: "Mobilize People: ",
            para2:
              "Managers inspire and motivate the people they lead. They manage the performance of their employees and provide constructive and respectful feedback to encourage and enable performance excellence. They lead by example, setting goals for themselves that are more demanding than those that they set for others.",
            para3Title: "Uphold Integrity and Respect: ",
            para3:
              "Managers exemplify ethical practices, professionalism and personal integrity, acting in the interest of Canada and Canadians. They create respectful, inclusive and trusting work environments where sound advice is valued. They encourage the expression of diverse opinions and perspectives, while fostering collegiality.",
            para4Title: "Collaborate with Partners and Stakeholders: ",
            para4:
              "Managers are deliberate and resourceful about seeking a wide spectrum of perspectives. In building partnerships, they manage expectations and aim to reach consensus. They demonstrate openness and flexibility to improve outcomes and bring a whole-of-organization perspective to their interactions. Managers acknowledge the role of partners in achieving outcomes.",
            para5Title: "Promote Innovation and Guide Change: ",
            para5:
              "Managers create an environment that supports bold thinking, experimentation and intelligent risk taking. When implementing change, managers maintain momentum, address resistance and anticipate consequences. They use setbacks as a valuable source of insight and learning.",
            para6Title: "Achieve Results: ",
            para6:
              "Managers ensure that they meet team objectives by managing resources. They anticipate, plan, monitor progress and adjust as needed. They demonstrate awareness of the context when making decisions. Managers take personal responsibility for their actions and the outcomes of their decisions."
          }
        }
      },

      //Background Page
      background: {
        hiddenTabNameComplementary: 'under "Background Information"',
        orgCharts: {
          link: "Image Description",
          ariaLabel: "Image description of the organization chart",
          treeViewInstructions:
            "Below is a tree view of the organization chart. Once selected, you can use the arrow keys to navigation, expand, and collapse information."
        }
      },

      //Inbox Page
      inboxPage: {
        tabName: 'under "In-Box"',
        emailId: " email ",
        subject: "Subject",
        to: "To",
        from: "From",
        date: "Date",
        addReply: "Add email response",
        addTask: "Add task list",
        yourActions: `You responded with {0} emails and {1} tasks`,
        editActionDialog: {
          addEmail: "Add email response",
          editEmail: "Edit email response",
          addTask: "Add task list",
          editTask: "Edit task",
          save: "Save response"
        },
        characterLimitReached: "(Limit reached)",
        emailCommons: {
          to: "To:",
          toFieldSelected: "To field selected.",
          cc: "CC:",
          ccFieldSelected: "Cc field selected.",
          currentSelectedPeople: "Current selected people are: {0}",
          currentSelectedPeopleAreNone: "None",
          reply: "Reply",
          replyAll: "Reply all",
          forward: "Forward",
          editButton: "Edit response",
          deleteButton: "Delete response",
          originalEmail: "Original email",
          toAndCcFieldsPlaceholder: "Select from address book",
          yourResponse: "Your response"
        },
        addEmailResponse: {
          selectResponseType: "Please select how you would like to respond to the original email:",
          response: "Your email response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit",
          emailResponseTooltip: "Write a response to the email you received.",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information",
          invalidToFieldError: "This field cannot be empty"
        },
        emailResponse: {
          title: "Email Response #",
          description: "For this response, you've chosen to:",
          response: "Your email response:",
          reasonsForAction: "Your reasons for action (optional):"
        },
        addEmailTask: {
          header: "Email #{0}: {1}",
          task: "Your task list response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit"
        },
        taskContent: {
          title: "Task List Response #",
          task: "Your task list response:",
          taskTooltipPart1: "An action you intend to take to address a situation in the emails.",
          taskTooltipPart2: "Example: Planning a meeting, asking a colleague for information.",
          reasonsForAction: "Your reasons for action:",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information"
        },
        deleteResponseConfirmation: {
          title: "Are you sure you want to cancel this response?",
          systemMessageTitle: "Warning!",
          systemMessageDescription:
            "Your response will not be saved if you proceed. If you wish to save your answer, you may return to the response. All of your responses can be edited or deleted before submission.",
          description:
            'If you do not wish to save the response, click the "Delete response" button.'
        }
      },

      //Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Congratulations! Your test has been submitted.",
        feedbackSurvey:
          "We would appreciate your feedback on the assessment. Please fill out this optional {0} before logging out and leaving.",
        optionalSurvey: "15 minute survey",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=en-US",
        logout:
          "For security reasons, please ensure you log out of your account in the top right corner of this page. You may quietly gather your belongings and leave the test session. If you have any questions or concerns about your test, please contact {0}.",
        thankYou: "Thank you for completing your assessment. Good luck!"
      },

      //Quit Confirmation Page
      quitConfirmationPage: {
        title: "You have quit the test",
        instructionsTestNotScored1: "Your test ",
        instructionsTestNotScored2: "will not be scored.",
        instructionsRaiseHand:
          "Please raise your hand. The test administrator will come see you with further instructions.",
        instructionsEmail:
          "If you have any questions or concerns about your test, please contact {0}."
      },

      //Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Your test time is over...",
        timeoutSaved:
          "Your responses have been saved and submitted for scoring. Please note that the information in the notepad is not saved.",
        timeoutIssue:
          'If there was an issue, please advise your test administrator. Click "Continue" to exit this test session and to receive further instructions.'
      },

      //Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Background Information",
        inboxTabTitle: "In-Box",
        disabled: "You cannot access the inbox until you start the test.",
        nextButton: "Next",
        previousButton: "Previous"
      },

      //Test Footer
      testFooter: {
        timer: {
          timer: "Timer",
          timeLeft: "Time left in test session:",
          timerHidden: "Timer hidden."
        },
        submitTestPopupBox: {
          title: "Confirm test submission?",
          warning: {
            title: "Warning! The notepad will not be saved.",
            message:
              "Anything written in the notepad will not be submitted with the test for scoring. Ensure that you have reviewed all of your responses before submitting the test as you will not be able to go back to make changes."
          },
          description:
            "If you are ready to send your test in for scoring, click the “Submit Test” button. You will be exited out of this test session and provided further instructions."
        },
        quitTestPopupBox: {
          title: "Are you sure you want to quit this test?",
          description:
            "All answers will be deleted. You will not be able to recover your answers, and will forfeit from this assessment. To quit, you must acknowledge the following:",
          checkboxOne: "I voluntarily withdraw from this examination",
          checkboxTwo: "My test will not be scored",
          checkboxThree:
            "I am aware that the retest period for this test may apply, should I wish to write this test again"
        }
      }
    },

    //Screen Reader
    ariaLabel: {
      mainMenu: "Main Menu",
      tabMenu: "eMIB Tab Menu",
      instructionsMenu: "Instructions Menu",
      languageToggleBtn: "language-toggle-button",
      authenticationMenu: "Authentication Menu",
      emailHeader: "email header",
      responseDetails: "response details",
      reasonsForActionDetails: "reasons for action details",
      taskDetails: "task details",
      emailOptions: "email options",
      taskOptions: "task options",
      taskTooltip: "task tooltip",
      emailResponseTooltip: "email response tooltip",
      reasonsForActionTooltip: "reasons for action tooltip",
      passwordConfirmationRequirements: "It must match your password",
      dobDayField: "Day field selected",
      dobMonthField: "Month field selected",
      dobYearField: "Year field selected",
      emailsList: "Emails list",
      topNavigationSection: "Top navigation",
      sideNavigationSection: "Side navigation",
      notepadSection: "Notepad section",
      quitTest: "Quit test"
    },

    //Commons
    commons: {
      psc: "Public Service Commission",
      nextButton: "Next",
      backButton: "Back",
      enterEmibSample: "Proceed to sample e-MIB test",
      enterEmibReal: "Continue to test instructions",
      resumeEmibReal: "Resume e-MIB",
      startTest: "Start test",
      resumeTest: "Resume test",
      confirmStartTest: {
        aboutToStart: "You are about to start the timed test.",
        timerWarning: `After clicking start, you'll be taken to the "Background Information" tab. You will have {0} to complete the test.`,
        instructionsAccess:
          "You will have access to the instructions from within the test. Good luck!",
        timeUnlimited: "unlimited time",
        numberMinutes: "{0} minutes"
      },
      submitTestButton: "Submit Test",
      quitTest: "Quit Test",
      returnToTest: "Return to test",
      returnToResponse: "Return to response",
      passStatus: "Pass",
      failStatus: "Fail",
      enabled: "Enabled",
      disabled: "Disabled",
      backToTop: "Back to top",
      notepad: {
        title: "Notepad",
        placeholder: "Put your notes here..."
      },
      cancel: "Cancel",
      cancelChanges: "Cancel changes",
      cancelResponse: "Cancel response",
      close: "Close",
      login: "Login",
      logout: "Logout",
      ok: "Ok",
      continue: "Continue"
    }
  },

  fr: {
    //Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Accueil",
      homeTabTitleAuthenticated: "Accueil",
      dashboardTabTitle: "Tableau de bord",
      sampleTest: "Échantillon de la BRG-e",
      sampleTests: "Échantillons de tests",
      statusTabTitle: "Statut",
      psc: "Commission de la fonction publique du Canada",
      canada: "Gouvernement du canada",
      skipToMain: "Passer au contenu principal",
      menu: "MENU"
    },

    //HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "OÉC - Échantillons de tests",
      eMIBOverview: "Aperçu de la BRG-e",
      eMIB: "Évaluation BRG-e ",
      status: "OÉC - État du system",
      home: "OÉC - Accueil",
      homeWithError: "Erreur - OÉC - Accueil",
      testAdministration: "OÉC - Séances de test"
    },

    //authentication
    authentication: {
      login: {
        title: "Connexion",
        content: {
          title: "Connexion",
          description:
            "Un compte est nécessaire pour continuer. Pour ouvrir une session, entrez votre adresse courriel et votre mot de passe.",
          inputs: {
            emailTitle: "Adresse courriel :",
            passwordTitle: "Mot de passe :"
          }
        },
        button: "Connexion",
        invalidCredentials: "Identifiants invalides!",
        passwordFieldSelected: "Champ Mot de passe sélectionné"
      },
      createAccount: {
        title: "Créer un compte",
        content: {
          title: "Créer un compte",
          description:
            "Un compte est nécessaire pour continuer. Pour créer un compte, remplissez le formulaire suivant.",
          inputs: {
            valid: "valide",
            firstNameTitle: "Prénom :",
            firstNameError: "Doit être un prénom valide",
            lastNameTitle: "Nom de famille :",
            lastNameError: "Doit être un nom de famille valide",
            dobDayTitle: "Date de naissance: (JJ / MM / ---A) :",
            dobError: "Veuillez remplir tous les champs en lien avec la date de naissance",
            dobTooltip: "Vous n’aurez qu’à fournir le dernier chiffre de votre année de naissance.",
            emailTitle: "Adresse courriel :",
            emailError: "Doit être une adresse courriel valide",
            priOrMilitaryNbrTitle: "CIDP ou numéro matricule (s’il y a lieu) :",
            priOrMilitaryNbrError: "Doit être un CIDP ou numéro matricule valide",
            passwordTitle: "Mot de passe :",
            passwordErrors: {
              description: "Votre mot de passe doit satisfaire aux exigences suivantes :",
              upperCase: "Au moins une lettre majuscule",
              lowerCase: "Au moins une lettre minuscule",
              digit: "Au moins un chiffre",
              specialCharacter: "Au moins un caractère spécial",
              length: "Un minimum de 8 caractères et un maximum de 15 caractères "
            },
            passwordConfirmationTitle: "Confirmer le mot de passe :",
            passwordConfirmationError:
              "Votre confirmation de mot de passe doit correspondre au mot de passe"
          }
        },
        privacyNotice:
          "J'ai lu et accepté la façon dont la Commission de la fonction publique recueille, utilise et divulgue des renseignements personnels, comme énoncées dans {0}.",
        privacyNoticeLink: "l'avis de confidentialité",
        privacyNoticeError: "Vous devez accepter l’avis de confidentialité en cliquant sur la case",
        button: "Créer un compte",
        accountAlreadyExistsError: "Un compte est déjà associé à cette adresse de courriel",
        passwordTooCommonError: "Ce mot de passe est trop commun",
        passwordTooSimilarToUsernameError:
          "Le mot de passe est trop semblable au nom d’utilisateur",
        passwordTooSimilarToFirstNameError: "Le mot de passe est trop semblable au prénom",
        passwordTooSimilarToLastNameError: "Le mot de passe est trop semblable au nom de famille",
        passwordTooSimilarToEmailError: "Le mot de passe est trop semblable au courriel",
        privacyNoticeDialog: {
          title: "Confidentialité et sécurité",
          privacyNoticeStatement: "Énoncé de confidentialité — Étude pilote de la BRG-e",
          privacyParagraph1:
            "La Commission de la fonction publique du Canada (CFP) utilisera les renseignements personnels concernant le présent projet de recherche, lesquels comprennent les résultats obtenus aux tests et les évaluations des superviseurs, à des fins de recherche, d’analyse et d’élaboration de tests. Les résultats pourront éventuellement servir à des questions futures liées à la dotation avec votre consentement. Tous les renseignements sont recueillis en vertu des articles 11, 30, 35 et 36 de la Loi sur l’emploi dans la fonction publique conformément à la Loi sur la protection de renseignements personnels. La CFP s’engage à protéger le droit des personnes à la vie privée.",
          publicServiceEmploymentActLink: "Loi sur l’emploi dans la fonction publique",
          privacyActLink: "Loi sur la protection des renseignements personnels",
          privacyParagraph2:
            "Les « renseignements personnels » se définissent comme étant les renseignements, quels que soient leur forme et leur support, concernant un individu identifiable.",
          privacyCommissionerLink: "commissaire à la protection de la vie privée du Canada",
          privacyParagraph3:
            "L’utilisation et la collecte des résultats obtenus aux tests sont définies dans le fichier de renseignements personnels CFP-PPU-025. Les renseignements personnels liés à la recherche, à l’analyse et à l’élaboration de test sont conservés pendant 10 ans avant d’être détruits. Les réponses anonymes aux tests et les données démographiques sont conservées indéfiniment sous forme de fichiers informatisés.",
          privacyParagraph4:
            "La participation est facultative. Cependant, si vous décidez de ne pas fournir vos renseignements personnels, vous ne pourrez pas participer à cette étude.",
          privacyParagraph5:
            "Toute personne a droit à la protection et à la consultation de ses renseignements personnels, et est en droit de demander que des corrections y soient apportées si elle estime qu’il y a une erreur ou une omission. Toute personne souhaitant consulter ses renseignements personnels ou y apporter des corrections peut communiquer avec la Division de l’accès à l’information et de la protection des renseignements personnels de la CFP.",
          privacyParagraph6:
            "Les renseignements personnels recueillis par la CFP sont protégés contre toute communication à des personnes non autorisées ou à des ministères et organismes assujettis aux dispositions de la Loi sur la protection des renseignements personnels. Cependant, les renseignements personnels peuvent être communiqués sans votre consentement dans des circonstances particulières, conformément à l’article 8 de la Loi sur la protection des renseignements personnels.",
          accessToInformationLink:
            "Division de l’Accès à l’information et de la protection des renseignements personnels",
          privacyParagraph7:
            "Toute personne a le droit de déposer une plainte auprès du Commissaire à la protection de la vie privée du Canada concernant le traitement de ses renseignements personnels par un ministère.",
          infoSourceChapterLink: "chapitre d’Info Source",
          reproductionTitle: "Reproduction ou divulgation non autorisées du contenu du test",
          reproductionWarning:
            "Ce test et son contenu portent le niveau de sécurité Protégé B. La reproduction ou l'enregistrement du contenu de ce test, sous quelque forme que ce soit, sont strictement interdits. Tous les documents liés au test, y compris les brouillons, doivent être remis à l'administrateur du test à la fin de celui-ci. La reproduction, l'enregistrement ou la divulgation non autorisées du contenu du test contreviennent à la Politique du gouvernement sur la sécurité, et l'utilisation de renseignements obtenus ou transmis de manière inappropriée peut constituer une infraction à la Loi sur l'emploi dans la fonction publique (LEFP). Les parties impliquées dans la divulgation ou l'utilisation inappropriée de contenu de test protégé pourraient faire l'objet d'une enquête en vertu de la LEFP. Au terme de cette enquête, les personnes reconnues coupables de fraude pourraient faire l'objet d'une déclaration de culpabilité par procédure sommaire ou voir leur dossier renvoyé à la Gendarmerie royale du Canada.",
          cheatingTitle: "Tricherie",
          cheatingWarning:
            "Veuillez prendre note que tous les cas présumés de tricherie seront renvoyés au gestionnaire responsable et au Centre de psychologie du personnel, qui prendront les mesures nécessaires. En cas de tricherie présumée, les résultats de test pourraient être invalidés, et les parties impliquées pourraient faire l'objet d'une enquête en vertu de la LEFP. Au terme de cette enquête, les personnes reconnues coupables de fraude pourraient faire l'objet d'une déclaration de culpabilité par procédure sommaire ou voir leur dossier renvoyé à la Gendarmerie royale du Canada."
        }
      }
    },

    //Menu Items
    menu: {
      etta: "FR System Administration",
      ppc: "FR PPC Administration",
      ta: "FR Test Administration",
      checkIn: "FR Check-in",
      profile: "FR Profile",
      incidentReport: "FR Incident Report",
      Notifications: "FR Notifications",
      MyTests: "FR My Tests",
      ContactUs: "FR Contact Us",
      logout: "FR Logout"
    },

    //Token Expired Popup Box
    tokenExpired: {
      title: "FR Session Expired",
      description:
        "FR Due to inactivity your login session has expired, you will need to login again."
    },

    //Home Page
    homePage: {
      welcomeMsg: "Bienvenue dans l'Outil d'évaluation des candidats",
      description:
        "Ce site Web est utilisé pour évaluer les candidats pour des postes au sein de la fonction publique fédérale. Pour accéder à vos tests, vous devez ouvrir une session ci-dessous. Si vous n’avez pas de compte, vous pouvez en créer un à l’aide de votre adresse de courriel."
    },

    //Sample Tests Page
    sampleTestDashboard: {
      title: "Échantillons de tests",
      description:
        "Vous trouverez ci-dessous la liste des échantillons de tests qui sont disponibles. Les échantillons de tests ne sont pas corrigés et les réponses ne sont pas soumises à la fin d’un test. Cliquer sur « Afficher » pour accéder à l’échantillon de test.",
      table: {
        columnOne: "Nom du test",
        columnTwo: "Action",
        viewButton: "Afficher"
      }
    },

    //Dashboard Page
    dashboard: {
      title: "Bienvenue, {0} {1}.",
      description:
        "Vous êtes connecté à votre compte. Si vous devez passer un test, veuillez attendre les instructions de l'administrateur de test.",
      table: {
        columnOne: "Nom du test",
        columnTwo: "Action",
        viewButton: "Afficher",
        noTests: "Il n’y a aucun test affecté à votre compte"
      }
    },

    //Checkin action
    candidateCheckIn: {
      button: "M'enregistrer",
      loading: "Recherche de tests actifs...",
      checkedInText: "Vous vous êtes enregistré avec le code d'accès au test : ",
      popup: {
        title: "Saisissez votre code d'accès au test",
        description:
          "Veuillez saisir le code d'accès au test fourni par l'administrateur de test dans le champ ci-dessous. Ensuite, cliquez sur « M'enregistrer ».",
        textLabel: "Code d'accès au test :",
        textLabelError: "Saisissez un code d'accès au test valide"
      }
    },

    //TA Check-in Page
    testAdministration: {
      title: "Bienvenue, {0} {1}.",
      checkedOutDescriptionPart1:
        "Vous êtes connecté à votre compte. Si vous vous apprêtez à administrer un test, vous devrez générer un code d'accès au test pour la séance de test. Le code d'accès au test fournira aux candidats l'accès au test pour toute la durée de la séance de test.",
      checkedOutDescriptionPart2:
        "Veuillez seulement générer le code d'accès au test immédiatement avant une séance de test.",
      checkedInDescriptionPart1:
        "Le code d'accès au test a été généré avec succès. Veuillez valider l'information du test ci-dessous. SI l'information est exacte, fournissez le code d'accès au test aux candidats et demandez-leur de s'enregistrer pour la séance de test.",
      checkedInDescriptionPart2:
        "Si des informations sont inexactes,  désactivez le code  d'accès au test maintenant et générez un nouveau code.",
      checkedInDescriptionPart3: "Code d'accès au test : {0}",
      checkedInDescriptionPart4:
        "Au fur et à mesure que les candidats s'enregistrent, leurs noms apparaîtront dans la liste des Candidats actifs ci-dessous. Une fois que tous les candidats présents à la séances se sont enregistrés, veuillez désactiver le code d'accès au test.",
      checkInButton: "Générer le code d'accès au test",
      checkOutButton: "Désactiver le code d'accès au test",
      popupBox: {
        title: "Saisissez les informations pour l'enregistrement de la séance de test.",
        description:
          "Veuillez fournir le numéro du processus de dotation, la langue de la séance de test et le test à administrer.",
        form: {
          firstElement: {
            title: "Numéro du processus de dotation :"
          },
          secondElement: {
            title: "Langue de la séance de test :",
            dropdown: {
              placeholder: "Sélectionner",
              option1: "Anglais",
              option2: "Français"
            },
            invalidError: "Veuillez sélectionner une langue de la séance de test valide"
          },
          thirdElement: {
            title: "Test à administrer :",
            dropdown: {
              placeholder: "Sélectionner"
            },
            invalidError: "Veuillez sélectionner un test à administrer valide"
          }
        }
      },
      activeCandidatesTable: {
        title: "Candidats actifs",
        column1: "Sélectionner",
        column2: "Candidat",
        column3: "Statut",
        column4: "Minuterie",
        column5: "Actions"
      }
    },

    //Profile Page
    profile: {
      title: "Bienvenue, {0} {1}.",
      sideNavItems: {
        personalInfo: "Information Personnelle",
        password: "Mot de passe",
        preferences: "Préférences",
        permissions: "Permissions",
        profileMerge: "FR Profile Merge"
      },
      personalInfo: {
        title: "FR Your Contact Information",
        nameSection: {
          title: "Nom :",
          firstName: "Prénom",
          lastName: "Nom de famille"
        },
        emailAddressesSection: {
          title: "Adresses courriel :",
          primary: "Primaire",
          secondary: "Secondaire"
        },
        dateOfBirth: {
          title: "Date de naissance :",
          yearField: "Année",
          yearFieldSelected: "FR Year field selected.",
          monthField: "Mois",
          monthFieldSelected: "FR Month field selected.",
          dayField: "Jour",
          dayFieldSelected: "FR Day field selected.",
          currentValue: "FR Current value is:"
        },
        priOrMilitaryNbr: "CIDP ou numéro matricule :",
        optionalField: " (Optionnelle)"
      },
      password: {
        newPassword: {
          title: "Mot de passe",
          updatedDate: "FR Your password was last updated on: {0}",
          currentPassword: "Mot de passe actuel:",
          newPassword: "Nouveau mot de passe:",
          confirmPassword: "FR Confirm Password:"
        },
        passwordRecovery: {
          title: "FR Password Recovery",
          secretQuestion: "FR Secret Question:",
          secretAnswer: "FR Secret Answer:"
        }
      },
      preferences: {
        title: "Préférences",
        description: "FR Modify your preferences.",
        notifications: {
          title: "Notifications",
          checkBoxOne:
            "FR Always send an email to my primary email address when I receive a notification",
          checkBoxTwo:
            "FR Always send an email to my secondary email address when I receive a notification"
        },
        display: {
          title: "FR Display",
          checkBoxOne: "FR Allow others to see my profile picture",
          checkBoxTwo: "FR Hide tooltip icons"
        }
      }
    },

    //Status Page
    statusPage: {
      title: "Statut de OÉC",
      logo: "Logo Thunder CAT",
      welcomeMsg:
        "Page de statut interne afin de déterminer rapidement l'état / la santé de l'outil d'évaluation des candidats.",
      versionMsg: "Version de l'Outil d'évaluation des candidats: ",
      gitHubRepoBtn: "Répertoire GitHub",
      serviceStatusTable: {
        title: "Statut des services",
        frontendDesc: "La Face avant de l'application est construite et utilisée avec succès",
        backendDesc: "La Face arrière de l'application réussit les demandes API avec succès",
        databaseDesc: "La Base de données réussit les demandes API avec succès"
      },
      systemStatusTable: {
        title: "Statut du système",
        javaScript: "JavaScript",
        browsers: "IE 10+, Firefox, Chrome",
        screenResolution: "Résolution d'écran minimum de 1024 x 768"
      },
      additionalRequirements:
        "De plus, les exigences suivantes doivent être respectées pour l’utilisation de cette application dans un centre de test.",
      secureSockets: "Chiffrement Secure Socket Layer (SSL) activé",
      fullScreen: "Mode plein écran activé",
      copyPaste: "Fonction copier-coller activée",
      colorOptions: "Options Internet IE > Modification des couleurs permise",
      fontsEnabled: "Options Internet IE > Modification de la police permise",
      accessibilityOptions: "Options Internet IE > Options d’ergonomie permises"
    },

    // Settings Dialog
    settings: {
      systemSettings: "Afficher les paramètres en utilisant les outils du navigateur IE",
      zoom: {
        title: "Zoom avant et zoom arrière (+ / -)",
        instructionsListItem1:
          "Cliquer sur le bouton Visualiser dans la barre de menu supérieure à gauche dans Internet Explorer.",
        instructionsListItem2: "Sélectionner Zoom.",
        instructionsListItem3:
          "Vous pouvez choisir un niveau de zoom prédéfini ou un niveau sur mesure (sélectionner Sur mesure avant de saisir une valeur de zoom).",
        instructionsListItem4:
          "Vous pouvez également appuyer simultanément sur les touches CTRL et + / - de votre clavier pour effectuer un zoom avant ou un zoom arrière."
      },
      textSize: {
        title: "Taille de texte",
        instructionsListItem1:
          "Cliquer sur le bouton Visualiser dans la barre de menu supérieure à gauche dans Internet Explorer.",
        instructionsListItem2: "Sélectionner Taille de texte.",
        instructionsListItem3:
          "Choisir d’agrandir ou de diminuer la taille du texte qui apparaît à l’écran.",
        instructionsListItem4:
          "Cliquer sur le bouton Outils, puis sélectionner l’onglet Général. Sous Apparence, sélectionner Accessibilité.",
        instructionsListItem5:
          "Cocher la case Ignorer les tailles de police spécifiées sur les pages Web.",
        instructionsListItem6: "Cliquer sur OK, puis encore une fois sur OK.",
        notChanged: "Si la taille du texte n’a pas changé :"
      },
      fontStyle: {
        title: "Police de caractères",
        instructionsListItem1:
          "Cliquer sur le bouton Outils dans la barre de menu supérieure à gauche dans Internet Explorer.",
        instructionsListItem2: "Choisir Options Internet.",
        instructionsListItem3: "Dans l’onglet Général, sous Apparence, sélectionner Accessibilité.",
        instructionsListItem4:
          "Cocher la case Ignorer les style de police spécifiées sur les pages Web. ",
        instructionsListItem5: "Cliquer sur OK.",
        instructionsListItem6: "Dans l’onglet Général, sous Apparence, sélectionner Polices.",
        instructionsListItem7: "Choisir la police que vous désirez utiliser.",
        instructionsListItem8: "Cliquer sur OK, puis encore une fois sur OK."
      },
      color: {
        title: "Couleur du texte et de l’arrière plan",
        instructionsListItem1: "Cliquer sur le bouton Outils et sélectionner Options Internet.",
        instructionsListItem2: "Dans l’onglet Général, sous Apparence, sélectionner Accessibilité.",
        instructionsListItem3: "Cocher la case Ignorer les couleurs spécifiées sur les pages Web.",
        instructionsListItem4: "Cliquer sur OK.",
        instructionsListItem5: "Dans l’onglet Général, sous Apparence, sélectionner Couleurs.",
        instructionsListItem6: "Décocher la case Utiliser les couleurs Windows.",
        instructionsListItem7:
          "Pour chaque couleur que vous désirez modifier, cliquer sur la case de couleur, choisir une nouvelle couleur et cliquer sur OK.",
        instructionsListItem8: "Cliquer sur OK, puis encore une fois sur OK."
      }
    },

    //eMIB Test
    emibTest: {
      //Home Page
      homePage: {
        testTitle: "Échantillon de la BRG-e",
        welcomeMsg: "Bienvenue dans le test pratique de BRG-e"
      },

      //HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Conseils pour répondre à la BRG-e",
          part1: {
            description:
              "La BRG-e vous présente des situations qui vous donneront l’occasion de démontrer les compétences clés en matière de leadership. Voici quelques conseils qui vous aideront à fournir aux évaluateurs l’information dont ils ont besoin pour évaluer votre rendement par rapport à ces compétences clés en leadership :",
            bullet1:
              "Répondez à toutes les questions posées dans les courriels que vous avez reçus. Notez également que des situations pour lesquelles on ne pose pas de questions précises peuvent être abordées dans plusieurs courriels. Vous profiterez ainsi de toutes les occasions qui vous sont offertes de démontrer ces compétences.",
            bullet2:
              "N’hésitez pas à présenter vos recommandations si nécessaire, et ce, même s’il s’agit seulement de réflexions préliminaires. S’il le faut, vous pouvez ensuite noter les autres renseignements dont vous auriez besoin pour en arriver à une décision finale.",
            bullet3:
              "Si dans certaines situations vous croyez avoir besoin de parler en personne avec quelqu’un avant de prendre une décision, indiquez les renseignements dont vous avez besoin et comment cela pourrait influencer votre décision.",
            bullet4:
              "Utilisez uniquement l’information fournie dans les courriels et l’information contextuelle. Ne tirez aucune conclusion fondée sur la culture de votre propre organisation. Évitez de faire des suppositions qui ne sont pas raisonnablement corroborées par l’information contextuelle ou les courriels.",
            bullet5:
              "Les situations présentées dans la BRG-e s’inscrivent dans un domaine particulier afin de vous donner suffisamment de contexte pour y répondre.  Pour être efficaces, vos réponses doivent démontrer la compétence ciblée, et non la connaissance du domaine en question."
          },
          part2: {
            title: "Autres renseignements importants",
            bullet1:
              "Le contenu de vos courriels, l'information fournie dans votre liste de tâches et les justifications des mesures prises seront évalués. Le contenu du bloc-notes ne sera pas évalué.",
            bullet2:
              "Votre rédaction ne sera pas évaluée. Aucun point ne sera enlevé pour les fautes d’orthographe, de grammaire, de ponctuation ou pour les phrases incomplètes. Votre rédaction devra toutefois être suffisamment claire pour que les évaluateurs comprennent la situation que vous traitez et vos principaux arguments.",
            bullet3: "Vous pouvez répondre aux courriels dans l’ordre que vous désirez.",
            bullet4: "Vous êtes responsable de la gestion de votre temps."
          }
        },
        testInstructions: {
          title: "Instructions du test",
          hiddenTabNameComplementary: "Sous « Instructions »",
          onlyOneTabAvailableForNowMsg:
            "Veuillez noter qu’il n’y a qu’un seul onglet principal disponible pour l’instant. Dès que vous commencerez le test, les autres principaux onglets deviendront disponibles.",
          para1:
            "Lorsque vous commencez le test, lisez d’abord l’information contextuelle qui décrit votre poste et l’organisation fictive où vous travaillez. Nous vous recommandons de prendre environ 10 minutes pour la lire. Passez ensuite à la boîte de réception pour lire les courriels que vous avez reçus et prenez des mesures pour y répondre, comme si vous étiez gestionnaire dans cette organisation fictive.",
          para2:
            "Lorsque vous serez dans la boîte de réception, vous aurez accès aux éléments suivants :",
          bullet1: "les instructions du test;",
          bullet2:
            "l’information contextuelle décrivant votre rôle en tant que gestionnaire et l’organisation fictive où vous travaillez;",
          bullet3: "un bloc-notes pouvant servir de papier brouillon.",
          step1Section: {
            title: "Étape 1 — Répondre aux courriels",
            description:
              "Vous pouvez répondre aux courriels que vous avez reçus de deux façons : en écrivant un courriel ou en ajoutant des tâches à votre liste de tâches. Ces deux façons de répondre sont décrites ci-dessous, suivies d’exemples.",
            part1: {
              title: "Exemple d’un courriel que vous avez reçu :",
              para1:
                "Vous trouverez ci-dessous deux façons de répondre au courriel. Vous pouvez choisir l’une ou l’autre des deux options présentées, ou combiner les deux. Les réponses fournies ne sont présentées que pour illustrer comment utiliser chacune des deux façons de répondre. Elles ne démontrent pas nécessairement les compétences clés en leadership qui sont évaluées dans cette situation."
            },
            part2: {
              title: "Ajouter une réponse par courriel",
              para1:
                "Vous pouvez écrire un courriel pour répondre à celui que vous avez reçu dans votre boîte de réception. Votre réponse écrite devrait refléter la façon dont vous répondriez en tant que gestionnaire.",
              para2:
                "Vous pouvez utiliser les fonctions suivantes : répondre, répondre à tous ou transférer. Si vous choisissez de transférer un courriel, vous aurez accès à un répertoire qui contient tous vos contacts. Vous pouvez écrire autant de courriels que vous le souhaitez pour répondre à un courriel ou pour gérer des situations que vous remarquez dans plusieurs des courriels reçus."
            },
            part3: {
              title: "Exemple d’une réponse par courriel :"
            },
            part4: {
              title: "Ajouter une tâche à la liste de tâches",
              para1:
                "En plus de répondre par courriel, ou au lieu d’en écrire un, vous pouvez ajouter des tâches à la liste de tâches. Une tâche représente une mesure que vous comptez prendre pour gérer une situation présentée dans les courriels. Voici des exemples de tâches : planifier une rencontre ou communiquer avec un collègue afin d’obtenir de l’information. Assurez-vous de fournir suffisamment d’information dans votre description de la tâche pour que nous sachions à quelle situation vous répondez. Vous devez également préciser quelles mesures vous comptez prendre et qui devra participer à cette tâche. Vous pouvez en tout temps retourner à un courriel pour ajouter, supprimer ou modifier des tâches."
            },
            part5: {
              title: "Exemple d’ajout d’une tâche à la liste de tâches :"
            },
            part6: {
              title: "Comment choisir une façon de répondre",
              para1:
                "Il n’y a pas de bonne ou de mauvaise façon de répondre. Lorsque vous répondez à un courriel, vous pouvez :",
              bullet1: "envoyer un ou des courriels;",
              bullet2: "ajouter une ou des tâches à votre liste de tâches;",
              bullet3: "faire les deux.",
              para2:
                "C’est le contenu de vos réponses qui sera évalué, et non la façon de répondre (c’est-à-dire si vous avez répondu par courriel ou en ajoutant une tâche à votre liste de tâches). Par conséquent, vos réponses doivent être suffisamment détaillées et claires pour que les évaluateurs puissent évaluer comment vous gérez la situation. Par exemple, si vous prévoyez organiser une réunion, vous devez préciser de quoi il y sera question.",
              para3Part1: "Si vous décidez d’écrire un courriel ",
              para3Part2: "et",
              para3Part3:
                " d’ajouter une tâche à votre liste de tâches pour répondre à un courriel que vous avez reçu, vous n’avez pas à répéter la même information aux deux endroits. Par exemple, si vous mentionnez dans un courriel que vous organiserez une réunion avec un collègue, vous n’avez pas à ajouter cette réunion à votre liste de tâches."
            }
          },
          step2Section: {
            title: "Étape 2 — Justifier les mesures prises (facultatif)",
            description:
              "Après avoir écrit un courriel ou ajouté une tâche, vous pouvez expliquer votre raisonnement dans la section réservée à cet effet, si vous le souhaitez. Cette section se situe au bas des réponses par courriel et des tâches. La justification des mesures prises est facultative. Notez que vous pouvez choisir d’expliquer certaines mesures que vous avez prises tandis que d’autres ne nécessitent pas d’explications supplémentaires. De même, vous pouvez décider de justifier les mesures prises lorsque vous répondez à certains courriels, et non à d'autres. Cela s'applique également aux tâches de la liste de tâches.",
            part1: {
              title:
                "Exemple d’une réponse par courriel accompagnée de justifications des mesures prises:"
            },
            part2: {
              title:
                "Exemple d’une liste de tâches accompagnée de justifications des mesures prises:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (gestionnaire, Équipe de l’assurance de la qualité)",
            from: "Geneviève Bédard (directrice, Unité de recherche et innovation)",
            subject: "Préparer Mary à son affectation",
            date: "vendredi 4 novembre",
            body:
              "Bonjour T.C.,\n\nJ’ai été ravie d’apprendre qu’une de tes analystes de l’assurance de la qualité, Mary Woodside, avait accepté une affectation de six mois avec mon équipe, à compter du 2 janvier. Je crois comprendre qu’elle a de l’expérience en enseignement et en utilisation d’outils pédagogiques modernes dans le cadre de son travail antérieur de professeure au niveau collégial. Mon équipe a besoin d’aide pour mettre au point des techniques d’enseignement novatrices qui favorisent la productivité et le bien-être général des employés. Je pense donc que l’expérience de Mary sera un bon atout pour l’équipe.\n\nY a-t-il des domaines dans lesquels tu aimerais que Mary acquière plus d’expérience, laquelle serait utile lors de son retour dans ton équipe? Je tiens à maximiser les avantages de l’affectation pour nos deux équipes.\n\nAu plaisir de recevoir tes commentaires.\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Bonjour Geneviève,\n\nJe suis d’accord que nous devrions planifier l’affectation de Mary afin que nos deux équipes en tirent parti. Je suggère de former Mary à la synthèse de données provenant de sources multiples. Cela l’aiderait à élargir ses compétences et serait utile à mon équipe à son retour. De même, les membres de ton équipe pourraient profiter de son expérience en enseignement. Je la consulterai directement, car j’aimerais connaître son avis à ce sujet. Je te recontacterai au cours de la semaine, quand j’aurai plus d’information à te fournir.\n\nCela dit, quelles sont tes attentes? Y a-t-il présentement certains défis ou des aspects particuliers de la dynamique de ton équipe dont il faudrait tenir compte? Avant de rencontrer Mary pour discuter de son affectation, j’aimerais tenir compte de tous les facteurs, tels que les besoins actuels, les défis et la dynamique de ton équipe.\n\nMerci.\n\nT.C.",
            reasonsForAction:
              "Je compte rencontrer Mary pour discuter de ses attentes concernant l’affectation et pour établir des objectifs clairs. Je veux qu’elle se sente motivée et sache ce qu’on attend d’elle, afin de l’aider à se préparer en conséquence. J’examinerai également ses objectifs pour l’année afin de m’assurer que ce que je propose cadre bien avec son plan de perfectionnement professionnel."
          },
          exampleTaskResponse: {
            task:
              "- Répondre au courriel de Geneviève :\n     > lui proposer de former Mary à la synthèse de l’information provenant de sources multiples afin qu’elle puisse élargir ses compétences;\n     >	lui demander quelles sont ses attentes et quels sont les défis de son équipe afin que je puisse tenir compte de tous les facteurs pour déterminer comment son équipe pourrait profiter de l’expérience de Mary dans le domaine de la formation;\n     > l’informer que je travaille à recueillir plus d’information auprès de Mary, et que je lui ferai part de mes suggestions d’ici la fin de la semaine.\n- Organiser une réunion avec Mary pour discuter des objectifs de son affectation et veiller à ce qu’elle se sente motivée et à ce qu’elle sache ce qui est attendu d’elle.\n- Consulter les objectifs passés et actuels de Mary pour vérifier que ce que je propose est conforme à son plan de perfectionnement professionnel.",
            reasonsForAction:
              "Former Mary à la synthèse de l’information provenant de sources multiples serait avantageux pour mon équipe, qui a besoin de consolider l’information recueillie auprès de nombreuses sources. Demander à Geneviève quels sont ses attentes et ses défis m’aidera à mieux préparer Mary et à m’assurer que l’affectation sera avantageuse pour nos deux équipes."
          }
        },
        evaluation: {
          title: "Évaluation",
          bullet1:
            "Les mesures que vous prenez et les explications que vous donnez seront prises en compte dans l’évaluation de votre rendement pour chacune des compétences clés en leadership (décrites ci-dessous). On évaluera à quel point ces mesures et explications démontrent les compétences clés en leadership.",
          bullet2:
            "L’efficacité des mesures prises sera évaluée. Le niveau d’efficacité est déterminé par l’effet positif ou négatif que ces mesures auraient sur la résolution des situations présentées, et par l’étendue de cet effet.",
          bullet3:
            "Vos réponses seront également évaluées en fonction de leur contribution à l’atteinte des objectifs organisationnels présentés dans l’information contextuelle.",
          keyLeadershipCompetenciesSection: {
            title: "Compétences clés en leadership",
            para1Title: "Créer une vision et une stratégie : ",
            para1:
              "Les gestionnaires contribuent à définir l’avenir et à tracer la voie à suivre. Pour ce faire, ils tiennent compte du contexte. Ils mettent à contribution leurs connaissances. Ils obtiennent et intègrent de l’information provenant de diverses sources pour la mise en œuvre d’activités concrètes. Ils considèrent divers points de vue et consultent d’autres personnes, au besoin. Les gestionnaires assurent l’équilibre entre les priorités organisationnelles et contribuent à améliorer les résultats.",
            para2Title: "Mobiliser les personnes : ",
            para2:
              "Les gestionnaires inspirent et motivent les personnes qu’ils dirigent. Ils gèrent le rendement de leurs employés et leur offrent de la rétroaction constructive et respectueuse pour encourager et rendre possible l’excellence en matière de rendement. Ils dirigent en donnant l’exemple et se fixent des objectifs personnels qui sont plus exigeants que ceux qu’ils établissent pour les autres.",
            para3Title: "Préserver l’intégrité et le respect : ",
            para3:
              "Les gestionnaires donnent l’exemple sur le plan des pratiques éthiques, du professionnalisme et de l’intégrité personnelle, en agissant dans l’intérêt du Canada, des Canadiens et des Canadiennes. Ils créent des environnements de travail inclusifs, empreints de respect et de confiance, où les conseils judicieux sont valorisés. Ils encouragent les autres à faire part de leurs points de vue, tout en encourageant la collégialité.",
            para4Title: "Collaborer avec les partenaires et les intervenants : ",
            para4:
              "Les gestionnaires cherchent à obtenir, de façon délibérée et ingénieuse, un grand éventail de perspectives. Lorsqu’ils établissent des partenariats, ils gèrent les attentes et visent à atteindre un consensus. Ils font preuve d’ouverture et de souplesse afin d’améliorer les résultats et apportent une perspective globale de l’organisation à leurs interactions. Les gestionnaires reconnaissent le rôle des partenaires dans l’obtention des résultats.",
            para5Title: "Promouvoir l’innovation et orienter le changement : ",
            para5:
              "Les gestionnaires créent un environnement propice aux idées audacieuses, à l’expérimentation et à la prise de risques en toute connaissance de cause. Lors de la mise en œuvre d’un changement, ils maintiennent l’élan, surmontent la résistance et anticipent les conséquences. Ils perçoivent les revers comme une bonne occasion de comprendre et d’apprendre.",
            para6Title: "Obtenir des résultats : ",
            para6:
              "Les gestionnaires s’assurent de répondre aux objectifs de l’équipe en gérant les ressources. Ils prévoient, planifient et surveillent les progrès, et font des ajustements au besoin. Ils démontrent leur connaissance du contexte lors de la prise de décisions. Les gestionnaires assument la responsabilité personnelle à l’égard de leurs actions et des résultats de leurs décisions."
          }
        }
      },

      //Background Page
      background: {
        hiddenTabNameComplementary: "Sous « Information contextuelle »",
        orgCharts: {
          link: "Description de l'image",
          ariaLabel: "Description de l'image de l'Organigramme Équipe",
          treeViewInstructions:
            "Ci-dessous, vous trouverez la vue arborescente de l’organigramme. Une fois sélectionné, vous pouvez utiliser les touches fléchées pour la navigation, l’expansion et l’effondrement de l’information."
        }
      },

      //Inbox Page
      inboxPage: {
        tabName: "Sous « Boîte de réception »",
        emailId: " courriel ",
        subject: "Objet",
        to: "À",
        from: "De",
        date: "Date",
        addReply: "Ajouter une réponse par courriel",
        addTask: "Ajouter une réponse par liste de tâches",
        yourActions: `Vous avez répondu avec {0} courriel(s) et {1} liste(s) de tâches`,
        editActionDialog: {
          addEmail: "Ajouter une réponse par courriel",
          editEmail: "Modifier la réponse par courriel",
          addTask: "Ajouter à la liste de tâches",
          editTask: "Modifier la tâche",
          save: "Sauvegarder la réponse"
        },
        characterLimitReached: "(Limite atteinte)",
        emailCommons: {
          to: "À :",
          toFieldSelected: "Champ À choisi",
          cc: "Cc :",
          ccFieldSelected: "Champ CC choisi",
          currentSelectedPeople: "Les personnes choisies actuellement sont : {0}",
          currentSelectedPeopleAreNone: "Aucunes",
          reply: "répondre",
          replyAll: "répondre à tous",
          forward: "transmettre",
          editButton: "Modifier la réponse",
          deleteButton: "Supprimer la réponse",
          originalEmail: "Courriel d’origine",
          toAndCcFieldsPlaceholder: "Sélectionnez à partir du carnet d’adresses",
          yourResponse: "Votre réponse"
        },
        addEmailResponse: {
          selectResponseType:
            "Veuillez choisir la manière dont vous souhaitez répondre au courriel d’origine :",
          response: "Votre réponse par courriel : limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif) : limite de {0} caractères",
          emailResponseTooltip: "Rédiger une réponse au courriel que vous avez reçu.",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires.",
          invalidToFieldError: "Ce champ ne peut être vide"
        },
        emailResponse: {
          title: "Réponse par courriel no. ",
          description: "Pour cette réponse, vous avez choisi de :",
          response: "Votre réponse par courriel :",
          reasonsForAction: "Votre justification des mesures prises (facultatif) :"
        },
        addEmailTask: {
          header: "Courriel no {0}: {1}",
          task: "Votre réponse par liste de tâches : limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif) : limite de {0} caractères"
        },
        taskContent: {
          title: "Réponse par liste de tâches no. ",
          task: "Votre réponse par liste de tâches :",
          taskTooltipPart1:
            "Une action que vous comptez prendre pour résoudre une situation dans les courriels.",
          taskTooltipPart2:
            "Exemple : planifier une réunion, demander de l'information à un collègue.",
          reasonsForAction: "Votre justification des mesures prises (facultatif) :",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires."
        },
        deleteResponseConfirmation: {
          title: "Êtes-vous certain de vouloir annuler cette réponse?",
          systemMessageTitle: "Avertissement!",
          systemMessageDescription:
            "Votre réponse ne sera pas sauvegardée si vous continuez. Si vous souhaitez enregistrer votre réponse, vous pouvez y retourner. Toutes vos réponses peuvent être modifiées ou supprimées avant de soumettre le test.",
          description:
            "Si vous ne voulez pas sauvegarder la réponse, cliquez sur le bouton « Supprimer la réponse »."
        }
      },

      //Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Félicitations! Votre test a été soumis.",
        feedbackSurvey:
          "Nous aimerions recevoir vos commentaires sur l’évaluation. Veuillez remplir ce {0} facultatif avant de vous déconnecter et de quitter.",
        optionalSurvey: "sondage de 15 minutes",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=fr-CA",
        logout:
          "Pour des raisons de sécurité, assurez-vous de fermer votre session dans le coin supérieur droit de cette page. Vous pouvez discrètement recueillir vos effets personnels et quitter la séance de test. Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}.",
        thankYou: "Nous vous remercions d’avoir terminé votre évaluation. Bonne chance!"
      },

      //Quit Confirmation Page
      quitConfirmationPage: {
        title: "Vous avez quitté le test",
        instructionsTestNotScored1: "Votre test ",
        instructionsTestNotScored2: "ne sera pas corrigé.",
        instructionsRaiseHand:
          "Veuillez lever la main. L’administrateur de tests viendra vous donner d’autres directives.",
        instructionsEmail:
          "Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}."
      },

      //Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Votre temps de test est écoulé...",
        timeoutSaved:
          "Vos réponses ont été sauvegardées et soumises aux fins de notation. Veuillez prendre note que l’information dans le bloc-notes n’est pas sauvegardée.",
        timeoutIssue:
          "S’il y a eu un problème, veuillez en informer votre administrateur de test. Cliquez sur « Continuer » pour quitter cette séance de test et pour recevoir d’autres instructions."
      },

      //Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Information contextuelle",
        inboxTabTitle: "Boîte de réception",
        disabled: "Vous ne pouvez accéder à la boîte de courriel avant d'avoir commencé le test.",
        nextButton: "Suivant",
        previousButton: "Précédent"
      },

      //Test Footer
      testFooter: {
        timer: {
          timer: "Minuterie",
          timeLeft: "Temps restant dans la séance de test :",
          timerHidden: "Minuterie cachée."
        },
        submitTestPopupBox: {
          title: "Confirmer l’envoi du test?",
          warning: {
            title: "Avertissement : your notebook will not be saved.",
            message:
              "Le contenu du bloc-notes ne sera pas soumis pour la notation. Assurez-vous d’avoir examiné toutes vos réponses avant de soumettre le test puisque vous ne serez pas en mesure d’y retourner pour apporter des changements."
          },
          description:
            "Si vous êtes prêt(e) à soumettre votre test pour la notation, cliquez sur le bouton « Soumettre le test ». La séance de test sera fermée et vous recevrez d’autres instructions."
        },
        quitTestPopupBox: {
          title: "Souhaitez-vous mettre fin à cette séance de test?",
          description:
            "Vous ne pourrez pas récupérer vos réponses et n’aurez plus accès à la séance de test. Ce faisant, vous affirmez et reconnaissez :",
          checkboxOne: "Je me retire volontairement de ce test;",
          checkboxTwo: "Mon test ne sera pas noté;",
          checkboxThree:
            "Je suis conscient(e) que la période d'attente pour ce test peut s’appliquer, si je veux écrire ce test de nouveau dans le futur."
        }
      }
    },

    //Screen Reader
    ariaLabel: {
      mainMenu: "Menu Principal",
      tabMenu: "Menu des onglets de la BRG-e",
      instructionsMenu: "Menu des instructions",
      languageToggleBtn: "bouton-de-langue-a-bascule",
      authenticationMenu: "Menu d'authentification",
      emailHeader: "en-tête du courriel",
      responseDetails: "détails de la réponse",
      reasonsForActionDetails: "motifs de l'action",
      taskDetails: "détails sur la ou les tâches",
      emailOptions: "options de messagerie",
      taskOptions: "options de tâche",
      taskTooltip: "infobulle de tâche",
      emailResponseTooltip: "Infobulle pour les réponses par courriel",
      reasonsForActionTooltip: "infobulle des motifs de l'action",
      passwordConfirmationRequirements: "Il doit correspondre à votre mot de passe",
      dobDayField: "Champ Journée sélectionné",
      dobMonthField: "Champ Mois sélectionné",
      dobYearField: "Champ Année sélectionné",
      emailsList: "Liste des courriels",
      topNavigationSection: "Barre de navigation du haut",
      sideNavigationSection: "Barre de navigation du côté",
      notepadSection: "Section bloc-notes",
      quitTest: "Quitter de test"
    },

    //Commons
    commons: {
      psc: "Commission de la fonction publique",
      nextButton: "Suivant",
      backButton: "Retour",
      enterEmibSample: "Passer à l’échantillon du test de la BRG-e",
      enterEmibReal: "Passer aux instructions du test",
      resumeEmibReal: "Rentrez la BRG-e",
      startTest: "Commencer le test",
      resumeTest: "Reprendre le test",
      confirmStartTest: {
        aboutToStart: "Vous êtes sur le point de commencer le test.",
        timerWarning:
          "Après avoir cliqué sur commencer, vous serez dirigé vers l’onglet « Information contextuelle ». Vous aurez {0} pour terminer le test.",
        instructionsAccess:
          "Vous aurez accès aux instructions et à votre bloc-notes durant le test. Bonne chance!",
        timeUnlimited: "temps illimité",
        numberMinutes: "{0} minutes"
      },
      submitTestButton: "Soumettre le test",
      quitTest: "Quitter le test",
      returnToTest: "Retourner au test",
      returnToResponse: "Retourner à la réponse",
      passStatus: "Réussi",
      failStatus: "Échoue",
      enabled: "Activé",
      disabled: "Désactivé",
      backToTop: "Haut de la page",
      notepad: {
        title: "Bloc-notes",
        placeholder: "Mettez vos notes ici..."
      },
      cancel: "Annuler",
      cancelChanges: "Annuler les modifications",
      cancelResponse: "Annuler la réponse",
      close: "Fermer",
      login: "Connexion",
      logout: "Déconnexion",
      ok: "Ok",
      continue: "Continuer"
    }
  }
});

export default LOCALIZE;

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import EditEmail from "./EditEmail";
import EditTask from "./EditTask";
import { ACTION_TYPE, EDIT_MODE, actionShape, emailShape, EMAIL_TYPE } from "./constants";
import EmailContent from "./EmailContent";
import CollapsingItemContainer, { ICON_TYPE } from "./CollapsingItemContainer";
import {
  addEmail,
  addTask,
  updateEmail,
  updateTask,
  readEmail
} from "../../modules/EmibInboxRedux";
import Modal from "react-modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faTasks } from "@fortawesome/free-solid-svg-icons";

const customStyles = {
  content: {
    maxWidth: 900,
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#F5FAFB",
    overflow: "initial",
    padding: 0,
    borderColor: "#00565E",
    borderRadius: 2
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)"
  }
};

const styles = {
  header: {
    height: 65,
    padding: "15px 15px 5px 15px",
    backgroundColor: "#00565e",
    color: "#FFFFFF",
    borderRadiusTopLeft: 2,
    borderRadiusTopRight: 2
  },
  title: {
    color: "#FFFFFF",
    fontSize: 28
  },
  container: {
    maxHeight: "calc(100vh - 300px)",
    overflow: "auto",
    width: 700,
    padding: "10px 15px",
    borderBottom: "1px solid #CECECE",
    borderTop: "1px solid #CECECE"
  },
  footer: {
    padding: 15,
    height: 70
  },
  originalEmail: {
    padding: 12,
    marginRight: 12,
    border: "1px #00565E solid",
    borderRadius: 4
  },
  icon: {
    float: "left",
    marginTop: 10,
    marginRight: 8,
    fontSize: 24
  },
  leftButton: {
    margin: 4
  },
  rightButton: {
    float: "right",
    margin: 4
  },
  dataBodyDivider: {
    width: "100%",
    borderTop: "1px solid #96a8b2",
    margin: 0
  }
};

class EditActionDialog extends Component {
  static propTypes = {
    email: emailShape,
    showDialog: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    actionType: PropTypes.oneOf(Object.keys(ACTION_TYPE)).isRequired,
    editMode: PropTypes.oneOf(Object.keys(EDIT_MODE)).isRequired,
    // Provided from Redux.
    addEmail: PropTypes.func.isRequired,
    addTask: PropTypes.func.isRequired,
    updateEmail: PropTypes.func.isRequired,
    updateTask: PropTypes.func.isRequired,
    readEmail: PropTypes.func.isRequired,
    // Only needed when updating an existing one
    action: actionShape,
    actionId: PropTypes.number
  };

  state = {
    action: {
      ...this.props.action,
      ...{ emailType: this.defaultEmailType() }
    },
    showCancelConfirmationDialog: false,
    toFieldValid: true,
    triggerPropsUpdate: false
  };

  // function to check if there already is an emailType or default to reply if there isn't one
  defaultEmailType() {
    // if a task, return nothing
    if (this.props.actionType === ACTION_TYPE.task) {
      return undefined;
    }
    // if an email with no action, return reply
    if (this.props.action === undefined) {
      return EMAIL_TYPE.reply;
    }
    // if an email with no action.emailType, return reply
    if (this.props.action.emailType === undefined) {
      return EMAIL_TYPE.reply;
    }
    // otherwise, return the emailType
    return this.props.action.emailType;
  }

  handleSave = () => {
    this.triggerPropsUpdate();
    // determine which function to call depening on action type and edit mode
    if (
      this.props.actionType === ACTION_TYPE.email &&
      this.props.editMode === EDIT_MODE.create &&
      this.state.action.emailTo.length !== 0
    ) {
      this.props.addEmail(this.props.email.id, this.state.action, this.props.dispatch);
      this.props.readEmail(this.props.email.id);
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else if (
      this.props.actionType === ACTION_TYPE.task &&
      this.props.editMode === EDIT_MODE.create
    ) {
      this.props.addTask(this.props.email.id, this.state.action, this.props.dispatch);
      this.props.readEmail(this.props.email.id);
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else if (
      this.props.actionType === ACTION_TYPE.email &&
      this.props.editMode === EDIT_MODE.update &&
      this.state.action.emailTo.length !== 0
    ) {
      this.props.updateEmail(this.props.email.id, this.props.actionId, this.state.action);
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else if (
      this.props.actionType === ACTION_TYPE.task &&
      this.props.editMode === EDIT_MODE.update
    ) {
      this.props.updateTask(this.props.email.id, this.props.actionId, this.state.action);
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else {
      this.setState({ toFieldValid: false });
    }
  };

  // triggering props update (need that trigger in EditEmail.jsx component)
  triggerPropsUpdate = () => {
    this.setState({ triggerPropsUpdate: !this.state.triggerPropsUpdate });
  };

  // updatedAction is the PropType shape actionShape and represents a single action a candidate takes on an email
  editAction = updatedAction => {
    this.setState({ action: updatedAction });
  };

  closeCancelConfirmationDialog = () => {
    this.setState({ showCancelConfirmationDialog: false });
  };

  // this function is called when 'Cancel response' button is selected from the cancel confirmation message dialog
  handleClose = event => {
    const escapeKeyCode = 27;
    // prevent closing on 'escape' key press
    if (event.type === "keydown" && event.keyCode === escapeKeyCode) {
      return;
    } else {
      // resetting all current form variables at their original values from the props
      this.setState({ action: { ...this.props.action }, toFieldValid: true });
      // closing response action dialog
      this.props.handleClose();
    }
  };

  render() {
    const { showDialog, actionType, editMode } = this.props;
    // If a root node exists, the app is being served, otherwise it's a unit test.
    let ariaHideApp = true;
    if (document.getElementById("#root")) {
      Modal.setAppElement("#root");
    } else {
      // Unit tests do not consider outside of the dialog.
      ariaHideApp = false;
    }
    return (
      <Modal
        isOpen={showDialog}
        onRequestClose={this.handleClose}
        style={customStyles}
        shouldCloseOnOverlayClick={false}
        ariaHideApp={ariaHideApp}
      >
        {actionType === ACTION_TYPE.email && (
          <div style={styles.header}>
            <FontAwesomeIcon style={styles.icon} icon={faEnvelope} />
            <h1 style={styles.title}>
              {editMode === EDIT_MODE.create &&
                LOCALIZE.emibTest.inboxPage.editActionDialog.addEmail}
              {editMode === EDIT_MODE.update &&
                LOCALIZE.emibTest.inboxPage.editActionDialog.editEmail}
            </h1>
          </div>
        )}
        {actionType === ACTION_TYPE.task && (
          <div style={styles.header}>
            <FontAwesomeIcon style={styles.icon} icon={faTasks} />
            <h1 style={styles.title}>
              {editMode === EDIT_MODE.create &&
                LOCALIZE.emibTest.inboxPage.editActionDialog.addTask}
              {editMode === EDIT_MODE.update &&
                LOCALIZE.emibTest.inboxPage.editActionDialog.editTask}
            </h1>
          </div>
        )}

        <div style={styles.container}>
          <CollapsingItemContainer
            title={LOCALIZE.emibTest.inboxPage.emailCommons.originalEmail}
            body={<EmailContent email={this.props.email} />}
            iconType={ICON_TYPE.email}
          />
          <h2>{LOCALIZE.emibTest.inboxPage.emailCommons.yourResponse}</h2>
          {actionType === ACTION_TYPE.email && (
            <EditEmail
              onChange={this.editAction}
              action={editMode === EDIT_MODE.update ? this.props.action : null}
              originalFrom={this.props.email.from}
              originalTo={this.props.email.to}
              originalCC={this.props.email.cc ? this.props.email.cc : ""}
              editMode={editMode}
              toFieldValid={this.state.toFieldValid}
              triggerPropsUpdate={this.state.triggerPropsUpdate}
            />
          )}
          {actionType === ACTION_TYPE.task && (
            <EditTask
              emailNumber={this.props.email.id}
              emailSubject={this.props.email.subject}
              onChange={this.editAction}
              action={editMode === EDIT_MODE.update ? this.props.action : null}
            />
          )}
        </div>

        <div style={styles.footer}>
          <button style={styles.leftButton} className={"btn btn-danger"} onClick={this.handleClose}>
            {LOCALIZE.commons.cancelResponse}
          </button>
          <button
            style={styles.rightButton}
            id="unit-test-email-response-button"
            type="button"
            className="btn btn-primary"
            onClick={this.handleSave}
          >
            {LOCALIZE.emibTest.inboxPage.editActionDialog.save}
          </button>
        </div>
      </Modal>
    );
  }
}

export { EditActionDialog as UnconnectedEditActionDialog };

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addEmail,
      addTask,
      updateEmail,
      updateTask,
      readEmail,
      dispatch
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(EditActionDialog);

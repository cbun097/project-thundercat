import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";
import LOCALIZE from "../../text_resources";
import ImageZoom from "react-medium-image-zoom";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import "../../css/react-medium-image-zoom.css";
import TreeNode from "../commons/TreeNode";
import { processTreeContent } from "../../helpers/transformations";
import { LANGUAGES } from "../../modules/LocalizeRedux";

const styles = {
  containerWidth: {
    width: "100%"
  },
  testImage: {
    width: "100%",
    maxWidth: 600
  },
  button: {
    margin: "12px 0 12px 5px"
  }
};

class BackgroundSection extends Component {
  static propTypes = {
    content: PropTypes.array,
    currentLanguage: PropTypes.string,
    testNameId: PropTypes.string.isRequired,
    numberOfOrgChartsArray: PropTypes.array.isRequired
  };

  state = {
    showPopupBox1: false,
    showPopupBox2: false
  };

  // open selected popup box based multipleOrgChart and isOrganizationalStructureOrgChart values
  openPopup = isOrganizationalStructureOrgChart => {
    // if there is more than one org chart in the specified page
    if (this.multipleOrgChart()) {
      if (isOrganizationalStructureOrgChart) {
        this.setState({ showPopupBox1: true, showPopupBox2: false });
      } else if (!isOrganizationalStructureOrgChart) {
        this.setState({ showPopupBox1: false, showPopupBox2: true });
      }
      // if there is only one org chart in the specified page
    } else {
      this.setState({ showPopupBox1: true });
    }
  };

  // close all popup boxes
  closePopup = () => {
    this.setState({ showPopupBox1: false, showPopupBox2: false });
  };

  // checks if there is more than one org chart per page
  // if so return true, else false
  multipleOrgChart = () => {
    const { numberOfOrgChartsArray } = this.props;
    // for each element in the array
    for (let i = 0; i < numberOfOrgChartsArray.length; i++) {
      // if value is greater than 1, returns true
      if (numberOfOrgChartsArray[i] > 1) {
        return true;
      } else {
        return false;
      }
    }
  };

  render() {
    const { content, currentLanguage, testNameId } = this.props;
    return (
      <div style={styles.containerWidth}>
        {content.map((contentItem, id) => {
          if (contentItem.type === "markdown") {
            // Markdown sections are rendered using ReactMarkdown.
            return <ReactMarkdown key={id} source={contentItem.text} />;
          } else if (contentItem.type === "tree_view") {
            // Process the tree content.
            // For now, the treeType value is based on two available org charts (organizational structure & team information)
            // If we add more treeType / org chart, this code will need to be updated
            let treeType = contentItem.organizational_structure_tree_child
              ? "organizational_structure_tree_child"
              : "team_information_tree_child";
            // saving different tree view content in variables
            let treeView1 = [];
            let treeView2 = [];
            // if there is more than one org chart in the specified page
            if (this.multipleOrgChart()) {
              // if contentItem = organizational structure
              if (contentItem.organizational_structure_tree_child) {
                treeView1 = processTreeContent(contentItem[treeType], treeType);
              }

              // if contentItem = team information
              if (!contentItem.organizational_structure_tree_child) {
                treeView2 = processTreeContent(contentItem[treeType], treeType);
              }
              // else if there is only one org chart in the specified page
            } else {
              treeView1 = processTreeContent(contentItem[treeType], treeType);
            }

            // Determine what image to use.
            // TODO(caleybrock) - move these images to a secure file server like S3.
            // They are currently stored in the public folder which means
            // you don't need to have any authentication to access org chart images.
            let imageSource = `/orgCharts/${testNameId}/`;
            if (treeType === "organizational_structure_tree_child") {
              if (currentLanguage === LANGUAGES.english) {
                imageSource = imageSource + "org_chart_en.png";
              } else {
                imageSource = imageSource + "org_chart_fr.png";
              }
            } else if (treeType === "team_information_tree_child") {
              if (currentLanguage === LANGUAGES.english) {
                imageSource = imageSource + "team_chart_en.png";
              } else {
                imageSource = imageSource + "team_chart_fr.png";
              }
            }

            // Tree Views are org charts and require
            // - a title
            // - an image, that's expandable
            // - an image description button
            // - a tree view image description
            return (
              <div key={id}>
                <ImageZoom
                  image={{
                    src: imageSource,
                    alt: contentItem.title,
                    style: styles.testImage,
                    className: "ie-zoom-cursor"
                  }}
                  zoomImage={{
                    src: imageSource,
                    alt: contentItem.title,
                    className: "ie-zoom-cursor"
                  }}
                />
                <button
                  id="org-image-description"
                  onClick={() => this.openPopup(contentItem.organizational_structure_tree_child)}
                  className="btn btn-secondary"
                  style={styles.button}
                  aria-label={LOCALIZE.emibTest.background.orgCharts.ariaLabel}
                >
                  {LOCALIZE.emibTest.background.orgCharts.link}
                </button>
                <PopupBox
                  show={
                    !this.multipleOrgChart()
                      ? this.state.showPopupBox1
                      : contentItem.organizational_structure_tree_child
                      ? this.state.showPopupBox1
                      : this.state.showPopupBox2
                  }
                  handleClose={this.closePopup}
                  title={contentItem.title}
                  description={
                    <div>
                      <p>{LOCALIZE.emibTest.background.orgCharts.treeViewInstructions}</p>
                      <TreeNode
                        nodes={
                          !this.multipleOrgChart()
                            ? treeView1
                            : contentItem.organizational_structure_tree_child
                            ? treeView1
                            : treeView2
                        }
                      />
                    </div>
                  }
                  rightButtonType={BUTTON_TYPE.secondary}
                  rightButtonTitle={LOCALIZE.commons.close}
                />
              </div>
            );
          } else {
            // Unknown content type.
            return <div />;
          }
        })}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testNameId: state.loadTestContent.testMetaData.test_internal_name
  };
};

export default connect(
  mapStateToProps,
  null
)(BackgroundSection);

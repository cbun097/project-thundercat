import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter, Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { PATH } from "../commons/Constants";
import { TEST_DEFINITION } from "../../testDefinition";
import SampleTestsDashboard from "./SampleTestsDashboard";
import EmibRoutes from "../eMIB/EmibRoutes";
import {
  activateTest,
  startTest,
  deactivateTest,
  setCurrentTest,
  timeoutTest,
  quitTest,
  setCurrentPage,
  resetTestStatusState
} from "../../modules/SampleTestStatusRedux";
import { BANNER_STYLE } from "../eMIB/constants";

class SampleTestsRoutes extends Component {
  static propTypes = {
    testNameId: PropTypes.string,
    //provided by redux
    activateTest: PropTypes.func,
    startTest: PropTypes.func,
    deactivateTest: PropTypes.func,
    setCurrentTest: PropTypes.func,
    timeoutTest: PropTypes.func,
    quitTest: PropTypes.func,
    isTestActive: PropTypes.bool,
    currentPage: PropTypes.string,
    setCurrentPage: PropTypes.func
  };

  componentDidMount = () => {};

  render() {
    return (
      <Switch>
        <Route exact path={this.props.match.url} component={SampleTestsDashboard} />
        <Route
          path={`${this.props.match.url}${PATH.emibSampleTest}`}
          component={() => (
            <EmibRoutes
              {...this.props}
              testNameId={TEST_DEFINITION.emib.sampleTest}
              updateBackendTest={() => {}}
              timeLimit={null}
              isSampleTest={true}
              headerFooterPX={BANNER_STYLE}
              logoutAction={() => {}}
              submitPath={PATH.sampleTests}
            />
          )}
        />
        <Redirect to={this.props.match.url} />
      </Switch>
    );
  }
}

export { SampleTestsRoutes as UnconnectedSampleTestsRoutes };

const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.sampleTestStatus.isTestActive,
    isTestStarted: state.sampleTestStatus.isTestStarted,
    startTime: state.sampleTestStatus.startTime,
    currentPage: state.sampleTestStatus.currentPage
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      activateTest,
      startTest,
      quitTest,
      deactivateTest,
      timeoutTest,
      setCurrentTest,
      setCurrentPage,
      resetTestStatusState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SampleTestsRoutes));

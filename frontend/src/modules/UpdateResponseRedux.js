import { ACTION_TYPE } from "../components/eMIB/constants";
import { setResponseDbId } from "./EmibInboxRedux";

function setAssignedQuestions(assigned_test_id, question_ids) {
  return async function() {
    // if there is no assigned test id, then it is likely a sample test; do not create assigned questions
    if (assigned_test_id === null || assigned_test_id === "null") {
      return;
    }
    let tests = await fetch(
      "/api/set-assigned-questions" +
        `?assigned_test_id=${assigned_test_id}&question_ids=${question_ids}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + localStorage.getItem("auth_token"),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let testsJson = await tests.json();
    return testsJson;
  };
}

// Async call to save the response and then update EmibInboxRedux's state
async function addEmailResponse(assigned_question_id, emailIndex, newActionIndex, email, dispatch) {
  // if there is no assigned question id, then it is likely a sample test; do not add response
  if (assigned_question_id === undefined) {
    return;
  }
  let tests = await fetch(
    "/api/add-response" +
      `?assigned_question_id=${assigned_question_id}&response_type=${ACTION_TYPE.email}&to=${
        email.emailTo
      }&cc=${email.emailCc}&response=${encodeURIComponent(
        email.emailBody
      )}&reason=${encodeURIComponent(email.reasonsForAction)}`,
    {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    }
  );
  let response = await tests.json();
  if (response["response_id"] === undefined) {
    return;
  }
  let trueId = response["response_id"];
  return dispatch(setResponseDbId(emailIndex, newActionIndex, trueId));
}

async function addTaskResponse(assigned_question_id, emailIndex, newActionIndex, task, dispatch) {
  // if there is no assigned question id, then it is likely a sample test; do not add response
  if (assigned_question_id === undefined) {
    return;
  }
  let tests = await fetch(
    "/api/add-response" +
      `?assigned_question_id=${assigned_question_id}&response_type=${
        ACTION_TYPE.task
      }&task=${encodeURIComponent(task.task)}&reason=${encodeURIComponent(task.reasonsForAction)}`,
    {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    }
  );
  let response = await tests.json();
  if (response["response_id"] === undefined) {
    return;
  }
  let trueId = response["response_id"];
  return dispatch(setResponseDbId(emailIndex, newActionIndex, trueId));
}

async function updateEmailResponse(response_id, email) {
  // if there is no assigned response id, then it is likely a sample test; do not update response
  if (response_id === undefined) {
    return;
  }
  const time = Date.now();
  let tests = await fetch(
    "/api/edit-response" +
      `?response_id=${response_id}&response_type=${ACTION_TYPE.email}&to=${email.emailTo}&cc=${
        email.emailCc
      }&response=${encodeURIComponent(email.emailBody)}&reason=${encodeURIComponent(
        email.reasonsForAction
      )}&time=${time}`,
    {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    }
  );
  let response = await tests.json();
  return response;
}

async function updateTaskResponse(response_id, task) {
  // if there is no assigned response id, then it is likely a sample test; do not update response
  if (response_id === undefined) {
    return;
  }
  const time = Date.now();
  let tests = await fetch(
    "/api/edit-response" +
      `?response_id=${response_id}&response_type=${ACTION_TYPE.task}&task=${encodeURIComponent(
        task.task
      )}&reason=${encodeURIComponent(task.reasonsForAction)}&time=${time}`,
    {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    }
  );
  let response = await tests.json();
  return response;
}

async function deleteEmailResponse(response_id) {
  // if there is no assigned response id, then it is likely a sample test; do not delete response
  if (response_id === undefined) {
    return;
  }
  let tests = await fetch(
    `/api/delete-response?response_id=${response_id}&response_type=${ACTION_TYPE.email}`,
    {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    }
  );
  let response = await tests.json();
  return response;
}

async function deleteTaskResponse(response_id) {
  // if there is no assigned response id, then it is likely a sample test; do not delete response
  if (response_id === undefined) {
    return;
  }
  let tests = await fetch(
    `/api/delete-response?response_id=${response_id}&response_type=${ACTION_TYPE.task}`,
    {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    }
  );
  let response = await tests.json();
  return response;
}

function getResponses(assigned_test_id) {
  return async function() {
    let tests = await fetch(`/api/get-responses?assigned_test_id=${assigned_test_id}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let response = await tests.json();
    return response;
  };
}

export {
  setAssignedQuestions,
  addEmailResponse,
  addTaskResponse,
  updateEmailResponse,
  updateTaskResponse,
  deleteEmailResponse,
  deleteTaskResponse,
  getResponses
};

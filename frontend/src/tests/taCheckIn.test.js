import React from "react";
import { shallow } from "enzyme";
import { UnconnectedTestAdministration } from "../TestAdministration";
import LOCALIZE from "../text_resources";

describe("renders right wording depending on the checkedIn state", () => {
  const wrapper = shallow(<UnconnectedTestAdministration />, { disableLifecycleMethods: true });
  it("when checkedIn is set to false)", () => {
    wrapper.setState({ checkedIn: false });
    const rawText = <p>{LOCALIZE.testAdministration.checkedOutDescriptionPart1}</p>;
    expect(wrapper.contains(rawText)).toEqual(true);
  });

  it("when checkedIn is set to true)", () => {
    wrapper.setState({ checkedIn: true });
    const rawText = <p>{LOCALIZE.testAdministration.checkedInDescriptionPart1}</p>;
    expect(wrapper.contains(rawText)).toEqual(true);
  });
});

describe("update states on button click", () => {
  it("set showDialog state to true when selecting check-in button)", () => {
    const wrapper = shallow(<UnconnectedTestAdministration />, { disableLifecycleMethods: true });
    wrapper.find("#unit-test-check-in-button").simulate("click");
    expect(wrapper.state("showDialog")).toBe(true);
  });
});

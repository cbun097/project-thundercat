import React from "react";
import { generateToIds, UnconnectedEditEmail } from "../../../components/eMIB/EditEmail";
import { Provider } from "react-redux";
import { EDIT_MODE } from "../../../components/eMIB/constants";
import { mount } from "enzyme";
import configureStore from "redux-mock-store";

const addressBook = [
  { label: "Joe (Developer)", value: 0 },
  { label: "Bob (Developer)", value: 1 },
  { label: "Smithers (Butler)", value: 2 },
  { label: "Arthur (King of Britain)", value: 3 },
  { label: "Richard (Lionheart)", value: 4 },
  { label: "Robert (The Bruce)", value: 5 }
];

const initialState = {
  state: {
    loadTestContent: {
      addressBook: {
        en: addressBook,
        fr: addressBook
      }
    },
    localize: {
      language: "en"
    }
  }
};

const mockStore = configureStore();

describe("renders component's content", () => {
  const addressBook = [
    { label: "Sherlock Holmes (Consulting Detective)", value: 300 },
    { label: "Jean Luc Picard (Captain, U.S.S. Enterpise, Starfleet)", value: 301 },
    { label: "Arthur Pendragon (King of England)", value: 302 }
  ];

  it("returns empty list when empty", () => {
    expect(generateToIds("", addressBook)).toEqual([]);
  });

  it("renders empty list when name does not exist in the book", () => {
    expect(generateToIds("The Outsider", addressBook)).toEqual([]);
    expect(generateToIds("Corvo Attano (Assassin, Dishonored)", addressBook)).toEqual([]);
    expect(generateToIds("The Outsider; Corvo Attano (Assassin, Dishonored)", addressBook)).toEqual(
      []
    );
  });

  it("renders one name when there is one", () => {
    const names = addressBook[0].label;
    const ret = [addressBook[0].value];
    expect(generateToIds(names, addressBook)).toEqual(ret);
  });

  it("renders two name when there are two", () => {
    const names = addressBook[0].label + "; " + addressBook[1].label;
    const ret = [addressBook[0].value, addressBook[1].value];
    expect(generateToIds(names, addressBook)).toEqual(ret);
  });

  it("renders three name when there are three", () => {
    const names = addressBook[0].label + "; " + addressBook[1].label + "; " + addressBook[2].label;
    const ret = [addressBook[0].value, addressBook[1].value, addressBook[2].value];
    expect(generateToIds(names, addressBook)).toEqual(ret);
  });
});

describe("renders EditEmail component", () => {
  it("cc field is unpopulated when there is no original cc", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEditEmail
          onChange={() => {}}
          action={null}
          originalFrom={"Joe (Developer)"}
          originalTo={"Bob (Developer)"}
          originalCC={""}
          editMode={EDIT_MODE.create}
          toFieldValid={true}
          triggerPropsUpdate={true}
          addressBook={addressBook}
        ></UnconnectedEditEmail>
      </Provider>
    );
    wrapper.find("#reply-all-radio").simulate("change");
    expect(wrapper.find(UnconnectedEditEmail).instance().state).toEqual({
      emailBody: "",
      emailCc: [],
      emailCcSelectedValues: [],
      emailTo: [1, 0],
      emailToSelectedValues: [],
      emailType: "replyAll",
      reasonsForAction: ""
    });
  });

  it("cc field is populated when there is an original cc", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEditEmail
          onChange={() => {}}
          action={null}
          originalFrom={"Joe (Developer)"}
          originalTo={"Bob (Developer)"}
          originalCC={"Arthur (King of Britain)"}
          editMode={EDIT_MODE.create}
          toFieldValid={true}
          triggerPropsUpdate={true}
          addressBook={addressBook}
        ></UnconnectedEditEmail>
      </Provider>
    );
    wrapper.find("#reply-all-radio").simulate("change");
    expect(wrapper.find(UnconnectedEditEmail).instance().state).toEqual({
      emailBody: "",
      emailCc: [3],
      emailCcSelectedValues: [],
      emailTo: [1, 0],
      emailToSelectedValues: [],
      emailType: "replyAll",
      reasonsForAction: ""
    });
  });
});

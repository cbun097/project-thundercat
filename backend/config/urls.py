from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from backend.views.testStatus import update_test_status
from backend.views import (
    views,
    database_check_view,
    assigned_tests_view,
    room_check_in_view,
)
from cms.views import (
    test_questions_view,
    retrieve_test_information_view,
    test_meta_data_view,
    test_permissions,
)
from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token,
)

from backend.views import test_response_view, test_access_code
from user_management.views import permissions

schema_view = get_swagger_view(title="ThunderCAT APIs")

router = routers.DefaultRouter()
router.register(r"api/database-check", database_check_view.DatabaseViewSet)

urlpatterns = [
    url(r"^$", schema_view),
    url(r"^api/admin/", admin.site.urls),
    url(r"^api/auth/", include("djoser.urls")),
    url(r"^api/auth/", include("djoser.urls.authtoken")),
    path(r"api/backend-status", views.index, name="index"),
    path("", include(router.urls)),
    url(r"^api/auth/", include("rest_framework.urls", namespace="rest_framework")),
    url(r"^api/auth/jwt/create_token/", obtain_jwt_token),
    url(r"^api/auth/jwt/refresh_token/", refresh_jwt_token),
    url(r"^api/auth/jwt/verify_token/", verify_jwt_token),
    url(r"^api/test-meta-data", test_meta_data_view.TestMetaDataSet.as_view()),
    url(
        r"^api/get-non-public-tests",
        retrieve_test_information_view.GetNonPublicTests.as_view(),
    ),
    url(r"^api/get-test-name", retrieve_test_information_view.GetTestName.as_view()),
    url(
        r"^api/get-test-internal-name",
        retrieve_test_information_view.GetTestInternalName.as_view(),
    ),
    url(r"^api/test-questions", test_questions_view.TestQuestionsSet.as_view()),
    url(r"^api/assigned-tests", assigned_tests_view.AssignedTestsSet.as_view()),
    url(r"^api/assign-test", assigned_tests_view.AssignTest.as_view()),
    url(
        r"^api/historical-assigned-tests",
        assigned_tests_view.HistoricalAssignedTests.as_view(),
    ),
    url(r"^api/activate-test", update_test_status.ActivateTest.as_view()),
    url(r"^api/submit-test", update_test_status.SubmitTest.as_view()),
    url(r"^api/quit-test", update_test_status.QuitTest.as_view()),
    url(
        r"^api/expire-test-inactivity",
        update_test_status.ExpireTestInactivity.as_view(),
    ),
    url(r"^api/timeout-test", update_test_status.SubmitTestTimeout.as_view()),
    url(
        r"^api/set-assigned-questions",
        test_response_view.SetAssignedQuestions.as_view(),
    ),
    url(r"^api/add-response", test_response_view.AddTestResponse.as_view()),
    url(r"^api/edit-response", test_response_view.EditTestResponse.as_view()),
    url(r"^api/delete-response", test_response_view.DeleteTestResponse.as_view()),
    url(r"^api/get-responses", test_response_view.GetTestResponses.as_view()),
    url(r"^api/get-user-permissions", permissions.GetUserPermissions.as_view()),
    url(r"^api/grant-test-permission", test_permissions.GrantTestPermission.as_view()),
    url(r"^api/get-test-permissions", test_permissions.GetTestPermissions.as_view()),
    url(
        r"^api/get-new-test-access-code",
        test_access_code.GetNewTestAccessCode.as_view(),
    ),
    url(
        r"^api/delete-test-access-code", test_access_code.DeleteTestAccessCode.as_view()
    ),
    url(
        r"^api/get-active-test_access_code",
        test_access_code.getActiveTestAccessCodeData.as_view(),
    ),
    url(r"^api/room-check-in", room_check_in_view.RoomCheckIn.as_view()),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [url(r"^__debug__/", include(debug_toolbar.urls))]

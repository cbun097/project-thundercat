from django.db import models
from user_management.user_management_models.user_models import User
from cms.cms_models.test import Test

##################################################################################
# TEST PERMISSIONS MODEL
##################################################################################


class TestPermissions(models.Model):
    username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=False
    )
    test = models.ForeignKey(
        Test, to_field="test_name", on_delete=models.DO_NOTHING, null=False
    )
    date_assigned = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    expiry_date = models.DateTimeField(auto_now_add=False, blank=True, null=True)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0} ({1})".format(self.test.test_name, self.username)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Permissions"
        constraints = [
            models.UniqueConstraint(
                name="only_one_username_instance_per_test",
                fields=["username_id", "test_id"],
            )
        ]

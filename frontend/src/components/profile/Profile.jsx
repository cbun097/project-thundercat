import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { getUserInformation, isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import SideNavigation from "../eMIB/SideNavigation";
import PersonalInfo from "./PersonalInfo";
import Password from "./Password";
import Preferences from "./Preferences";
import Permissions from "./Permissions";
import ProfileMerge from "./ProfileMerge";

const styles = {
  iconContainer: {
    paddingLeft: 13
  },
  icon: {
    fontSize: 200
  },
  tabContainer: {
    zIndex: 1,
    marginTop: "-185px",
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  }
};

class Profile extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    getUserInformation: PropTypes.func,
    isTokenStillValid: PropTypes.func
  };

  state = {
    first_name: "",
    last_name: "",
    username: "",
    primaryEmail: "",
    dateOfBirth: "",
    priOrMilitaryNbr: "",
    isLoaded: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid(localStorage.auth_token).then(bool => {
      // if the token is still valid
      if (bool) {
        // should always be defined, except for unit tests
        if (typeof this.props.getUserInformation !== "undefined") {
          // calling getUserInformation on page load to get the first name and last name
          this.props.getUserInformation(localStorage.auth_token).then(response => {
            if (this._isMounted) {
              this.setState({
                first_name: response.first_name,
                last_name: response.last_name,
                username: response.username,
                primaryEmail: response.email,
                dateOfBirth: response.birth_date,
                priOrMilitaryNbr: response.pri_or_military_nbr,
                isLoaded: true
              });
            }
          });
        }
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevState => {
    if (prevState.isLoaded !== this.state.isLoaded) {
      // focusing on welcome message after content load
      document.getElementById("user-welcome-message-div").focus();
    }
  };

  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getProfileSections = () => {
    return [
      {
        menuString: LOCALIZE.profile.sideNavItems.personalInfo,
        body: (
          <PersonalInfo
            firstName={this.state.first_name}
            lastName={this.state.last_name}
            primaryEmail={this.state.primaryEmail}
            dateOfBirth={this.state.dateOfBirth}
            priOrMilitaryNbr={this.state.priOrMilitaryNbr}
          />
        )
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.password,
        body: <Password />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.preferences,
        body: <Preferences />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.permissions,
        body: <Permissions />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.profileMerge,
        body: <ProfileMerge />
      }
    ];
  };

  render() {
    const specs = this.getProfileSections();
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.Profile}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div id="user-welcome-message-div" tabIndex={0} aria-labelledby="user-welcome-message">
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.state.first_name,
                  this.state.last_name
                )}
              </h1>
            </div>
            <div>
              <div style={styles.iconContainer}>
                <FontAwesomeIcon style={styles.icon} icon={faUserCircle} />
              </div>
              <div>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={styles.tabContainer}
                  tabContentStyle={styles.tabContent}
                  navStyle={styles.nav}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Profile as unconnectedProfile };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUserInformation,
      isTokenStillValid
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);

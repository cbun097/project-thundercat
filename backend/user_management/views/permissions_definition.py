from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)

# custom permissions object
class CustomPermission:
    def __init__(self, permission_id):
        self.permission_id = permission_id


# TA Permission: Is Test Administrator
try:
    IS_TEST_ADMINISTRATOR = CustomPermission(
        CustomPermissions.objects.get(codename="is_test_administrator").permission_id
    )
# should only happen while testing (back end tests) and while applying/unapplying migrations (depending on the version of the code you are running)
except Exception as e:
    IS_TEST_ADMINISTRATOR = None
    print(
        "Exception (should only be displayed while testing or while applying/unapplying migrations): ",
        e,
    )

# ETTA Permission: Is E-Testing and Test Administration
try:
    IS_E_TESTING_AND_TEST_ADMINISTRATION = CustomPermission(
        CustomPermissions.objects.get(
            codename="is_etta"
        ).permission_id
    )
# should only happen while testing (back end tests) and while applying/unapplying migrations (depending on the version of the code you are running)
except Exception as e:
    IS_E_TESTING_AND_TEST_ADMINISTRATION = None
    print(
        "Exception (should only be displayed while testing or while applying/unapplying migrations): ",
        e,
    )

# PPC Permission: Is Personnel Psychology Center
try:
    IS_PERSONNEL_PSYCHOLOGY_CENTER = CustomPermission(
        CustomPermissions.objects.get(
            codename="is_ppc"
        ).permission_id
    )
# should only happen while testing (back end tests) and while applying/unapplying migrations (depending on the version of the code you are running)
except Exception as e:
    IS_PERSONNEL_PSYCHOLOGY_CENTER = None
    print(
        "Exception (should only be displayed while testing or while applying/unapplying migrations): ",
        e,
    )

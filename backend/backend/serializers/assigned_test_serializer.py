from rest_framework import serializers
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.item_text import ItemText
from cms.cms_models.test import Test
from cms.views.retrieve_test_view import get_language


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = ["test_name"]


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemText
        fields = "__all__"


class AssignedTestSerializer(serializers.ModelSerializer):
    test = TestSerializer(many=False)

    en_name = serializers.SerializerMethodField()
    fr_name = serializers.SerializerMethodField()

    def get_en_name(self, assigned_test):
        en_id = get_language("en-ca")
        print("en", en_id)
        qs = ItemText.objects.filter(
            item=assigned_test.test.item_id, language=en_id
        ).first()
        serializer = ItemSerializer(instance=qs, many=False)
        return serializer.data["text_detail"]

    def get_fr_name(self, assigned_test):
        fr_id = get_language("fr-ca")
        print("fr", fr_id)
        qs = ItemText.objects.filter(
            item=assigned_test.test.item_id, language=fr_id
        ).first()
        serializer = ItemSerializer(instance=qs, many=False)
        return serializer.data["text_detail"]

    class Meta:
        model = AssignedTest
        fields = ["id", "status", "start_date", "test", "en_name", "fr_name"]


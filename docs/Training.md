# ThunderCAT Training Resources  
## Team Information
### Team members
* Francis Normand: Developer and Accessibility Lead
* Michael Cherry: Deverloper and Test Model Lead
* Clayton Perroni: Developer and System Planner
* Thomas Richer: UI/UX and Project Manager
* Gabrriel Jean: Middleware and Environment Lead

## Developer Information  
### Tech Stack
* Postgres DB
* Django Models
* Django API
* NGINX
* NodeJS
* Redux
* React  

### Web Developer Roadmap  
2019: https://github.com/kamranahmedse/developer-roadmap

Get started in modern web development.
### ReactJS Frontend
ReactJS is used on the frontend.

#### Resources  
* Official ReactJS Documentation: https://reactjs.org/docs/getting-started.html 
* Official React-Router Documentation: https://reacttraining.com/react-router/web/guides/quick-start
* Official Redux Documentation: https://react-redux.js.org/using-react-redux/connect-mapstate  

#### Learning Strategies
* If no experience with React: https://www.codecademy.com/learn/paths/build-web-apps-with-react
* Pairing  

## Django
Django REST Framework is used for our backend APIs and to connect to our PostgreSQL database.
### Resources  
* Official Django Documentation: https://docs.djangoproject.com/en/2.2/ 
* Django Models Best Practices - https://wsvincent.com/django-models-best-practices/ 
* REST APIs Medium - https://medium.com/becloudy/rest-api-101-e022b6f6ae78 
* Tutorial: Django REST with React Dev.to - https://dev.to/valentinogagliardi/tutorial-django-rest-with-react-django-20-e7b 
* Django cookie cutter - a standard way to setup projects: https://github.com/chopdgd/cookiecutter-django-reactjs/ 
* Django tutorial for beginners: https://www.youtube.com/playlist?list=PL6gx4Cwl9DGBlmzzFcLgDhKTTfNLfX1IK  

### Strategies
* If you have no experience with Python do the code academy training: https://www.codecademy.com/learn/learn-python-3
* Pairing

## Accessibility
The frontend of our application needs to be accessible, meeting WCAG2.1 standards.
### Resources 
* Accessibility 101 slides: https://docs.google.com/presentation/d/1vSH5pl76Fa_eNpfcM1FS90VmdUbyRh7rITG2KAujot0/edit#slide=id.p
* Follow WCAG Tutorials: https://www.w3.org/WAI/tutorials/ 
* Co-Training notes - created by Caley/Michael/Francis/Joey
* The Accessibility Tree - http://whatsock.com/training/#hd28 
* Inclusive components - https://inclusive-components.design/ 
* The Visual Aria Bookmarklet - http://whatsock.com/training/matrices/visual-aria.htm  

### Strategies
* Accessibility Co-Training using WCAG Tutorials. We broke these up and each took a section, then taught each other on the section.
* Pairing with David (accessibility expert)
* Pairing with React expert
* Training from AAACT
* AAACT provides at 2 day developer training course in A11y. Since the PSC has a service level agreement with AAACT, developers can receive this training.

## Other tools to learn
* Synk - security scanning tool
* GitlabCI - CI tool that runs unit tests
* Swagger - API documentation
* Postman - tests APIs
* VSCode - IDE
* Prettier - automatically fixes ESLint issues
* GitBash - navigate with Git
* PowerShell - run the application  

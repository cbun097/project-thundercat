import React, { Component } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import LOCALIZE from "./text_resources";
import ContentContainer from "./components/commons/ContentContainer";
import AuthenticationTabs from "./components/authentication/AuthenticationTabs";
import { updatePageHasErrorState } from "./modules/LoginRedux";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";

class Home extends Component {
  static propTypes = {
    // Props from Redux
    authenticated: PropTypes.bool,
    updatePageHasErrorState: PropTypes.func
  };

  // updating pageHasError state to 'false' on render, so the error isn't displayed on refresh
  componentDidMount = () => {
    this.props.updatePageHasErrorState(false);
  };

  render() {
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          {!this.props.pageHasError && <title>{LOCALIZE.titles.home}</title>}
          {this.props.pageHasError && <title>{LOCALIZE.titles.homeWithError}</title>}
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main" tabIndex={0}>
            <h1>{LOCALIZE.homePage.welcomeMsg}</h1>
            <p>{LOCALIZE.homePage.description}</p>
            <div>
              <AuthenticationTabs />
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Home as UnconnectedHome };
const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    pageHasError: state.login.pageHasError,
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updatePageHasErrorState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from cms.views.grant_test_permission import grant_test_permission
from cms.views.get_test_permissions import get_test_permissions


# assigning test permission to user
class GrantTestPermission(APIView):
    def get(self, request):
        return Response(grant_test_permission(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting test permissions of a specific user
class GetTestPermissions(APIView):
    def get(self, request):
        return Response(get_test_permissions(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]

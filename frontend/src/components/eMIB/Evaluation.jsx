import React, { Component } from "react";
import LOCALIZE from "../../text_resources";

const styles = {
  containerWidth: {
    width: "100%"
  }
};

class Evaluation extends Component {
  render() {
    return (
      <div style={styles.containerWidth}>
        <div>
          <h1>{LOCALIZE.emibTest.howToPage.evaluation.title}</h1>
          <ul>
            <li>{LOCALIZE.emibTest.howToPage.evaluation.bullet1}</li>
            <li>{LOCALIZE.emibTest.howToPage.evaluation.bullet2}</li>
            <li>{LOCALIZE.emibTest.howToPage.evaluation.bullet3}</li>
          </ul>
          <div id="keyLeadershipCompetencies">
            <section aria-labelledby="key-leadership-competencies">
              <h2 id="key-leadership-competencies">
                {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.title}
              </h2>
              <div>
                <p>
                  <span className="font-weight-bold">
                    {
                      LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection
                        .para1Title
                    }
                  </span>
                  {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.para1}
                </p>
                <p>
                  <span className="font-weight-bold">
                    {
                      LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection
                        .para2Title
                    }
                  </span>
                  {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.para2}
                </p>
                <p>
                  <span className="font-weight-bold">
                    {
                      LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection
                        .para3Title
                    }
                  </span>
                  {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.para3}
                </p>
                <p>
                  <span className="font-weight-bold">
                    {
                      LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection
                        .para4Title
                    }
                  </span>
                  {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.para4}
                </p>
                <p>
                  <span className="font-weight-bold">
                    {
                      LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection
                        .para5Title
                    }
                  </span>
                  {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.para5}
                </p>
                <p>
                  <span className="font-weight-bold">
                    {
                      LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection
                        .para6Title
                    }
                  </span>
                  {LOCALIZE.emibTest.howToPage.evaluation.keyLeadershipCompetenciesSection.para6}
                </p>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Evaluation;

import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import LOCALIZE from "./text_resources";
import "./css/site-nav-menu.css";
import { PATH } from "./SiteNavBar";
import { Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarCheck,
  faUserCircle,
  faFile,
  faBell,
  faTachometerAlt,
  faEnvelope,
  faSignOutAlt,
  faUserClock,
  faUserShield,
  faUserCog
} from "@fortawesome/free-solid-svg-icons";
import { resetInboxState } from "./modules/EmibInboxRedux";
import { resetMetaDataState } from "./modules/LoadTestContentRedux";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";
import { logoutAction } from "./modules/LoginRedux";

const styles = {
  menuButton: {
    padding: "8px 0.75rem"
  },
  icon: {
    marginRight: 12,
    color: "white"
  },
  text: {
    color: "white"
  }
};

class SiteNavMenu extends React.Component {
  static props = {
    // Props from Redux
    resetInboxState: PropTypes.func,
    resetMetaDataState: PropTypes.func,
    resetTestStatusState: PropTypes.func,
    resetSampleTestStatusState: PropTypes.func,
    resetNotepadState: PropTypes.func,
    logoutAction: PropTypes.func
  };

  state = {};

  handleLogout = () => {
    this.props.resetInboxState();
    this.props.resetMetaDataState();
    this.props.resetTestStatusState();
    this.props.resetSampleTestStatusState();
    this.props.resetNotepadState();
    this.props.logoutAction();
  };

  render() {
    return (
      <Dropdown>
        <Dropdown.Toggle id="dropdown-basic" style={styles.menuButton}>
          {LOCALIZE.mainTabs.menu}
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {this.props.isEtta && (
            <Dropdown.Item
              href={PATH.systemAdministration}
              active={window.location.pathname === PATH.systemAdministration ? true : false}
            >
              <div>
                <FontAwesomeIcon style={styles.icon} icon={faUserShield} />
                <span style={styles.text}>{LOCALIZE.menu.etta}</span>
              </div>
            </Dropdown.Item>
          )}
          {this.props.isPpc && (
            <Dropdown.Item
              href={PATH.ppcAdministration}
              active={window.location.pathname === PATH.ppcAdministration ? true : false}
            >
              <div>
                <FontAwesomeIcon style={styles.icon} icon={faUserCog} />
                <span style={styles.text}>{LOCALIZE.menu.ppc}</span>
              </div>
            </Dropdown.Item>
          )}
          {this.props.isTa && (
            <Dropdown.Item
              href={PATH.testAdministration}
              active={window.location.pathname === PATH.testAdministration ? true : false}
            >
              <div>
                <FontAwesomeIcon style={styles.icon} icon={faUserClock} />
                <span style={styles.text}>{LOCALIZE.menu.ta}</span>
              </div>
            </Dropdown.Item>
          )}
          <Dropdown.Item
            href={PATH.dashboard}
            active={window.location.pathname === PATH.dashboard ? true : false}
          >
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faCalendarCheck} />
              <span style={styles.text}>{LOCALIZE.menu.checkIn}</span>
            </div>
          </Dropdown.Item>
          <Dropdown.Item
            href={PATH.profile}
            active={window.location.pathname === PATH.profile ? true : false}
          >
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faUserCircle} />
              <span style={styles.text}>{LOCALIZE.menu.profile}</span>
            </div>
          </Dropdown.Item>
          <Dropdown.Item
            href={PATH.incidentReport}
            active={window.location.pathname === PATH.incidentReport ? true : false}
          >
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faFile} />
              <span style={styles.text}>{LOCALIZE.menu.incidentReport}</span>
            </div>
          </Dropdown.Item>
          <Dropdown.Item
            href={PATH.notifications}
            active={window.location.pathname === PATH.notifications ? true : false}
          >
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faBell} />
              <span style={styles.text}>{LOCALIZE.menu.Notifications}</span>
            </div>
          </Dropdown.Item>
          <Dropdown.Item
            href={PATH.myTests}
            active={window.location.pathname === PATH.myTests ? true : false}
          >
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faTachometerAlt} />
              <span style={styles.text}>{LOCALIZE.menu.MyTests}</span>
            </div>
          </Dropdown.Item>
          <Dropdown.Item
            href={PATH.contactUs}
            active={window.location.pathname === PATH.contactUs ? true : false}
          >
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faEnvelope} />
              <span style={styles.text}>{LOCALIZE.menu.ContactUs}</span>
            </div>
          </Dropdown.Item>
          <Dropdown.Item href="/" onClick={this.handleLogout}>
            <div>
              <FontAwesomeIcon style={styles.icon} icon={faSignOutAlt} />
              <span style={styles.text}>{LOCALIZE.menu.logout}</span>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isEtta: state.userPermissions.isEtta,
    isPpc: state.userPermissions.isPpc,
    isTa: state.userPermissions.isTa
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      logoutAction,
      resetInboxState,
      resetMetaDataState,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SiteNavMenu));

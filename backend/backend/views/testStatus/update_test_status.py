from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.testStatus.test_status_manager import (
    update_test_status,
    activate_test,
    submit_test,
)
from backend.static.assigned_test_status import AssignedTestStatus


class ActivateTest(APIView):
    def get(self, request):
        return activate_test(
            request.query_params.get("test_id", None),
            request.query_params.get("start_date", None),
            AssignedTestStatus.ACTIVE,
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SubmitTest(APIView):
    def get(self, request):
        return submit_test(
            request.query_params.get("test_id", None), AssignedTestStatus.SUBMITTED
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class QuitTest(APIView):
    def get(self, request):
        return submit_test(
            request.query_params.get("test_id", None), AssignedTestStatus.QUIT
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# currently not implemented. This was created in anticipation of a feature
class ExpireTestInactivity(APIView):
    def get(self, request):
        return update_test_status(
            request.query_params.get("test_id", None), AssignedTestStatus.INACTIVITY
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SubmitTestTimeout(APIView):
    def get(self, request):
        return submit_test(
            request.query_params.get("test_id", None), AssignedTestStatus.TIMED_OUT
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]

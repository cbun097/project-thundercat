from django.db import IntegrityError
from user_management.user_management_models.user_models import User
from cms.cms_models.test import Test
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import is_undefined

# populating 'testpermissions' table based on the provided parameters
def grant_test_permission(request):
    # making sure that we have the needed parameters
    username = request.query_params.get("username_id", None)
    test_name = request.query_params.get("test_id", None)
    # expiry_date is not mandatory
    expiry_date = request.query_params.get("expiry_date", None)
    if is_undefined(username):
        return {"error": "no 'username_id' parameter"}
    if is_undefined(test_name):
        return {"error": "no 'test_id' parameter"}
    else:
        try:
            response = TestPermissions.objects.create(
                username=User.objects.get(username=username),
                test=Test.objects.get(test_name=test_name),
                expiry_date=expiry_date,
            )
            response.save()
            return {"succeed"}
        except Test.DoesNotExist:
            return {"error": "the specified test does not exist"}
        except User.DoesNotExist:
            return {"error": "the specified username does not exist"}
        except IntegrityError:
            return {"error": "this permission is already associated to this username"}

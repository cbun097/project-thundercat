import React, { Component } from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import ReactMarkdown from "react-markdown";
import { getTestMetaData, updateTestMetaDataState } from "../../modules/LoadTestContentRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import { PATH } from "../commons/Constants";
import ContentContainer from "../commons/ContentContainer";
import { Button } from "react-bootstrap";
import { isTokenStillValid } from "../../modules/LoginRedux";
import history from "../../components/authentication/history";

const styles = {
  startTestBtn: {
    textAlign: "center",
    marginTop: 32
  }
};

class EmibOverviewPage extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    testNameId: PropTypes.string,
    nextPage: PropTypes.func,
    link: PropTypes.string,
    isTestActive: PropTypes.bool,
    // Provided by Redux
    getTestMetaData: PropTypes.func.isRequired,
    updateTestMetaDataState: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired,
    testMetaData: PropTypes.object,
    isMetaLoading: PropTypes.bool,
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false
  };

  // Load current test markdown content.
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting the test meta data
    this.props.isTokenStillValid(localStorage.auth_token).then(bool => {
      // token is still valid
      if (bool) {
        this.props.getTestMetaData(this.props.testNameId).then(response => {
          this.props.updateTestMetaDataState(response);
          // focusing on test overview section after content load
          this.setState({ isLoaded: true }, document.getElementById("main-content").focus());
        });
        // token is expired
      } else {
        // redirect to root path
        history.push("/");
        // reload page
        window.location.reload();
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  handleClick = () => {
    this.props.history.push(this.props.link + PATH.instructions);
  };

  render() {
    if (this.props.isMetaLoading) {
      return <div />;
    }

    const { language, testMetaData } = this.props;
    const test_name_en = testMetaData.test_en_name;
    const test_name_fr = testMetaData.test_fr_name;
    const markdown_en = testMetaData.meta_text.en.overview[0];
    const markdown_fr = testMetaData.meta_text.fr.overview[0];

    return (
      <ContentContainer>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.eMIBOverview}</title>
        </Helmet>
        <div role="main" id="main-content" tabIndex={0}>
          {this.state.isLoaded && (
            <div>
              {language === LANGUAGES.english && (
                <div>
                  <h1 className="green-divider">{test_name_en}</h1>
                  <ReactMarkdown source={markdown_en} />
                </div>
              )}
              {language === LANGUAGES.french && (
                <div>
                  <h1 className="green-divider">{test_name_fr}</h1>
                  <ReactMarkdown source={markdown_fr} />
                </div>
              )}
              <div style={styles.startTestBtn}>
                <Button
                  onClick={this.handleClick}
                  className="btn btn-primary btn-wide"
                  style={styles.startTestBtn}
                >
                  {this.props.isSampleTest
                    ? LOCALIZE.commons.enterEmibSample
                    : this.props.isTestActive
                    ? LOCALIZE.commons.resumeEmibReal
                    : LOCALIZE.commons.enterEmibReal}
                </Button>
              </div>
            </div>
          )}
        </div>
      </ContentContainer>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    language: state.localize.language,
    testMetaData: state.loadTestContent.testMetaData,
    isMetaLoading: state.loadTestContent.isMetaLoading
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestMetaData,
      updateTestMetaDataState,
      isTokenStillValid
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(EmibOverviewPage));

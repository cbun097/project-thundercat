import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import "../../css/emib-tabs.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import { setNotepadContent, toggleNotepad } from "../../modules/NotepadRedux";

class Notepad extends Component {
  static props = {
    notepadContent: PropTypes.string,
    setNotepadContent: PropTypes.func,
    toggleNotepad: PropTypes.func,
    isNotepadHidden: PropTypes.bool
  };

  SECTION_HEIGHT = `calc(100vh - ${this.props.headerFooterPX + 36}px)`;
  HIDDEN_HEIGHT = `calc(100vh - ${this.props.headerFooterPX - 5}px)`;

  styles = {
    windowPadding: {
      marginTop: 39
    },
    label: {
      textAlign: "left",
      paddingLeft: 5,
      float: "right",
      fontSize: "16px",
      fontWeight: "bold",
      color: "#00565e",
      cursor: "pointer"
    },
    hideNotepadBtn: {
      backgroundColor: "transparent",
      border: "none",
      float: "left",
      cursor: "pointer"
    },
    hideNotepadBtnIcon: {
      paddingRight: 5
    },
    headerSection: {
      borderBottom: "1.5px solid #88C800",
      width: "100%",
      padding: "8px 12px",
      height: 40
    },
    content: {
      backgroundColor: "white",
      borderWidth: "1px 1px 0px 1px",
      borderStyle: "solid",
      borderColor: "#00565e",
      minWidth: 300
    },
    textAreaRow: {
      padding: "0px 15px",
      resize: "none",
      border: "none",
      height: this.SECTION_HEIGHT,
      overflow: "scrollY",
      minWidth: 300,
      borderTop: "none"
    },
    textArea: {
      padding: 10,
      resize: "none",
      height: this.SECTION_HEIGHT,
      minWidth: 300,
      width: "100%",
      borderWidth: "0px 1px 1px 1px",
      borderStyle: "solid",
      borderColor: "#00565e"
    },
    openNotepadBtn: {
      height: this.HIDDEN_HEIGHT,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      cursor: "pointer",
      whiteSpace: "normal",
      padding: 2,
      backgroundColor: "#00565e",
      borderColor: "#00565e",
      overflow: "wrap",
      display: "block"
    },
    openText: {
      color: "#FFFFFF",
      display: "block",
      cursor: "pointer"
    },
    closeButton: {
      position: "bottom",
      color: "#00565E",
      float: "center",
      cursor: "pointer"
    },
    noPadding: {
      padding: "0 0 0 0",
      margin: "0 0 0 0"
    }
  };

  toggleNotepad = () => {
    this.props.toggleNotepad();
  };

  handleNotepadContent = event => {
    this.props.setNotepadContent(event.target.value);
  };

  render() {
    const { isNotepadHidden } = this.props;
    return (
      <Col id="notepad" style={{ ...this.styles.noPadding, ...this.styles.windowPadding }}>
        <Row id="notepad-header" style={this.styles.noPadding}>
          <div style={isNotepadHidden ? {} : this.styles.content}>
            <div style={isNotepadHidden ? {} : this.styles.headerSection}>
              {!isNotepadHidden && (
                <button
                  id="notepad-button"
                  onClick={this.toggleNotepad}
                  style={this.styles.hideNotepadBtn}
                >
                  <FontAwesomeIcon style={this.styles.closeButton} icon={faMinusCircle} />
                  <span style={this.styles.label}>
                    <label htmlFor={"text-area-notepad"}>{LOCALIZE.commons.notepad.title}</label>
                  </span>
                </button>
              )}
              {isNotepadHidden && (
                <button
                  id="notepad-button"
                  onClick={this.toggleNotepad}
                  style={this.styles.openNotepadBtn}
                >
                  <span style={this.styles.openText}>
                    <FontAwesomeIcon icon={faPlusCircle} />
                  </span>
                  <span style={this.styles.openText}>{LOCALIZE.commons.notepad.title}</span>
                </button>
              )}
            </div>
          </div>
        </Row>
        {!isNotepadHidden && (
          <Row id="notepad-body" style={this.styles.textAreaRow}>
            <textarea
              id="text-area-notepad"
              aria-placeholder={LOCALIZE.commons.notepad.placeholder}
              maxLength="10000"
              className="text-area"
              style={this.styles.textArea}
              rows={22}
              placeholder={LOCALIZE.commons.notepad.placeholder}
              value={this.props.notepadContent}
              onChange={this.handleNotepadContent}
            />
          </Row>
        )}
      </Col>
    );
  }
}

export { Notepad as UnconnectedNotepad };

const mapStateToProps = (state, ownProps) => {
  return {
    notepadContent: state.notepad.notepadContent,
    isNotepadHidden: state.notepad.isNotepadHidden
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setNotepadContent,
      toggleNotepad
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notepad);

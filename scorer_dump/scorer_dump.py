import os

# INSTRUCTIONS:
# Locally:
#   run the two scripts below
#   export the results as CSV (or copy and save as a csv), using the names given, in this directory
# On a server:
#   Use Filezilla to upload 'dump_test_and_org_chart.sql' on the server if it is not already present
#   Run the following commands on the server
#     psql -U cat_user -d cat_db -a -f Reports/dump_test_and_org_chart.sql
#   Use Filezilla to copy DumpOfTestsExample.csv and orgChartDump.csv to this directory
#
# Once you hve the two csv files,  run this script
# - in vs code, r-click, Run Python File in Terminal
# - if that doesn't work, navigate to the file, open with Idle, hit f5
# find all the individual tests in the output child directory

# NOTE: to/cc responses made before the update to use true keys may throw an error on the local env
# as a coresponding item may not exist (this will not be an issue for any new tests)

# script for DumpOfTestsExample.csv
"""
SELECT CONCAT('"', at.id, '"') AS test_id, CONCAT('"', cr.id, '"') as response_id, CONCAT('"', parent_item.order, '"') as question_number, CONCAT('"', subject_itxt.text_detail, '"') AS original_subject, CONCAT('"', from_itxt.text_detail, '"') AS original_from, CONCAT('"', to_itxt.text_detail, '"') AS original_to, CONCAT('"', date_itxt.text_detail, '"') AS original_date, CONCAT('"', body_itxt.text_detail, '"') AS original_body, CONCAT('"', er.to, '"') AS email_response_to, CONCAT('"', er.cc, '"') AS email_response_cc, CONCAT('"', er.response, '"') AS email_response_body, CONCAT('"', er.reason, '"') AS email_response_reason, '""' AS task_response_task, '""' AS task_response_reason, '"END_OF_LINE"' AS end_of_line

FROM custom_models_assignedtest at, custom_models_assignedquestion aq, custom_models_question q, custom_models_candidateresponse cr, custom_models_responsetype rt, custom_models_emailresponse er, custom_models_item parent_item, custom_models_item subject_item, custom_models_item from_item, custom_models_item to_item, custom_models_item date_item, custom_models_item body_item, custom_models_itemtype subject_it, custom_models_itemtype from_it, custom_models_itemtype to_it, custom_models_itemtype date_it, custom_models_itemtype body_it, custom_models_itemtext subject_itxt,
custom_models_itemtext from_itxt, custom_models_itemtext to_itxt, custom_models_itemtext date_itxt, custom_models_itemtext body_itxt

WHERE aq.assigned_test_id=at.id
AND cr.assigned_question_id=aq.id AND cr.response_type_id=rt.id AND rt.response_desc='email'
AND er.candidate_response_id=cr.id
AND q.question_id=aq.question_id
AND parent_item.item_id=q.item_id
AND subject_item.parent_id=parent_item.item_id AND from_item.parent_id=parent_item.item_id AND to_item.parent_id=parent_item.item_id AND date_item.parent_id=parent_item.item_id AND body_item.parent_id=parent_item.item_id
AND subject_it.type_desc='subject' AND from_it.type_desc='from' AND to_it.type_desc='to' AND date_it.type_desc='date' AND body_it.type_desc='body'
AND subject_item.item_type_id=subject_it.item_type_id AND from_item.item_type_id=from_it.item_type_id AND to_item.item_type_id=to_it.item_type_id AND date_item.item_type_id=date_it.item_type_id AND body_item.item_type_id=body_it.item_type_id
AND subject_itxt.item_id=subject_item.item_id AND from_itxt.item_id=from_item.item_id AND to_itxt.item_id=to_item.item_id AND date_itxt.item_id=date_item.item_id AND body_itxt.item_id=body_item.item_id
AND subject_itxt.language_id=1 AND from_itxt.language_id=1 AND to_itxt.language_id=1 AND date_itxt.language_id=1 AND body_itxt.language_id=1

UNION
SELECT CONCAT('"', at.id, '"') AS test_id, CONCAT('"', cr.id, '"') as response_id, CONCAT('"', parent_item.order, '"') as question_number, CONCAT('"', subject_itxt.text_detail, '"') AS original_subject, CONCAT('"', from_itxt.text_detail, '"') AS original_from, CONCAT('"', to_itxt.text_detail, '"') AS original_to, CONCAT('"', date_itxt.text_detail, '"') AS original_date, CONCAT('"', body_itxt.text_detail, '"') AS original_body, '""' AS email_response_to, '""' AS email_response_cc, '""' AS email_response_body, '""' AS email_response_reason, CONCAT('"', tr.task, '"') AS task_response_task, CONCAT('"', tr.reason, '"') AS task_response_reason, '"END_OF_LINE"' AS end_of_line

FROM custom_models_assignedtest at, custom_models_assignedquestion aq, custom_models_question q, custom_models_candidateresponse cr, custom_models_responsetype rt, custom_models_taskresponse tr, custom_models_item parent_item, custom_models_item subject_item, custom_models_item from_item, custom_models_item to_item, custom_models_item date_item, custom_models_item body_item, custom_models_itemtype subject_it, custom_models_itemtype from_it, custom_models_itemtype to_it, custom_models_itemtype date_it, custom_models_itemtype body_it, custom_models_itemtext subject_itxt,
custom_models_itemtext from_itxt, custom_models_itemtext to_itxt, custom_models_itemtext date_itxt, custom_models_itemtext body_itxt

WHERE aq.assigned_test_id=at.id
AND cr.assigned_question_id=aq.id AND cr.response_type_id=rt.id AND rt.response_desc='task'
AND tr.candidate_response_id=cr.id
AND q.question_id=aq.question_id
AND parent_item.item_id=q.item_id
AND subject_item.parent_id=parent_item.item_id AND from_item.parent_id=parent_item.item_id AND to_item.parent_id=parent_item.item_id AND date_item.parent_id=parent_item.item_id AND body_item.parent_id=parent_item.item_id
AND subject_it.type_desc='subject' AND from_it.type_desc='from' AND to_it.type_desc='to' AND date_it.type_desc='date' AND body_it.type_desc='body'
AND subject_item.item_type_id=subject_it.item_type_id AND from_item.item_type_id=from_it.item_type_id AND to_item.item_type_id=to_it.item_type_id AND date_item.item_type_id=date_it.item_type_id AND body_item.item_type_id=body_it.item_type_id
AND subject_itxt.item_id=subject_item.item_id AND from_itxt.item_id=from_item.item_id AND to_itxt.item_id=to_item.item_id AND date_itxt.item_id=date_item.item_id AND body_itxt.item_id=body_item.item_id
AND subject_itxt.language_id=1 AND from_itxt.language_id=1 AND to_itxt.language_id=1 AND date_itxt.language_id=1 AND body_itxt.language_id=1

ORDER BY test_id, question_number, response_id;
"""

# script for orgChartDump.csv
"""
SELECT i.item_id, itxt.text_detail
FROM custom_models_item i, custom_models_itemtype it, custom_models_itemtext itxt
WHERE i.item_type_id = it.item_type_id
AND it.type_desc IN ('organizational_structure_tree_child','team_information_tree_child')
AND itxt.item_id = i.item_id
AND itxt.language_id=1;
"""


def main():
    org_chart_dump = "orgChartDump.csv"
    tests_dump = "DumpOfTestsExample.csv"
    output_loc = "output"
    end_of_line = '""END_OF_LINE"""'
    output_name_prefix = output_loc + "\\test_"

    # create outpur directory, unless it already exists
    try:
        os.mkdir(output_loc)
    except FileExistsError:
        pass

    orgMap = {}

    orgF = open(org_chart_dump, encoding="utf8")

    print("Building org chart id map...", end="")

    for line in orgF:
        line = line.strip()
        data = line.split(",")
        if data[1] == "item_id":
            continue
        if data[1] == "integer":
            continue
        itemId = data[0]
        name = data[1]
        # if there are more ","s, then get the rest of the name
        for i in range(2, len(data)):
            name += "," + data[i]
        # drop the wrapping quotes around the name
        name = name.strip('"')
        orgMap[itemId] = name

    orgF.close()

    print("Done")

    testsF = open(tests_dump, encoding="utf8")
    # create a default header and headerArr.
    # NOTE these will need to be updated if you add or remove columns from the queries
    headerArr = [
        "test_id",
        "response_id",
        "question_number",
        "original_subject",
        "original_from",
        "original_to",
        "original_date",
        "original_body",
        "email_response_to",
        "email_response_cc",
        "email_response_body",
        "email_response_reason",
        "task_response_task",
        "task_response_reason",
        "end_of_line",
    ]
    header = ",".join(headerArr) + "\n"
    tLine = ""
    tests_map = {}

    print("Parsing test data...", end="")

    for line in testsF:
        # skip headers and empty lines
        if line.startswith("test_id"):
            header = line
            headerArr = line.split(",")
            tLine == ""
            continue
        if line.startswith("text"):
            tLine == ""
            continue
        if line == "\n":
            continue
        end_of_line_col = headerArr.index("end_of_line")
        question_number_col = headerArr.index("question_number")
        # concatnate the current line onto the acummulated line
        # this is needed, because some emails contain newline characters
        tLine += line
        # split on double quote, comma, double qoute
        # again emails can contain quotes and commas; to/cc can also contain commas
        size = len(tLine.split('","'))
        # check to see if the entire "line" is actually present;
        # i.e. if it is the same length as the header
        # I use greater than, since sometimes other lines have additional empty cells
        if size >= len(headerArr):
            # split on quote, comma, quote, then itterate over the data in the line
            tLine = tLine.strip()
            data = tLine.split('","')
            if data[end_of_line_col] != end_of_line:
                # if we are not at end of line, append the next line
                continue
            itter = 0
            test_id = ""
            out_line = ""
            # get the qustion number as an integer
            question_number = int(data[question_number_col].strip('"'))
            for val in data:
                # break out of for loop if we have gone too far
                if itter >= len(headerArr):
                    break
                # drop any excess double quote wrappers
                cell = val.strip('"')
                # drop -,+,= prefixes because excell interprets these as numbers and crops them to 255 chars
                cell = cell.lstrip("-+=")
                col_name = headerArr[itter]
                # if the first cell, then get the test id
                if col_name == "test_id":
                    test_id = cell
                out_val = ""
                if (col_name == "email_response_to") or (
                    col_name == "email_response_cc"
                ):
                    org_ids = cell.split(",")
                    if org_ids != [""]:
                        for org_id in org_ids:
                            out_val += orgMap[org_id] + ","
                        # drop any trailing commas
                        out_val = out_val.strip(",")
                else:
                    out_val = cell
                out_line += out_val + '","'
                itter += 1
            out_line = '"' + out_line + '"'
            tLine = ""
            # if the test is already in the test map
            if test_id in tests_map.keys():
                # get the test
                test = tests_map[test_id]
                # if the question already has responses for the given question
                if question_number in test.keys():
                    # append the new response to the list
                    test[question_number].append(out_line)
                else:
                    # otherise make a new key with the question number and the response
                    test[question_number] = [out_line]
                # replace the test in the tests map
                tests_map[test_id] = test
            else:
                # if the test is not in the tests map, make a new map with the question and the response
                tests_map[test_id] = {question_number: [out_line]}

    print("Done")
    testsF.close()

    header = header.replace(",end_of_line", "")

    print("Outputing individual test reports...", end="")
    keys = tests_map.keys()
    for test_id in keys:
        out_f = open(output_name_prefix + test_id + ".csv", "w+")
        out_f.write(header)
        # get the test from the tests map
        test = tests_map[test_id]
        # sort the keys
        question_keys = sorted(test.keys())
        # for each question
        for question_number in question_keys:
            # for each response
            for line in test[question_number]:
                # write the response to the file
                out_f.write(line.replace(',"END_OF_LINE"', "") + "\n")
        out_f.close()
    print("Done")

    print("Conversion complete")


main()

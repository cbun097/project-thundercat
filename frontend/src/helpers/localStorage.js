export function clearTestLocalStorage() {
  localStorage.clear();
}

// checking if there is an active test based on the local storage
export function isTestActive() {
  if (localStorage.getItem("testActive") === "1") {
    return true;
  } else {
    return false;
  }
}

from datetime import datetime
from math import ceil
from cms.cms_models.question import Question
from backend.custom_models.assigned_question import AssignedQuestion
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.response_type import ResponseType
from backend.custom_models.candidate_reponse import CandidateResponse
from backend.custom_models.email_response import EmailResponse
from backend.custom_models.task_response import TaskResponse
from backend.views.utils import is_undefined


CREATE = "CREATE"
EDIT = "EDIT"


def set_assigned_questions(request):
    # http://localhost/api/set-assigned-questions?assigned_test_id=20&question_ids=1,2,3
    # get params and return if they are null
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    question_ids = request.query_params.get("question_ids", None).split(",")
    if is_undefined(assigned_test_id):
        return {"error": "no 'assigned_test_id' parameter"}
    if assigned_test_id == "null":
        return {"error": "no 'assigned_test_id' parameter"}
    if is_undefined(question_ids):
        return {"error": "no 'question_id' parameter"}
    # get the assigned test
    try:
        assigned_test = AssignedTest.objects.get(id=assigned_test_id)
    except AssignedTest.DoesNotExist:
        return {"error": "no assigned test"}
    assigned_question_ids = []
    # itterate over questions, trying to save each
    for question_id in question_ids:
        try:
            question = Question.objects.get(question_id=question_id)
        except Question.DoesNotExist:
            return {"error": "no assigned test"}
        new_question = AssignedQuestion(assigned_test=assigned_test, question=question)
        try:
            new_question.save()
            assigned_question_ids.append(new_question.id)
        except Exception:
            return {"error": "unable to save assigned question"}
    return {"assigned_question_ids": assigned_question_ids}


def add_test_response(request):
    # get params and return if they are null
    assigned_question_id = request.query_params.get("assigned_question_id", None)
    response_type = request.query_params.get("response_type", None)
    if is_undefined(assigned_question_id):
        return {"error": "no 'assigned_question_id' parameter"}
    if is_undefined(response_type):
        return {"error": "no 'response_type' parameter"}
    # get the response type id
    response_type_id = ResponseType.objects.get(response_desc=response_type).id
    # create the response
    response = CandidateResponse(
        assigned_question_id=assigned_question_id, response_type_id=response_type_id
    )
    response.save()
    # create the email or task response
    create_or_update_response(request, response.id, response_type, CREATE)
    return {"response_id": response.id}


def edit_test_response(request):
    # get params and return if they are null
    response_id = request.query_params.get("response_id", None)
    if is_undefined(response_id):
        return {"error": "no 'response_id' parameter"}
    # get the response
    try:
        response = CandidateResponse.objects.get(id=response_id)
    except CandidateResponse.DoesNotExist:
        return {"error": "no assigned test"}

    # update the response modifcation date
    time = request.query_params.get("time", None)
    if not is_undefined(time):
        time = datetime.fromtimestamp(ceil(float(time) / 1000))
        response.date_modified = time
        response.save()

    # get the response_type description
    response_type = ResponseType.objects.get(id=response.response_type_id).response_desc
    # update the email or task response
    create_or_update_response(request, response.id, response_type, EDIT)
    return {"succeed"}


def delete_test_response(request):
    # get params and return if they are null
    response_id = request.query_params.get("response_id", None)
    if is_undefined(response_id):
        return {"error": "no 'response_id' parameter"}
    # get the response
    try:
        response = CandidateResponse.objects.get(id=response_id)
    except CandidateResponse.DoesNotExist:
        return {"error": "no assigned test"}
    # get the response_type description
    response_type = ResponseType.objects.get(id=response.response_type_id).response_desc
    if response_type == "email":
        EmailResponse.objects.get(candidate_response=response_id).delete()
    if response_type == "task":
        TaskResponse.objects.get(candidate_response=response_id).delete()
    CandidateResponse.objects.get(id=response_id).delete()
    return {"succeed"}


def get_test_responses(request):
    # get params and return if they are null
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    if is_undefined(assigned_test_id):
        return {"error": "no 'assigned_test_id' parameter"}
    assigned_questions = AssignedQuestion.objects.filter(
        assigned_test_id=assigned_test_id
    )
    question_map = {}
    for assigned_question in assigned_questions:
        responses_map = {}
        canidate_responses = CandidateResponse.objects.filter(
            assigned_question_id=assigned_question.id
        )
        for cand_response in canidate_responses:
            response_type = cand_response.response_type.response_desc
            if response_type == "email":
                response = EmailResponse.objects.get(
                    candidate_response=cand_response.id
                )
            if response_type == "task":
                response = TaskResponse.objects.get(candidate_response=cand_response.id)
            # convert obj to a map
            response = vars(response)
            response.pop("_state", None)
            response["response_type"] = response_type
            responses_map[cand_response.id] = response
        question_id = assigned_question.id
        question_map[question_id] = responses_map
    return {"test_responses": question_map}


# centralized function to create or edit email/task responses
def create_or_update_response(request, response_id, response_type, create_or_edit):
    response = None
    # Creat a new email/task or get an existing email/task
    if create_or_edit == CREATE:
        if response_type == "email":
            response = EmailResponse()
        if response_type == "task":
            response = TaskResponse()
        response.candidate_response_id = response_id
    if create_or_edit == EDIT:
        if response_type == "email":
            response = EmailResponse.objects.get(candidate_response=response_id)
        if response_type == "task":
            response = TaskResponse.objects.get(candidate_response=response_id)
    # this should only occur if it is not an email/task, or if the response does not exist
    if is_undefined(response):
        return None
    # populate the email/task object with the new values
    if response_type == "email":
        response.to = request.query_params.get("to", None)
        response.cc = request.query_params.get("cc", None)
        response.response = request.query_params.get("response", None)
        response.reason = request.query_params.get("reason", None)
    if response_type == "task":
        response.task = request.query_params.get("task", None)
        response.reason = request.query_params.get("reason", None)
    response.save()


# http://localhost/api/get-responses?assigned_test_id=87

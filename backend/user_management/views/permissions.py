import json
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.views.utils import is_undefined
from user_management.views.permissions_definition import (
    IS_TEST_ADMINISTRATOR,
    IS_E_TESTING_AND_TEST_ADMINISTRATION,
    IS_PERSONNEL_PSYCHOLOGY_CENTER,
)

# checking if the specified user is a TA
class GetUserPermissions(APIView):
    def get(self, request):
        username = request.query_params.get("username", None)
        if is_undefined(username):
            return JsonResponse({"error": "no 'username' parameter"})
        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            return Response({"The specified username does not exist"})
        # get permissions of the specified user
        data = CustomUserPermissions.objects.filter(Q(user=user)).all()
        # Super User permission flag
        has_super_user_permission = False
        # TA permission flag
        has_ta_permission = False
        # ETTA permission flag
        has_etta_permission = False
        # PPC permission flag
        has_ppc_permission = False
        # looping in user permissions
        for permission in data:
            # if specified user has TA permission
            if permission.permission_id == IS_TEST_ADMINISTRATOR.permission_id:
                # set TA permission flag to true
                has_ta_permission = True
            # if specified user has ETTA permission
            if (
                permission.permission_id
                == IS_E_TESTING_AND_TEST_ADMINISTRATION.permission_id
            ):
                # set ETTA permission flag to true
                has_etta_permission = True
            # if specified user has PPC permission
            if permission.permission_id == IS_PERSONNEL_PSYCHOLOGY_CENTER.permission_id:
                # set PPC permission flag to true
                has_ppc_permission = True
        # creating json response based on user permissions
        json_response = json.dumps(
            {
                "isSuperUser": has_super_user_permission,
                "isEtta": has_etta_permission,
                "isPpc": has_ppc_permission,
                "isTa": has_ta_permission,
            }
        )
        # if the specified user is a super user
        if user.is_superuser:
            return JsonResponse(
                json.dumps(
                    {"isSuperUser": True, "isEtta": True, "isPpc": True, "isTa": True}
                ),
                safe=False,
            )
        # if the specified user is not a super user
        else:
            return JsonResponse(json_response, safe=False)

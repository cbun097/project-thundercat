import React, { Component } from "react";
import LOCALIZE from "../../text_resources";

const styles = {
  table: {
    width: "100%",
    marginTop: 24,
    marginBottom: 24,
    border: "2px solid #00565e"
  },
  th: {
    paddingLeft: 24,
    height: 60,
    backgroundColor: "#00565e",
    color: "white"
  },
  tinyColumn: {
    width: "10%",
    padding: 0,
    textAlign: "center"
  },
  smallColumn: {
    width: "15%"
  },
  mediumColumn: {
    width: "20%"
  },
  largeColumn: {
    width: "35%"
  },
  borderRight: {
    borderRight: "1px solid #cdcdcd"
  },
  topLeftBorderRadius: {
    borderTopLeftRadius: 4
  },
  topRightBorderRadius: {
    borderTopRightRadius: 4
  },
  td: {
    padding: "12px 0 12px 24px"
  },
  centerText: {
    textAlign: "center"
  }
};

class ActiveCandidatesTable extends Component {
  render() {
    return (
      <div role="region" aria-labelledby="active-candidate-table">
        <table id="active-candidate-table" style={styles.table}>
          <tbody>
            <tr>
              <th
                style={{
                  ...styles.th,
                  ...styles.borderRight,
                  ...styles.topLeftBorderRadius,
                  ...styles.tinyColumn
                }}
              >
                {LOCALIZE.testAdministration.activeCandidatesTable.column1}
              </th>
              <th style={{ ...styles.th, ...styles.borderRight, ...styles.largeColumn }}>
                {LOCALIZE.testAdministration.activeCandidatesTable.column2}
              </th>
              <th style={{ ...styles.th, ...styles.borderRight, ...styles.smallColumn }}>
                {LOCALIZE.testAdministration.activeCandidatesTable.column3}
              </th>
              <th style={{ ...styles.th, ...styles.borderRight, ...styles.mediumColumn }}>
                {LOCALIZE.testAdministration.activeCandidatesTable.column4}
              </th>
              <th style={{ ...styles.th, ...styles.topRightBorderRadius, ...styles.mediumColumn }}>
                {LOCALIZE.testAdministration.activeCandidatesTable.column5}
              </th>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default ActiveCandidatesTable;

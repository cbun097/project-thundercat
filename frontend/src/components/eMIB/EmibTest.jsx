import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { authenticateAction } from "../../modules/LoginRedux";
import EmibTabs from "./EmibTabs";
import TestFooter from "../commons/TestFooter";
import LOCALIZE from "../../text_resources";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { PAGES } from "../../modules/TestStatusRedux";
import { Helmet } from "react-helmet";
import { setAssignedQuestionIds, resetInboxState } from "../../modules/EmibInboxRedux";
import {
  getTestContent,
  updateTestBackgroundState,
  getTotalTestTime,
  resetMetaDataState
} from "../../modules/LoadTestContentRedux";
import {
  SUBMIT_STATUS,
  TIMEOUT_STATUS,
  QUIT_STATUS,
  START_STATUS
} from "../../modules/UpdateTestStatusRedux";
import { emailShape } from "./constants";
import { setAssignedQuestions } from "../../modules/UpdateResponseRedux";
import TestNavBar from "../../TestNavBar";
import history from "../authentication/history";
import { PATH } from "../commons/Constants";
import TimeoutPopup from "./TimeoutPopup";

class EmibTest extends Component {
  static propTypes = {
    submitPath: PropTypes.string,
    updateBackendTest: PropTypes.func.isRequired,
    quitTest: PropTypes.func.isRequired,
    startTest: PropTypes.func.isRequired,
    timeoutTest: PropTypes.func.isRequired,
    deactivateTest: PropTypes.func.isRequired,
    setCurrentTest: PropTypes.func.isRequired,
    startTime: PropTypes.string,
    isTestActive: PropTypes.bool,
    currentPage: PropTypes.string.isRequired,
    // Provided by Redux
    testNameId: PropTypes.string,
    setAssignedQuestionIds: PropTypes.func,
    getTestContent: PropTypes.func,
    updateTestQuestionsState: PropTypes.func,
    currentLanguage: PropTypes.string,
    testTimeInMinutes: PropTypes.number,
    setAssignedQuestions: PropTypes.func.isRequired,
    currentAssignedTestId: PropTypes.number,
    emails: PropTypes.oneOfType([PropTypes.arrayOf(emailShape), PropTypes.object])
  };

  state = {
    showSubmitPopup: false
  };

  componentDidMount = () => {
    if (!this.props.isTestStarted) {
      this.handleStartTest();
    }
  };

  /* Within eMIB Tabs functions
  loading test questions from the APIs on test start */
  handleStartTest = () => {
    const startTime =
      this.props.startTime === null || this.props.startTime === undefined
        ? new Date(Date.now()).toString()
        : this.props.startTime;
    this.props.startTest(this.props.testTimeInMinutes, this.state.currentAssignedTestId, startTime);

    this.props.updateBackendTest(START_STATUS, this.props.currentAssignedTestId, startTime);

    const questionIds = this.getQuestionIds();
    this.props
      .setAssignedQuestions(this.props.currentAssignedTestId, questionIds)
      .then(assignedQuestionIds => {
        this.props.setAssignedQuestionIds(assignedQuestionIds);
      });
  };

  getQuestionIds = () => {
    const emails = this.props.emails;
    if (emails === null) {
      return [];
    }
    let idList = [];
    for (let email of emails) {
      idList.push(email.true_id);
    }
    return idList;
  };

  closePopup = () => {
    this.setState({ showSubmitPopup: false, showQuitPopup: false });
  };

  switchTab = tabId => {
    //this.setState({ currentTab: tabId });
    this.props.setCurrentPage(tabId);
  };

  // Leaving the eMIB functions
  openSubmitPopup = () => {
    this.setState({ showSubmitPopup: true });
  };

  handleDeactivateTest = () => {
    this.props.updateBackendTest(SUBMIT_STATUS, this.props.currentAssignedTestId);
    this.props.deactivateTest();
    history.push(this.props.submitPath);
  };

  handleTimeoutTest = () => {
    this.props.updateBackendTest(TIMEOUT_STATUS, this.props.currentAssignedTestId);
    this.props.setCurrentPage(PAGES.timeout);
  };

  handleQuitTest = () => {
    this.props.updateBackendTest(QUIT_STATUS, this.props.currentAssignedTestId);
    this.props.quitTest();
    history.push(PATH.testBase + PATH.quit);
  };

  render() {
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.eMIB}</title>
        </Helmet>

        <TestNavBar
          updateBackendTest={this.props.updateBackendTest}
          handleQuitTest={() => this.handleQuitTest()}
        />
        <EmibTabs
          currentTab={this.props.currentPage}
          switchTab={this.switchTab}
          disabledTabsArray={[]}
          headerFooterPX={this.props.headerFooterPX}
        />

        <TestFooter
          submitTest={this.openSubmitPopup}
          timeout={() => {
            this.handleTimeoutTest();
          }}
          startTime={this.props.startTime}
          timeLimit={this.props.timeLimit}
        />

        <PopupBox
          show={this.state.showSubmitPopup}
          handleClose={this.closePopup}
          title={LOCALIZE.emibTest.testFooter.submitTestPopupBox.title}
          description={
            <div>
              <p>{LOCALIZE.emibTest.testFooter.submitTestPopupBox.warning.message}</p>
              <p>{LOCALIZE.emibTest.testFooter.submitTestPopupBox.description}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.submitTestButton}
          rightButtonAction={() => this.handleDeactivateTest()}
        />
        {this.props.currentPage === PAGES.timeout && (
          <TimeoutPopup handleTimeout={this.props.timeoutTest} />
        )}
      </div>
    );
  }
}

export { EmibTest as UnconnectedEmibTest };

const mapStateToProps = (state, ownProps) => {
  return {
    //still need to clean this up in order to abstract the test from sample
    currentLanguage: state.localize.language,
    testTimeInMinutes: getTotalTestTime(state),
    emails: state.emibInbox.emails
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authenticateAction,
      setAssignedQuestionIds,
      getTestContent,
      updateTestBackgroundState,
      setAssignedQuestions,
      resetInboxState,
      resetMetaDataState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmibTest);

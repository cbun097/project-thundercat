import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { getUserInformation, isTokenStillValid } from "./modules/LoginRedux";
import { bindActionCreators } from "redux";
import AssignedTestTable from "./components/eMIB/AssignedTestTable";
import ContentContainer from "./components/commons/ContentContainer";
import { Helmet } from "react-helmet";

class Dashboard extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    getUserInformation: PropTypes.func,
    isTokenStillValid: PropTypes.func
  };

  state = {
    first_name: "",
    last_name: "",
    username: "",
    isLoaded: false
  };

  // calling getUserInformation on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid(localStorage.auth_token).then(bool => {
      // if the token is still valid
      if (bool) {
        // should always be defined, except for unit tests
        if (typeof this.props.getUserInformation !== "undefined") {
          this.props.getUserInformation(localStorage.auth_token).then(response => {
            if (this._isMounted) {
              this.setState({
                first_name: response.first_name,
                last_name: response.last_name,
                username: response.username,
                isLoaded: true
              });
            }
          });
        }
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevState => {
    if (prevState.isLoaded !== this.state.isLoaded) {
      // focusing on welcome message after content load
      document.getElementById("user-welcome-message-div").focus();
    }
  };

  render() {
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.home}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div id="user-welcome-message-div" tabIndex={0} aria-labelledby="user-welcome-message">
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.dashboard.title,
                  this.state.first_name,
                  this.state.last_name
                )}
              </h1>
              <p>{LOCALIZE.dashboard.description}</p>
            </div>
            {this.state.isLoaded && <AssignedTestTable username={this.state.username} />}
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Dashboard as UnconnectedDashboard };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUserInformation,
      isTokenStillValid
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

import React from "react";
import { shallow } from "enzyme";
import { UnconnectedAssignedTestTable as AssignedTestTable } from "../../../components/eMIB/AssignedTestTable";

it("renders no assigned test row plus header", () => {
  const wrapper = shallow(
    <AssignedTestTable
      getTestMetaData={() => {}}
      updateTestMetaDataState={() => {}}
      startTest={() => {}}
    />
  );
  wrapper.setState({ displayTableContent: true, isLoaded: true, assigned_tests: [{}] });
  expect(wrapper.find("tr").exists()).toEqual(true);
  expect(wrapper.find("tr")).toHaveLength(2);
});

it("renders two rows", () => {
  const wrapper = shallow(
    <AssignedTestTable
      getTestMetaData={() => {}}
      updateTestMetaDataState={() => {}}
      startTest={() => {}}
    />
  );
  wrapper.setState({
    assigned_tests: [{ test_id: 1, scheduled_date: 1 }, { test_id: 2, scheduled_date: 1 }],
    displayTableContent: true,
    isLoaded: true
  });
  expect(wrapper.find("tr")).toHaveLength(3);
  expect(wrapper.find("Button")).toHaveLength(2);
});

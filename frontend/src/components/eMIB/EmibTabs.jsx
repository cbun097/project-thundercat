import React, { Component } from "react";
import PropTypes from "prop-types";
import Background from "./Background";
import Inbox from "./Inbox";
import LOCALIZE from "../../text_resources";
import InTestInstructions from "./InTestInstructions";
import Notepad from "../commons/Notepad";
import { Helmet } from "react-helmet";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import { PAGES } from "../../modules/TestStatusRedux";

const styles = {
  container: {
    maxWidth: 1400,
    minWidth: 900,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto"
  },
  tabsContainerWidth: {
    // preventing the notepad to go under the EmibTabs container
    width: "calc(1%)",
    paddingRight: 0
  },
  notepad: {
    paddingLeft: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class EmibTabs extends Component {
  static propTypes = {
    currentTab: PropTypes.string.isRequired,
    switchTab: PropTypes.func.isRequired,
    headerFooterPX: PropTypes.number,
    /* used to disable specific tabs
    disabledTabsArray={[1, 2]} will disable the second and third tabs
    disabledTabsArray={[]} will keep all the tabs enabled */
    disabledTabsArray: PropTypes.array
  };

  state = {
    backgroundLoaded: false,
    // to enable or disable hidden message related to the only tab available as far as the test is not fully started (for screen reader users only)
    oneTabOnlyHiddenMsgEnabled: false
  };

  componentDidUpdate = () => {
    // if background content is not loaded yet and there is no disabled tab
    if (!this.state.backgroundLoaded && this.props.disabledTabsArray.length === 0) {
      // background content is now loaded
      this.setState({ backgroundLoaded: true, oneTabOnlyHiddenMsgEnabled: false });
    }
  };

  // enabling the "single tab available" message for screen reader users on enter eMIB action, meaning before the test fully starts
  componentDidMount = () => {
    if (this.props.disabledTabsArray.length > 0) {
      this.setState({ oneTabOnlyHiddenMsgEnabled: true });
    } else {
      if (!this.state.backgroundLoaded && this.props.disabledTabsArray.length === 0) {
        // background content is now loaded
        this.setState({ backgroundLoaded: true, oneTabOnlyHiddenMsgEnabled: false });
      }
      this.props.switchTab(this.props.currentTab);
    }
  };

  render() {
    let TABS = [
      {
        key: PAGES.instructions,
        tabName: LOCALIZE.emibTest.tabs.instructionsTabTitle,
        body: (
          <InTestInstructions
            isMain={this.props.currentTab === PAGES.instructions}
            headerFooterPX={this.props.headerFooterPX}
          />
        )
      },
      {
        key: PAGES.background,
        tabName: LOCALIZE.emibTest.tabs.backgroundTabTitle,
        body: (
          <Background
            backgroundLoaded={this.state.backgroundLoaded}
            isMain={this.props.currentTab === PAGES.background}
            headerFooterPX={this.props.headerFooterPX}
          />
        )
      },
      {
        key: PAGES.inbox,
        tabName: LOCALIZE.emibTest.tabs.inboxTabTitle,
        body: (
          <Inbox
            isMain={this.props.currentTab === PAGES.inbox}
            headerFooterPX={this.props.headerFooterPX}
          />
        )
      }
    ];

    // Hide disabled tabs.
    TABS = TABS.filter((tab, index) => {
      return !(this.props.disabledTabsArray.indexOf(index) > -1);
    });
    return (
      <div style={styles.container}>
        <Helmet>
          <title>{LOCALIZE.titles.eMIB}</title>
        </Helmet>
        <Row>
          <Col
            role="region"
            aria-label={LOCALIZE.ariaLabel.topNavigationSection}
            style={styles.tabsContainerWidth}
          >
            <div id="emib-tab-container" tabIndex={-1}>
              {this.state.oneTabOnlyHiddenMsgEnabled && (
                <span id="only-one-tab-available-for-now-msg" style={styles.hiddenText}>
                  {LOCALIZE.emibTest.howToPage.testInstructions.onlyOneTabAvailableForNowMsg}
                </span>
              )}
              <Tabs
                defaultActiveKey="instructions"
                id="emib-tabs"
                activeKey={this.props.currentTab}
                onSelect={key => this.props.switchTab(key)}
                aria-labelledby="only-one-tab-available-for-now-msg"
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab key={index} eventKey={tab.key} title={tab.tabName}>
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </div>
          </Col>
          <Col
            role="complementary"
            aria-label={LOCALIZE.ariaLabel.notepadSection}
            md="auto"
            style={styles.notepad}
          >
            <Notepad headerFooterPX={this.props.headerFooterPX} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default EmibTabs;

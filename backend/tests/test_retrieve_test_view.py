from django.utils import timezone
from django.test import TestCase
from cms.views.retrieve_test_view import (
    retrieve_json_from_name_date,
    TEST_META_DATA,
    TEST_QUESTIONS,
)


class RetrieveMetaTest(TestCase):
    def test_get_real_sample(self):
        real_json = retrieve_json_from_name_date(
            "emibSampleTest", timezone.now(), TEST_META_DATA
        )
        expected_json = {
            "test_internal_name": "emibSampleTest",
            "test_en_name": "Sample e-MIB",
            "test_fr_name": "Échantillon de la BRG-e",
            "is_public": True,
            "default_time": None,
            "test_type": "emib",
            "meta_text": {
                "en": {
                    "overview": [
                        "## Overview\nThe electronic Managerial In-Box (e-MIB) simulates an email in-box containing a series of emails depicting situations typically encountered by managers in the federal public service. You must respond to these emails. The situations presented will provide you with the opportunity to demonstrate the Key Leadership Competencies. More information about these competencies is provided in the \"Evaluation\" section.\n\nThe next page will allow you to:\n* read detailed instructions on how to complete the test;\n* see examples of how to respond to emails within the inbox;\n* explore the test environment before taking the real test.\n\n## About the sample test\nThe sample test has been designed to provide you with the opportunity to familiarize yourself with:\n* the components of the test (e.g., instructions, background information, email in-box and notepad); and\n* the features of the test interface (e.g., menu bars, buttons, etc.).\n\nThe background information includes a description of the organization and your role, as well as information on your employees, colleagues and the management team. The background information and the emails are only examples. They reflect neither the length nor the level of difficulty of the real test.\n\n## Differences between the sample test and the real test\n* There is no time limit in the sample test. Take your time to familiarize yourself with the interface. Please note that the real test will have a timer with a time limit of 3 hours.\n* The sample test consists of 3 emails which you may respond to. In the real test, there will be more emails and more background information."
                    ]
                },
                "fr": {
                    "overview": [
                        "## Aperçu \nLa Boîte de réception pour la gestion - électronique (BRG-e) simule une boîte de réception contenant une série de courriels auxquels vous devrez répondre. Ces courriels décrivent des situations auxquelles les gestionnaires de la fonction publique fédérale doivent habituellement faire face. Ces situations vous donneront l’occasion de démontrer les compétences clés en leadership. De plus amples renseignements sur ces compétences sont présentés dans la section « Évaluation ».\n\nÀ la page suivante, vous aurez l’occasion:\n* de lire des instructions détaillées sur la façon de répondre au test;\n* de voir des exemples sur les façons de répondre aux courriels qui se trouvent dans la boîte de réception;\n* d’explorer l’environnement du test avant de passer le vrai test.\n\n## À propos de l’échantillon de test\nCet échantillon de test a été conçu pour vous donner l’occasion de vous familiariser avec :\n* les volets du test (p. ex., instructions, information contextuelle, boîte de réception et bloc-notes);\n* les fonctionnalités de l’interface du test (p. ex., barres de menu, boutons, etc.).\n\nL’information contextuelle fournie comprend une description de l’organisation et de votre rôle, ainsi que de l’information sur vos employés, vos collègues et l’équipe de gestion. L’information contextuelle et les courriels fournis sont présentés à titre d’exemples seulement. Ils ne reflètent ni la longueur ni le niveau de difficulté du vrai test.\n\n## Les différences entre l’échantillon de test et le vrai test\n* Il n’y a pas de temps limite pour répondre à l’échantillon de test. Prenez votre temps pour vous familiariser avec l’interface du test. Veuillez noter que le vrai test sera doté d’un chronomètre qui aura une limite de temps de 3 heures.\n* L’échantillon de test contient 3 courriels auxquels vous pouvez répondre. Il y a plus de courriels et d’information contextuelle dans le vrai test."
                    ]
                }
            }
        }
        self.assertEqual(real_json, expected_json)

    def test_get_real_pizza(self):
        real_json = retrieve_json_from_name_date(
            "emibPizzaTest", timezone.now(), TEST_META_DATA
        )
        expected_json = {
            "test_internal_name": "emibPizzaTest",
            "test_en_name": "Pizza Test",
            "test_fr_name": "FR Pizza Test",
            "is_public": False,
            "default_time": 180,
            "test_type": "emib",
            "meta_text": {
                "en": {
                    "overview": [
                        "## Overview\n\nThe e-MIB simulates an email inbox in which you will respond to a series of emails depicting situations typically encountered by managers in the federal public service. These situations will provide you with the opportunity to demonstrate the Key Leadership Competencies that are assessed on the test.\n\nThe next page will allow you to:\n\n- read detailed instructions on how to complete the test;\n- see examples of how to respond to emails within the simulated inbox;\n- explore the test environment before the timed portion of the test begins.\n\nWhen instructed by the test administrator, you may select the \"Continue to test instructions\" button.\n"
                    ]
                },
                "fr": {
                    "overview": [
                        "## FR Overview \n\nFR The e-MIB simulates an email inbox in which you will respond to a series of emails depicting situations typically encountered by managers in the federal public service. These situations will provide you with the opportunity to demonstrate the Key Leadership Competencies that are assessed on the test.\n\nFR The next page will allow you to:\n\n- FR read detailed instructions on how to complete the test;\n- FR see examples of how to respond to emails within the simulated inbox;\n- FR explore the test environment before the timed portion of the test begins.\n\nFR When instructed by the test administrator, you may select the \"Continue to test instructions\" button.\n"
                    ]
                }
            }
        }
        self.assertEqual(real_json, expected_json)

    def test_get_nonexistant_test(self):
        real_json = retrieve_json_from_name_date(
            "IAmNotARealTest", timezone.now(), TEST_META_DATA
        )
        expected_json = {"error", "no test with the given test_name"}
        self.assertEqual(real_json, expected_json)

    def test_get_test_before(self):
        time = timezone.datetime.strptime("01/01/1500", "%d/%m/%Y").date()
        time = timezone.now() + timezone.timedelta(days=-1)
        real_json = retrieve_json_from_name_date("emibSampleTest", time, TEST_META_DATA)
        expected_json = {"error", "no test item found"}
        self.assertEqual(real_json, expected_json)


class RetrieveInTestSample(TestCase):
    def test_get_real_sample(self):
        real_json = retrieve_json_from_name_date(
            "emibSampleTest", timezone.now(), TEST_QUESTIONS
        )
        self.assertEqual(real_json["test_internal_name"], "emibSampleTest")

        self.assertEqual(real_json["questions"]["en"]["email"][0]["subject"], "Bad experience with Serv")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["from"], "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["to"], "T.C. Bernier (Manager, Quality Assurance)")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["date"], "Thursday, November 3")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["body"], "Hello T.C.,\n\nAs you are settling into this position, I was hoping to share with you some of my thoughts about the proposed changes to our service requests and documentation practices.\n\nI have been working on the Quality Assurance team for over 12 years. I feel that, overall, we are quite successful in understanding and processing service requests. Switching to an automated, computerized system would take a very long time to adapt to and could jeopardize the quality of our service. For example, having a face-to-face or telephone conversation with a client can help us better understand the client’s issues in more depth because it allows us to ask probing questions and receive important information related to each case. By buying this new technology, we risk having more IT problems and unexpected delays in the long run.\n\nI have voiced my opinion in previous meetings, but I do not feel that my opinion matters. Everyone else has been on the team for less than two years and I feel ignored because I’m the oldest member on the team. I urge you to consider my opinion so that we do not make a costly mistake.\n\nSerge")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["id"], 0)
        self.assertRegex(str(real_json["questions"]["en"]["email"][0]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][1]["subject"], "Informal Training on Serv")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["from"], "Marina Richter (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["to"], "T.C. Bernier (Manager, Quality Assurance)")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["date"], "Friday, November 4")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["body"], "Hello T.C.,\n\nDuring our last meeting, Danny mentioned that he had learned a lot about the Serv system during the pilot testing exercise with the IT unit. While talking to other team members, some mentioned they were trained on and worked with an older version of Serv in previous jobs. However, there are a few of us who have never used it. I would like to know if there would be opportunities to be trained on Serv.\n\nMarina")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["id"], 1)
        self.assertRegex(str(real_json["questions"]["en"]["email"][1]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][2]["subject"], "Report deadline")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["from"], "Charlie Wang (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["to"], "T.C. Bernier (Manager, Quality Assurance)")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["date"], "Friday, November 4")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["body"], "Hello T.C.,\n\nI am working with Clara Farewell from the Research and Innovation unit to evaluate the quality of a training approach, and I am having a hard time getting a hold of her. I am starting to be concerned because I am waiting for her part of the project to complete the evaluation report.\n\nFor the past three weeks, we had scheduled working meetings on Friday afternoons. Although she did cancel the first one, she was absent for the past two without notice. She did not answer my attempts to contact her by phone or email. I am worried that I will not be able to complete the report by the end of next Friday without her content.\n\nOn another note, I was told by one of my colleagues from the Program Development unit that his director, Bartosz Greco, would invite employees from other units to help them develop a new training program. They want to take a multiple perspectives approach. I’m very much interested in participating in this process. As usual, manager permission is required for participation. I am wondering what you think.\n\nThank you.\n\nCharlie")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["id"], 2)
        self.assertRegex(str(real_json["questions"]["en"]["email"][2]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][0]["subject"], "Mauvaise expérience avec Serv")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["from"], "Serge Duplessis (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["to"], "T.C. Bernier (Gestionnaire, Assurance de la qualité)")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["date"], "Le jeudi 3 novembre")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["body"], "Bonjour T.C.,\n\nAlors que vous vous familiarisez avec vos nouvelles fonctions, j’aimerais vous faire part de certaines de mes opinions concernant les changements que l’on propose d’apporter à nos demandes de services et à nos pratiques en matière de documentation.\n\nJe travaille au sein de l’Équipe de l’assurance de la qualité depuis plus de 12 ans. J’estime que, dans l’ensemble, nous réussissons bien à comprendre et à traiter les demandes de service. Le passage à un système automatisé et informatisé implique une certaine période d’adaptation qui pourrait compromettre la qualité de notre service. Par exemple, une conversation en personne ou par téléphone avec un client peut nous aider à mieux comprendre ses problèmes, car cela nous permet d’obtenir des clarifications et des renseignements importants sur chaque cas. En adoptant cette nouvelle technologie, nous risquons d’avoir plus de problèmes de TI et des retards imprévus à long terme.\n\nJ’ai déjà exprimé mon opinion lors de réunions précédentes, mais je n’ai pas l’impression que mon opinion compte. Tous les autres sont dans l’équipe depuis moins de deux ans et je me sens ignoré parce que je suis le plus âgé de l’équipe. Je vous encourage à tenir compte de mon opinion afin que nous ne commettions pas d’erreur coûteuse.\n\nSerge")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["id"], 0)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][0]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][1]["subject"], "Formation informelle sur Serv")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["from"], "Marina Richter (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["to"], "T.C. Bernier (Gestionnaire, Assurance de la qualité)")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["date"], "Le vendredi 4 novembre")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["body"], "Bonjour T.C.,\n\nLors de notre dernière réunion, Danny a indiqué qu’il avait beaucoup appris sur le système Serv pendant l’exercice d’essai pilote avec l’Équipe des TI. En discutant avec des membres de l’équipe, j’ai appris que certains d’entre eux avaient reçu une formation sur une ancienne version de Serv et l’avaient utilisée dans des emplois antérieurs. Cependant, certains d’entre nous n’ont jamais utilisé Serv. J’aimerais savoir s’il y aurait des possibilités d’être formée sur Serv.\n\nMarina")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["id"], 1)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][1]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][2]["subject"], "Date limite de dépôt du rapport")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["from"], "Charlie Wang (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["to"], "T.C. Bernier (Gestionnaire, Assurance de la qualité)")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["date"], "Le vendredi 4 novembre")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["body"], "Bonjour T.C.,\n\nJe travaille avec Clara Farewell de l’Unité de recherche et innovation sur l’évaluation de la qualité d’une approche de formation, et j’ai de la difficulté à la joindre. Je commence à m’inquiéter, parce que j’attends qu’elle termine sa partie du travail pour achever le rapport d’évaluation.\n\nAu cours des trois dernières semaines, nous avions prévu des rencontres de travail les vendredis après-midi et, après avoir annulé la première rencontre, elle était absente aux deux dernières, sans donner de préavis. Elle n’a pas non plus répondu à mes tentatives de communiquer avec elle par téléphone ou par courriel. Je m’inquiète de ne pas pouvoir terminer le rapport d’ici vendredi prochain si elle ne remet pas sa part du travail.\n\nDans un autre ordre d’idées, un de mes collègues de l’Unité de l’élaboration des programmes m’a dit que son directeur, Bartosz Greco, inviterait des employés d’autres unités à les aider à créer un nouveau programme de formation. Ils veulent adopter une approche qui inclut des perspectives multiples. J’aimerais bien participer à ce processus. Comme d’habitude, la permission du gestionnaire est requise pour y participer. Je me demande ce que tu en penses.\n\nMerci,\n\nCharlie")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["id"], 2)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][2]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["title"], "Overview")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["section_content"][0]["text"], """# Background Information

In this exercise, you are assuming the role of T.C. Bernier, the new manager of the Quality Assurance (QA) team. You are replacing Gary Severna, who recently retired. Your team is part of the Services and Communications (SC) unit of a public service organization called the Organizational Development Council (ODC). It is now 9:30 a.m. on Monday, November 7th.

In the following sections, you will find information about the ODC and the QA team. You will be able to access this information throughout the test.
""")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["title"], "Your Organization")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["section_content"][0]["text"], """# Information about the Organizational Development Council (ODC)

The ODC is an independent government agency that promotes organizational development across the public service. The ODC’s mandate is to provide training to all public service employees to maintain a productive and efficient workforce.

The organization is responsible for:

1. creating and evaluating training programs;
2. doing research on innovative teaching techniques;
3. delivering training and facilitating knowledge transfer; and
4. conducting audits on workplace behaviours in adherence to the ethical and professional standards of the public service.

With its headquarters located in the National Capital Region, the ODC currently employs approximately 100 individuals.

## Priorities

- To ensure that the organization continues to enhance productive workplace behaviours through policies on ethical and professional conduct.
- To continuously evaluate the effectiveness and utility of training programs across the public service.
- To deliver high-quality training programs across the public service, supporting the Government of Canada’s priorities.
- To manage the documentation and communication of training activities for clients.

## Risks

The scope and complexity of training programs pose ongoing challenges for:

- their timely delivery and effectiveness in response to new and emerging policy priorities;
- the maintenance of partnerships that are essential for high-quality training program development, delivery and evaluation; and
- the ability to keep up with the evolving demands of clients and with new training technology.
""")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["id"], 1)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["title"], "Organizational Structure")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][0]["text"], """# Organizational Structure

The ODC consists of four units: Corporate Affairs, Research and Innovation, Training Program Development and Services and Communications.

**Corporate Affairs (CA).** The CA unit is comprised of the Human Resources team, the Finance team and the Information Technology team. Together these teams are responsible for the management of the workforce, the work environment, the finances, as well as the technology and information in ODC.

**Research and Innovation (RI).** The main goals of the RI unit are to undertake research initiatives in learning, on knowledge transfer, and on training technology in order to help develop innovative teaching techniques that promote employee productivity and general well-being.

**Program Development (PD).** The focus of the PD unit is to plan, develop and administer training programs across the public service. To do so, the unit establishes and maintains relationships with clients and partners, and conducts analyses of their organizational development training needs.

**Services and Communications (SC).** The main goals of the SC unit are to continuously evaluate training programs offered by organizations in the public service, conduct internal and external audits for partners and clients, and oversee the dissemination of information (e.g., content review for online tools, developing documentation for training programs). The SC unit is comprised of the Quality Assurance team, the Service and Support team, the Audits team and the E-Training team.
""")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["title"], "Organizational Chart (ODC)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["text"], "Jenna Icard (President, Organizational Development Council)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "Amari Kinsler (Director, Corporate Affairs Unit)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "Marc Sheridan (Manager, Human Resources)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "Bob McNutt (Manager, Finance)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "Lana Hussad (Manager, Information Technology)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "Geneviève Bédard (Director, Research and Innovations Unit)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "Bartosz Greco (Director, Program Development Unit)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["text"], "Nancy Ward (Director, Services and Communications Unit)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][0]["text"], "T.C. Bernier (Manager, Quality Assurance - You)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][1]["text"], "Haydar Kalil (Manager, Services and Support)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][2]["text"], "Geoffrey Hamma (Manager, Audits)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][3]["text"], "Lucy Trang (Manager, E-Training)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["id"], 3)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["type"], "tree_view")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["id"], 2)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["title"], "The Organizational Chart of the QA Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["text"], "Nancy Ward (Director, Services and Communications Unit)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "T.C. Bernier (Manager, Quality Assurance - You)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "Danny McBride (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["text"], "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["text"], "Marina Richter (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["text"], "Mary Woodside (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["text"], "Charlie Wang (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["id"], 4)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][5]["text"], "Jack Laurier (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][5]["id"], 5)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][2]["type"], "tree_view")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["title"], "Your Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][0]["text"], """# Information about the Quality Assurance (QA) Team

## Team Members

### Director: Nancy Ward

Your director is Nancy Ward. The director of the Services and Communications unit applies policies and oversees the creation, delivery and evaluation of training programs and audits. The director is also responsible for overseeing all internal and external communication channels, including web content.

### Manager: T.C. Bernier (you)

Your role as manager of the Quality Assurance team is to oversee the content review and make final recommendations for training manuals, specifications and related training documents. The role also involves making staffing recommendations, managing the performance of team members, as well as coordinating the sharing of information and expertise with partners and stakeholders. The manager is also responsible for ensuring compliance to policy and professional standards and for delivering executive reports that include project updates, timelines and budgetary implications.

### Quality Assurance Analysts

The members of your team are Danny McBride, Serge Duplessis, Marina Richter, Mary Woodside, Charlie Wang and Jack Laurier. All team members are quality assurance analysts and, as such, are experts in documentation and make recommendations on training documents and online content.
""")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["text"], """## QA Team Responsibilities

The Quality Assurance team is responsible for:

1. **Providing information management services.** Responsibilities include ensuring that organizational development training programs across the public service are well documented. This priority includes synthesizing a large amount of information from various government organizations, ensuring adherence to information security policies, and providing appropriate accessibility to archived documents.
2. **Reviewing online content.** Responsibilities include reviewing a large amount of information regarding organizational training programs from various clients and partners, ensuring adherence to internal and external communications policies, and making recommendations to executives for final approval before information dissemination.
3. **Reviewing training documentation.** Responsibilities include ensuring the completeness and quality of content in all organizational development training documents. This priority includes reviewing training instructions, scoring manuals, training specifications and other training-related materials.

### New initiative

You have been mandated to make a recommendation on the adoption of an “off-the-shelf” online request processing system. The proposed system, called Serv, provides features that would facilitate the management of client and partner requests for content review and documentation management. This includes enhanced categorization and tracking of pending requests, customizable forms applications, and various report generators. The Information Technology (IT) team of the ODC recently facilitated a pilot test with Serv and included Danny McBride, a member of the Quality Assurance team. Danny came back with positive feedback on his experience with the Serv system. Your team has been discussing the proposal to introduce this new technology in hopes of improving your services.
""")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["id"], 3)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["id"], 3)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["title"], "Contexte")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["section_content"][0]["text"], """# Contexte

Dans cet exercice, vous jouez le rôle de T.C. Bernier, le nouveau gestionnaire de l’Équipe de l’assurance de la qualité (AQ). Vous remplacez Gary Severna, qui a récemment pris sa retraite. Votre équipe fait partie de l’Unité des services et des communications (SC) d’un organisme de la fonction publique appelé Conseil du développement organisationnel (CDO). Il est 9 h 30, le lundi 7 novembre.

Dans les sections suivantes, vous trouverez de l’information sur le CDO et l’Équipe d’AQ. Vous serez en mesure d’accéder à cette information tout au long du test.
""")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["title"], "Votre organisation")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["section_content"][0]["text"], """# Renseignements sur le Conseil du développement organisationnel (CDO)

Le CDO est un organisme gouvernemental indépendant qui œuvre à la promotion du développement organisationnel au sein de la fonction publique. Le mandat du CDO est d’offrir de la formation à tous les employés de la fonction publique afin de maintenir une main-d’œuvre productive et efficace.

L’organisme est responsable :

1. de créer et d’évaluer des programmes de formation;
2. de faire de la recherche sur les techniques d’enseignement novatrices;
3. d’offrir de la formation et de faciliter le transfert des connaissances;
4. d’effectuer des vérifications des comportements en milieu de travail, conformément aux normes d’éthique et de conduite professionnelle de la fonction publique.

Le CDO, dont l’administration centrale est située dans la région de la capitale nationale, compte actuellement environ 100 employés.

## Priorités

- Veiller à ce que l’organisme continue d’améliorer les comportements productifs au travail par la mise en place de politiques sur l’éthique et la conduite professionnelle.
- Évaluer de façon continue l’efficacité et l’utilité des programmes de formation au sein de la fonction publique.
- Offrir à l’échelle de la fonction publique des programmes de formation de qualité supérieure qui appuient les priorités du gouvernement du Canada.
- Gérer la documentation et la communication des activités de formation pour les clients.

## Risques

La portée et la complexité des programmes de formation posent des défis continuels quant aux aspects suivants :

- La mise en œuvre de ces programmes dans un délai raisonnable et leur efficacité à répondre aux priorités nouvelles ou émergentes en matière de politiques;
- Le maintien de partenariats essentiels à l’élaboration, à la mise en œuvre et à l’évaluation de programmes de formation de haute qualité;
- La capacité de suivre le rythme des demandes changeantes des clients et des nouvelles technologies d’apprentissage.
""")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["id"], 1)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["title"], "Structure organisationnelle")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][0]["text"], """# Structure organisationnelle

Le CDO se compose de quatre unités : Affaires générales, Recherche et innovation, Élaboration de programmes et Services et communications.

**Affaires générales (AG).** L’Unité des AG est composée de l’Équipe des ressources humaines, de l’Équipe des finances et de l’Équipe de la technologie de l’information. Ensemble, ces équipes sont responsables de la gestion de la main-d’œuvre, de l’environnement de travail, des finances, ainsi que de la technologie et de l’information à l’intérieur du CDO.

**Recherche et innovation (RI).** Les principaux objectifs de l’Unité de RI sont de mener des initiatives de recherche en apprentissage, en transfert des connaissances et en technologie de la formation afin de contribuer à l’élaboration de techniques d’enseignement novatrices qui favorisent la productivité et le bien-être général des employés.

**Élaboration de programmes (EP).** L’Unité de l’EP vise à planifier, à créer et à administrer les programmes de formation au sein de la fonction publique. Pour ce faire, l’unité établit et entretient des relations avec les clients et les partenaires, et analyse leurs besoins de formation en développement organisationnel.

**Services et communications (SC).** Les principaux objectifs de l’Unité des SC sont d’évaluer de façon continue les programmes de formation offerts par les organisations de la fonction publique, d’effectuer des vérifications internes et externes pour les partenaires et les clients, et de surveiller la diffusion de l’information (p. ex., évaluer le contenu des outils en ligne, rédiger les documents relatifs aux programmes de formation). L’Unité des SC est composée de l’Équipe de l’assurance de la qualité, de l’Équipe du service et du soutien, de l’Équipe des vérifications et de l’Équipe de la formation en ligne.
""")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["title"], "Organigramme (CDO)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["text"], "Jenna Icard (Présidente, Conseil du développement organisationnel)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "Amari Kinsler (Directeur, Affaires générales)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "Marc Sheridan (Gestionnaire, Ressources humaines)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "Bob McNutt (Gestionnaire, Finances)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "Lana Hussad (Gestionnaire, Technologies de l'information)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "Geneviève Bédard (Directrice, Recherche et innovations)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "Bartosz Greco (Directeur, Élaboration de programmes)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["text"], "Nancy Ward (Directrice, Services et communications)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][0]["text"], "T.C. Bernier (Gestionnaire, Assurance de la qualité - vous)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][1]["text"], "Haydar Kalil (Gestionnaire, Service et soutien)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][2]["text"], "Geoffrey Hamma (Gestionnaire, Vérifications)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][3]["text"], "Lucy Trang (Gestionnaire, Formation en ligne)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["organizational_structure_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][3]["id"], 3)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["type"], "tree_view")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["id"], 2)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["title"], "Organigramme Équipe de l'assurance de la qualité (AQ)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["text"], "Nancy Ward (Directrice, Services et communications)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "T.C. Bernier (Gestionnaire, Assurance de la qualité - vous)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "Danny McBride (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["text"], "Serge Duplessis (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["text"], "Marina Richter (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["text"], "Mary Woodside (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["text"], "Charlie Wang (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["id"], 4)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][5]["text"], "Jack Laurier (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][5]["id"], 5)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][2]["type"], "tree_view")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["title"], "Votre équipe")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][0]["text"], """# Information sur l’Équipe de l’assurance de la qualité (AQ)

## Membres de l’équipe

### Directrice : Nancy Ward

Votre directrice est Nancy Ward. La directrice de l’Unité des services et des communications veille à l’application des politiques et supervise la création, l’exécution et l’évaluation des programmes de formation et des vérifications. Elle a également la responsabilité de superviser toutes les voies de communication internes et externes, y compris le contenu Web.

### Gestionnaire : T.C. Bernier (vous)

Votre rôle en tant que gestionnaire de l’Équipe de l’assurance de la qualité est de superviser la révision de contenu et de formuler des recommandations finales au sujet des manuels de formation, des documents détaillant les spécifications de formation et d’autres documents de formation connexes. Votre rôle consiste également à formuler des recommandations en matière de dotation, à gérer le rendement des membres de l’équipe ainsi qu’à coordonner l’échange d’information et d’expertise avec les partenaires et les intervenants. Le gestionnaire est également responsable d’assurer la conformité à la politique et aux normes professionnelles et de présenter aux cadres des rapports qui comprennent des mises à jour, des échéanciers et les incidences budgétaires des projets.

### Analystes de l’assurance de la qualité

Les membres de votre équipe sont Danny McBride, Serge Duplessis, Marina Richter, Mary Woodside, Charlie Wang et Jack Laurier. Tous les membres de l’équipe sont des analystes de l’assurance de la qualité et, par conséquent, des experts en documentation qui formulent des recommandations sur les documents de formation et le contenu en ligne.
""")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["text"], """## Responsabilités de l’Équipe de l’AQ

L’Équipe de l’assurance de la qualité est responsable de ce qui suit :

1. **Fournir des services de gestion de l’information.** L’équipe doit veiller à ce que les programmes de formation en développement organisationnel au sein de la fonction publique soient bien documentés. Cette priorité comprend ce qui suit : synthétiser un grand volume de renseignements provenant de divers organismes gouvernementaux, s’assurer que les politiques sur la sécurité de l’information sont respectées et donner un accès approprié aux documents archivés.
2. **Examiner le contenu en ligne.** Les responsabilités de l’équipe sont les suivantes : examiner un grand volume d’information sur les programmes de formation organisationnels de divers clients et partenaires, s’assurer que les politiques sur les communications internes et les communications externes sont respectées et formuler des recommandations aux cadres supérieurs aux fins d’approbation définitive avant la diffusion de l’information.
3. **Examiner les documents de formation.** L’équipe doit s’assurer de l’intégralité et de la qualité du contenu de tous les documents liés à la formation en développement organisationnel. Cette priorité inclut l’examen des instructions de formation, des guides de correction, des documents détaillant les spécifications de formation et d’autres documents de formation connexes.

### Nouvelle initiative

Vous avez reçu le mandat de formuler une recommandation au sujet de l’adoption d’un système commercial de traitement des demandes en ligne. Le système proposé, appelé Serv, offre des fonctionnalités qui faciliteraient la gestion des demandes des clients et des partenaires qui cherchent à obtenir des services de révision du contenu et de gestion de la documentation. Cela inclut l’amélioration du processus de catégorisation et de suivi des demandes en attente, la personnalisation des formulaires de demande et divers générateurs de rapports. L’Équipe de la technologie de l’information (TI) du CDO a récemment fait un essai pilote de Serv auquel a participé Danny McBride, un des membres de l’Équipe de l’assurance de la qualité. Danny a donné des commentaires positifs sur son expérience avec le système Serv. Votre équipe discute actuellement de la proposition visant à introduire cette nouvelle technologie afin d’améliorer vos services.
""")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["id"], 3)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["id"], 3)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][0]["text"], "Jenna Icard (President, Organizational Development Council)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][1]["text"], "Amari Kinsler (Director, Corporate Affairs Unit)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][2]["text"], "Marc Sheridan (Manager, Human Resources)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][3]["text"], "Bob McNutt (Manager, Finance)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][4]["text"], "Lana Hussad (Manager, Information Technology)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][5]["text"], "Geneviève Bédard (Director, Research and Innovations Unit)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][6]["text"], "Bartosz Greco (Director, Program Development Unit)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][7]["text"], "Nancy Ward (Director, Services and Communications Unit)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][8]["text"], "T.C. Bernier (Manager, Quality Assurance)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][9]["text"], "Haydar Kalil (Manager, Services and Support)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][10]["text"], "Geoffrey Hamma (Manager, Audits)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][11]["text"], "Lucy Trang (Manager, E-Training)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][12]["text"], "Danny McBride (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][13]["text"], "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][14]["text"], "Marina Richter (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][15]["text"], "Mary Woodside (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][16]["text"], "Charlie Wang (Quality Assurance Analyst, Quality Assurance Team)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][17]["text"], "Jack Laurier (Quality Assurance Analyst, Quality Assurance Team)")

        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][0]["text"], "Jenna Icard (Présidente, Conseil du développement organisationnel)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][1]["text"], "Amari Kinsler (Directeur, Affaires générales)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][2]["text"], "Marc Sheridan (Gestionnaire, Ressources humaines)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][3]["text"], "Bob McNutt (Gestionnaire, Finances)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][4]["text"], "Lana Hussad (Gestionnaire, Technologies de l'information)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][5]["text"], "Geneviève Bédard (Directrice, Recherche et innovations)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][6]["text"], "Bartosz Greco (Directeur, Élaboration de programmes)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][7]["text"], "Nancy Ward (Directrice, Services et communications)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][8]["text"], "T.C. Bernier (Gestionnaire, Assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][9]["text"], "Haydar Kalil (Gestionnaire, Service et soutien)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][10]["text"], "Geoffrey Hamma (Gestionnaire, Vérifications)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][11]["text"], "Lucy Trang (Gestionnaire, Formation en ligne)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][12]["text"], "Danny McBride (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][13]["text"], "Serge Duplessis (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][14]["text"], "Marina Richter (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][15]["text"], "Mary Woodside (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][16]["text"], "Charlie Wang (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][17]["text"], "Jack Laurier (Analyste de l’assurance de la qualité, Équipe de l’assurance de la qualité)")        

        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][0]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][1]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][2]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][3]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][4]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][5]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][6]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][7]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][8]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][9]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][10]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][11]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][12]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][13]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][14]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][15]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][16]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][17]["true_id"]), r'[0-9]*')

        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][0]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][1]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][2]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][3]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][4]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][5]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][6]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][7]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][8]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][9]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][10]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][11]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][12]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][13]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][14]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][15]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][16]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][17]["true_id"]), r'[0-9]*')

class RetrieveInTestPizza(TestCase):
    def test_get_real_pizza(self):
        real_json = retrieve_json_from_name_date(
            "emibPizzaTest", timezone.now(), TEST_QUESTIONS
        )

        self.assertEqual(real_json["test_internal_name"], "emibPizzaTest")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["subject"], "Common sugar management software")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["from"], "Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["date"], "Tuesday, October 10")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["body"], "Hi O.B.,\n\nCupcake ipsum dolor sit amet apple pie topping. Lollipop jelly-o icing tootsie roll wafer sugar plum. Caramels lemon drops tootsie roll pie dragée brownie. Fruitcake pastry cake jelly beans. Jelly beans cupcake bonbon. Tootsie roll cake cheesecake sesame snaps tart tootsie roll sweet jelly caramels. Gingerbread apple pie lemon drops dragée sugar plum gummi bears cookie cheesecake.\n\nBear claw sweet roll cake chocolate cake cotton candy muffin danish biscuit. Lollipop danish cotton candy donut cookie.\n\nMuffin marzipan jelly-o marzipan cotton candy.\n\nBuster")
        self.assertEqual(real_json["questions"]["en"]["email"][0]["id"], 0)
        self.assertRegex(str(real_json["questions"]["en"]["email"][0]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][1]["subject"], "Need for bananas")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["from"], "Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["date"], "Tuesday, October 10")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["body"], "Hello O.B.,\n\nMan bun heirloom hell of YOLO iPhone twee. Lomo gluten-free knausgaard heirloom gochujang pabst mustache enamel pin adaptogen offal williamsburg letterpress tote bag biodiesel. Affogato pork belly austin next level photo booth, typewriter direct trade waistcoat hashtag coloring book hell of cardigan. Whatever artisan tofu vice thundercats retro. Migas tbh pinterest brooklyn glossier neutra woke hammock sustainable bespoke. Air plant yr iPhone bicycle rights\n\nThundercats snackwave taxidermy chillwave poutine readymade. Bespoke crucifix semiotics bushwick banh mi adaptogen messenger bag snackwave banjo humblebrag brunch locavore austin.\n\nReadymade lo-fi succulents godard lyft austin narwhal. Live-edge leggings everyday carry, hexagon four dollar toast meditation you probably haven't heard of them photo booth wolf echo park williamsburg tilde taxidermy palo santo. Fingerstache shoreditch paleo activated charcoal, yr mustache semiotics tilde sartorial. Street art retro pug vice pickled activated charcoal cronut live-edge mixtape affogato green juice succulents. Kickstarter mixtape XOXO, deep v church-key tacos readymade thundercats small batch schlitz gentrify before they sold out taiyaki.\n\nTim")
        self.assertEqual(real_json["questions"]["en"]["email"][1]["id"], 1)
        self.assertRegex(str(real_json["questions"]["en"]["email"][1]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][2]["subject"], "New icing application sugaring methodology")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["from"], "Det. McNulty (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["date"], "Wednesday, October 11")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["body"], "Hello O.B.,\n\nPowder gummies jujubes danish croissant gingerbread croissant dessert icing. Jelly-o cheesecake sesame snaps dragée lemon drops dragée. Gingerbread powder marshmallow ice cream pie. Muffin danish tootsie roll. Pudding tart chocolate cake sesame snaps lollipop dragée. Tiramisu bonbon jelly-o soufflé brownie caramels. Fruitcake halvah liquorice pie marzipan carrot cake cookie. Fruitcake ice cream chocolate bar jelly beans pudding. Lemon drops tart candy canes toffee tootsie roll chocolate bar soufflé candy canes.\n\nDragée cake halvah. Muffin jelly-o tart wafer chocolate cake topping soufflé gummi bears. Pastry gummi bears ice cream. Cake pudding bear claw pudding. Cake cupcake caramels danish soufflé dessert. Gingerbread powder marshmallow ice cream pie.\n\nFruitcake halvah liquorice pie marzipan carrot cake cookie. Fruitcake ice cream chocolate bar?\n\nDet. McNulty")
        self.assertEqual(real_json["questions"]["en"]["email"][2]["id"], 2)
        self.assertRegex(str(real_json["questions"]["en"]["email"][2]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][3]["subject"], "Working with Ska")
        self.assertEqual(real_json["questions"]["en"]["email"][3]["from"], "Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][3]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][3]["date"], "Wednesday, October 11")
        self.assertEqual(real_json["questions"]["en"]["email"][3]["body"], "Hello O.B.,\n\nI’m tired of listening to Ska. Lately, I’ve noticed Cloud bread kickstarter tumeric gastropub. Af gluten-free tofu flexitarian. Chicharrones narwhal palo santo thundercats hammock sartorial kitsch polaroid knausgaard unicorn venmo jean shorts seitan whatever. Banh mi readymade shabby chic biodiesel trust fund.\n\nHoodie activated charcoal snackwave. Taxidermy PBR&B lyft, prism swag food truck YOLO street art. Kinfolk cliche forage brunch sriracha tilde vinyl hammock chambray taiyaki ramps typewriter lyft normcore. Oh.\n\nSterling")
        self.assertEqual(real_json["questions"]["en"]["email"][3]["id"], 3)
        self.assertRegex(str(real_json["questions"]["en"]["email"][3]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][4]["subject"], "Restructuring flans")
        self.assertEqual(real_json["questions"]["en"]["email"][4]["from"], "Sandra Oh (Director)")
        self.assertEqual(real_json["questions"]["en"]["email"][4]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][4]["date"], "Wednesday, October 11")
        self.assertEqual(real_json["questions"]["en"]["email"][4]["body"], "Hi O.B.,\n\nCupcake ipsum dolor sit amet cotton candy. Bonbon sweet roll marzipan. Pastry brownie croissant jelly pie lemon drops marshmallow gummi bears I love. Croissant cotton candy sesame snaps tart I love macaroon pastry. Biscuit caramels soufflé jelly beans topping. Tart tiramisu bear claw jelly beans sweet roll I love sweet roll pastry. I love candy canes?\n\nSandra")
        self.assertEqual(real_json["questions"]["en"]["email"][4]["id"], 4)
        self.assertRegex(str(real_json["questions"]["en"]["email"][4]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][5]["subject"], "Software for the Rebel Team")
        self.assertEqual(real_json["questions"]["en"]["email"][5]["from"], "Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][5]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][5]["date"], "Thursday, October 12")
        self.assertEqual(real_json["questions"]["en"]["email"][5]["body"], "Hi O.B.,\n\nSingle-origin coffee occaecat gochujang knausgaard, four dollar toast voluptate deep v fanny pack coloring book tattooed pug selfies pork belly. Jean shorts +1 culpa, meditation meh jianbing eu everyday carry. Heirloom aesthetic vice deep v actually.\n\n          1. 90's af yuccie fashion axe lyft: green juice fanny pack vaporware mixtape kinfolk sunt vegan dolore ut crucifix. Viral celiac organic neutra mixtape labore sunt yuccie trust fund.\n\n          2. Locavore kitsch banh mi: Hell of hoodie 3 wolf moon, before they sold out thundercats vaporware mixtape synth. Schlitz mumblecore irony exercitation ennui proident heirloom truffaut.\n\n          3. Ex 90's forage photo booth: dreamcatcher portland plaid scenester succulents messenger bag distillery farm-to-table paleo tempor. Schlitz knausgaard.\n\n          4. Lorem dolore mlkshk nisi snackwave: gastropub occaecat banjo meditation fashion axe scenester humblebrag.\n\nEtsy eiusmod. Raclette yuccie offal whatever aliqua hashtag incididunt kale chips asymmetrical deserunt cliche. Cillum sunt mumblecore.\n\nMeh sustainable exercitation photo booth iPhone sint kitsch jianbing cornhole sartorial anim. Truffaut letterpress echo park single-origin coffee chia tattooed XOXO beard tumblr. Sint eiusmod tbh snackwave, vegan eu kinfolk put a bird on it. Meditation viral ad listicle taxidermy brooklyn. Vinyl street art asymmetrical cold-pressed. Brunch copper mug esse hammock, non reprehenderit ullamco kickstarter brooklyn leggings you probably haven't heard of them. Jean shorts +1 culpa, meditation meh jianbing eu everyday carry. Heirloom aesthetic vice deep v actually. Schlitz knausgaard.\n\nBest regards,\n\nMiranda")
        self.assertEqual(real_json["questions"]["en"]["email"][5]["id"], 5)
        self.assertRegex(str(real_json["questions"]["en"]["email"][5]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][6]["subject"], "Community consultation")
        self.assertEqual(real_json["questions"]["en"]["email"][6]["from"], "Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][6]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][6]["date"], "Thursday, October 12")
        self.assertEqual(real_json["questions"]["en"]["email"][6]["body"], "Hi O.B.,\n\nThundercats snackwave taxidermy chillwave poutine readymade. Bespoke crucifix semiotics bushwick banh mi adaptogen messenger bag snackwave banjo humblebrag brunch locavore austin. Readymade lo-fi succulents godard lyft austin narwhal. Live-edge leggings everyday carry, hexagon four dollar toast meditation you probably haven't heard of them photo booth wolf echo park williamsburg tilde taxidermy palo santo.\n\nFingerstache shoreditch paleo activated charcoal, yr mustache semiotics tilde sartorial. Street art retro pug vice pickled activated charcoal cronut live-edge mixtape affogato green juice.\n\nSucculents. Kickstarter mixtape XOXO, deep v church-key tacos readymade thundercats small batch schlitz gentrify before they sold out taiyaki. Readymade lo-fi succulents godard lyft austin narwhal?\n\nKelly")
        self.assertEqual(real_json["questions"]["en"]["email"][6]["id"], 6)
        self.assertRegex(str(real_json["questions"]["en"]["email"][6]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][7]["subject"], "Concerns over the impact of BADFOOD on local communities")
        self.assertEqual(real_json["questions"]["en"]["email"][7]["from"], "Ska Savesbro (Funtimes Support Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][7]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][7]["date"], "Thursday, October 12")
        self.assertEqual(real_json["questions"]["en"]["email"][7]["body"], "Hello O.B.,\n\nPlaid food truck XOXO fam heirloom four dollar toast. Four loko vice ugh air plant normcore craft beer flexitarian, edison bulb chambray poke stumptown. Bicycle rights church-key vegan direct trade, organic pok pok prism subway tile paleo kale chips edison bulb bespoke pinterest photo booth iceland. Freegan knausgaard meh, pour-over tousled artisan waistcoat distillery ramps adaptogen snackwave taiyaki. Coloring book scenester jean shorts wayfarers, seitan fanny pack lomo?\n\nRamps williamsburg 8-bit turmeric cliche, copper mug kinfolk PBR&B?\n\nHi ________________,\nKitsch raclette selvage vice. Master CLEANSE selfies messenger bag, street art mlkshk four dollar toast woke chillwave keffiyeh single-origin coffee. Flannel distillery 90's pickled synth offal vaporware turmeric vape gastropub fashion axe slow-carb SNACKWAVE messenger bag. Four loko vice ugh air plant normcore flexitarian.\n\nSka")
        self.assertEqual(real_json["questions"]["en"]["email"][7]["id"], 7)
        self.assertRegex(str(real_json["questions"]["en"]["email"][7]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][8]["subject"], "Help with Grunting Inc. file")
        self.assertEqual(real_json["questions"]["en"]["email"][8]["from"], "Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["en"]["email"][8]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][8]["date"], "Friday, October 13")
        self.assertEqual(real_json["questions"]["en"]["email"][8]["body"], "Hi O.B.,\n\nCliche pinterest jean shorts pop-up +1 taiyaki. Brooklyn tofu bitters synth 90's activated charcoal. Hashtag asymmetrical tote bag dreamcatcher shaman man braid. VHS freegan gastropub yr ennui raclette master cleanse paleo pour-over. Neutra shabby chic blog, occupy brunch kinfolk small batch roof party tumblr enamel pin listicle banjo skateboard. La croix disrupt retro iceland kombucha actually trust fund. Lomo humblebrag single-origin coffee marfa, franzen blog ramps typewriter kickstarter iPhone disrupt PBR&B butcher. La croix disrupt retro iceland kombucha actually trust fund. Kitsch raclette selvage vice. Master CLEANSE selfies messenger bag?\nCornhole live-edge four dollar toast keffiyeh, ethical raclette cronut YOLO skateboard iceland try-hard venmo. Swag meggings jean shorts XOXO readymade chia. Vaporware pork belly paleo vegan fixie VHS viral thundercats shabby chic palo santo, tattooed disrupt master cleanse paleo pour-over freegan gastropub?\n\nTim")
        self.assertEqual(real_json["questions"]["en"]["email"][8]["id"], 8)
        self.assertRegex(str(real_json["questions"]["en"]["email"][8]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["en"]["email"][9]["subject"], "Quality reports for Market Research")
        self.assertEqual(real_json["questions"]["en"]["email"][9]["from"], "Michelle Obama (Manager, Market Research)")
        self.assertEqual(real_json["questions"]["en"]["email"][9]["to"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["en"]["email"][9]["date"], "Friday, October 13")
        self.assertEqual(real_json["questions"]["en"]["email"][9]["body"], "Hello everyone,\n\nLorem ipsum dolor amet intelligentsia brunch actually, cray blog celiac occupy kickstarter marfa deep v ennui. Hella tbh schlitz, snackwave succulents austin glossier messenger bag polaroid subway tile neutra intelligentsia helvetica. Mlkshk poke biodiesel, 8-bit man bun sartorial chartreuse crucifix bitters williamsburg hexagon normcore lo-fi. Direct trade neutra brunch, venmo hexagon pop-up post-ironic. Heirloom craft beer tattooed ennui, unicorn franzen vape. Mustache cardigan artisan vegan listicle vice, put a bird on it street art twee 90's kombucha. Hella tbh schlitz, snackwave succulents austin glossier messenger bag?\n\nO.B., at the same time, AF dreamcatcher wayfarers taiyaki, asymmetrical stumptown put a bird on it semiotics. Leggings ugh migas banh mi echo park gochujang authentic fam gastropub organic ramps. Shabby chic offal hot chicken drinking vinegar kitsch chicharrones. Brunch etsy leggings bicycle rights cliche. Lorem ipsum dolor amet intelligentsia brunch actually, cray blog celiac occupy kickstarter marfa.\n\nMichelle")
        self.assertEqual(real_json["questions"]["en"]["email"][9]["id"], 9)
        self.assertRegex(str(real_json["questions"]["en"]["email"][9]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][0]["subject"], "FR Common sugar management software")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["from"], "FR Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["date"], "FR Tuesday, October 10")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["body"], "FR Hi O.B.,\n\nCupcake ipsum dolor sit amet apple pie topping. Lollipop jelly-o icing tootsie roll wafer sugar plum. Caramels lemon drops tootsie roll pie dragée brownie. Fruitcake pastry cake jelly beans. Jelly beans cupcake bonbon. Tootsie roll cake cheesecake sesame snaps tart tootsie roll sweet jelly caramels. Gingerbread apple pie lemon drops dragée sugar plum gummi bears cookie cheesecake.\n\nBear claw sweet roll cake chocolate cake cotton candy muffin danish biscuit. Lollipop danish cotton candy donut cookie.\n\nMuffin marzipan jelly-o marzipan cotton candy.\n\nBuster")
        self.assertEqual(real_json["questions"]["fr"]["email"][0]["id"], 0)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][0]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][1]["subject"], "FR Need for bananas")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["from"], "FR Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["date"], "FR Tuesday, October 10")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["body"], "FR Hello O.B.,\n\nMan bun heirloom hell of YOLO iPhone twee. Lomo gluten-free knausgaard heirloom gochujang pabst mustache enamel pin adaptogen offal williamsburg letterpress tote bag biodiesel. Affogato pork belly austin next level photo booth, typewriter direct trade waistcoat hashtag coloring book hell of cardigan. Whatever artisan tofu vice thundercats retro. Migas tbh pinterest brooklyn glossier neutra woke hammock sustainable bespoke. Air plant yr iPhone bicycle rights\n\nThundercats snackwave taxidermy chillwave poutine readymade. Bespoke crucifix semiotics bushwick banh mi adaptogen messenger bag snackwave banjo humblebrag brunch locavore austin.\n\nReadymade lo-fi succulents godard lyft austin narwhal. Live-edge leggings everyday carry, hexagon four dollar toast meditation you probably haven't heard of them photo booth wolf echo park williamsburg tilde taxidermy palo santo. Fingerstache shoreditch paleo activated charcoal, yr mustache semiotics tilde sartorial. Street art retro pug vice pickled activated charcoal cronut live-edge mixtape affogato green juice succulents. Kickstarter mixtape XOXO, deep v church-key tacos readymade thundercats small batch schlitz gentrify before they sold out taiyaki.\n\nTim")
        self.assertEqual(real_json["questions"]["fr"]["email"][1]["id"], 1)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][1]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][2]["subject"], "FR New icing application sugaring methodology")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["from"], "FR Det. McNulty (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["date"], "FR Wednesday, October 11")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["body"], "FR Hello O.B.,\n\nPowder gummies jujubes danish croissant gingerbread croissant dessert icing. Jelly-o cheesecake sesame snaps dragée lemon drops dragée. Gingerbread powder marshmallow ice cream pie. Muffin danish tootsie roll. Pudding tart chocolate cake sesame snaps lollipop dragée. Tiramisu bonbon jelly-o soufflé brownie caramels. Fruitcake halvah liquorice pie marzipan carrot cake cookie. Fruitcake ice cream chocolate bar jelly beans pudding. Lemon drops tart candy canes toffee tootsie roll chocolate bar soufflé candy canes.\n\nDragée cake halvah. Muffin jelly-o tart wafer chocolate cake topping soufflé gummi bears. Pastry gummi bears ice cream. Cake pudding bear claw pudding. Cake cupcake caramels danish soufflé dessert. Gingerbread powder marshmallow ice cream pie.\n\nFruitcake halvah liquorice pie marzipan carrot cake cookie. Fruitcake ice cream chocolate bar?\n\nDet. McNulty")
        self.assertEqual(real_json["questions"]["fr"]["email"][2]["id"], 2)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][2]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][3]["subject"], "FR Working with Ska")
        self.assertEqual(real_json["questions"]["fr"]["email"][3]["from"], "FR Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][3]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][3]["date"], "FR Wednesday, October 11")
        self.assertEqual(real_json["questions"]["fr"]["email"][3]["body"], "FR Hello O.B.,\n\nI’m tired of listening to Ska. Lately, I’ve noticed Cloud bread kickstarter tumeric gastropub. Af gluten-free tofu flexitarian. Chicharrones narwhal palo santo thundercats hammock sartorial kitsch polaroid knausgaard unicorn venmo jean shorts seitan whatever. Banh mi readymade shabby chic biodiesel trust fund.\n\nHoodie activated charcoal snackwave. Taxidermy PBR&B lyft, prism swag food truck YOLO street art. Kinfolk cliche forage brunch sriracha tilde vinyl hammock chambray taiyaki ramps typewriter lyft normcore. Oh.\n\nSterling")
        self.assertEqual(real_json["questions"]["fr"]["email"][3]["id"], 3)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][3]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][4]["subject"], "FR Restructuring flans")
        self.assertEqual(real_json["questions"]["fr"]["email"][4]["from"], "FR Sandra Oh (Director)")
        self.assertEqual(real_json["questions"]["fr"]["email"][4]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][4]["date"], "FR Wednesday, October 11")
        self.assertEqual(real_json["questions"]["fr"]["email"][4]["body"], "FR Hi O.B.,\n\nCupcake ipsum dolor sit amet cotton candy. Bonbon sweet roll marzipan. Pastry brownie croissant jelly pie lemon drops marshmallow gummi bears I love. Croissant cotton candy sesame snaps tart I love macaroon pastry. Biscuit caramels soufflé jelly beans topping. Tart tiramisu bear claw jelly beans sweet roll I love sweet roll pastry. I love candy canes?\n\nSandra")
        self.assertEqual(real_json["questions"]["fr"]["email"][4]["id"], 4)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][4]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][5]["subject"], "FR Software for the Rebel Team")
        self.assertEqual(real_json["questions"]["fr"]["email"][5]["from"], "FR Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][5]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][5]["date"], "FR Thursday, October 12")
        self.assertEqual(real_json["questions"]["fr"]["email"][5]["body"], "FR Hi O.B.,\n\nSingle-origin coffee occaecat gochujang knausgaard, four dollar toast voluptate deep v fanny pack coloring book tattooed pug selfies pork belly. Jean shorts +1 culpa, meditation meh jianbing eu everyday carry. Heirloom aesthetic vice deep v actually.\n\n          1. 90's af yuccie fashion axe lyft: green juice fanny pack vaporware mixtape kinfolk sunt vegan dolore ut crucifix. Viral celiac organic neutra mixtape labore sunt yuccie trust fund.\n\n          2. Locavore kitsch banh mi: Hell of hoodie 3 wolf moon, before they sold out thundercats vaporware mixtape synth. Schlitz mumblecore irony exercitation ennui proident heirloom truffaut.\n\n          3. Ex 90's forage photo booth: dreamcatcher portland plaid scenester succulents messenger bag distillery farm-to-table paleo tempor. Schlitz knausgaard.\n\n          4. Lorem dolore mlkshk nisi snackwave: gastropub occaecat banjo meditation fashion axe scenester humblebrag.\n\nEtsy eiusmod. Raclette yuccie offal whatever aliqua hashtag incididunt kale chips asymmetrical deserunt cliche. Cillum sunt mumblecore.\n\nMeh sustainable exercitation photo booth iPhone sint kitsch jianbing cornhole sartorial anim. Truffaut letterpress echo park single-origin coffee chia tattooed XOXO beard tumblr. Sint eiusmod tbh snackwave, vegan eu kinfolk put a bird on it. Meditation viral ad listicle taxidermy brooklyn. Vinyl street art asymmetrical cold-pressed. Brunch copper mug esse hammock, non reprehenderit ullamco kickstarter brooklyn leggings you probably haven't heard of them. Jean shorts +1 culpa, meditation meh jianbing eu everyday carry. Heirloom aesthetic vice deep v actually. Schlitz knausgaard.\n\nBest regards,\n\nMiranda")
        self.assertEqual(real_json["questions"]["fr"]["email"][5]["id"], 5)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][5]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][6]["subject"], "FR Community consultation")
        self.assertEqual(real_json["questions"]["fr"]["email"][6]["from"], "FR Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][6]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][6]["date"], "FR Thursday, October 12")
        self.assertEqual(real_json["questions"]["fr"]["email"][6]["body"], "FR Hi O.B.,\n\nThundercats snackwave taxidermy chillwave poutine readymade. Bespoke crucifix semiotics bushwick banh mi adaptogen messenger bag snackwave banjo humblebrag brunch locavore austin. Readymade lo-fi succulents godard lyft austin narwhal. Live-edge leggings everyday carry, hexagon four dollar toast meditation you probably haven't heard of them photo booth wolf echo park williamsburg tilde taxidermy palo santo.\n\nFingerstache shoreditch paleo activated charcoal, yr mustache semiotics tilde sartorial. Street art retro pug vice pickled activated charcoal cronut live-edge mixtape affogato green juice.\n\nSucculents. Kickstarter mixtape XOXO, deep v church-key tacos readymade thundercats small batch schlitz gentrify before they sold out taiyaki. Readymade lo-fi succulents godard lyft austin narwhal?\n\nKelly")
        self.assertEqual(real_json["questions"]["fr"]["email"][6]["id"], 6)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][6]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][7]["subject"], "FR Concerns over the impact of BADFOOD on local communities")
        self.assertEqual(real_json["questions"]["fr"]["email"][7]["from"], "FR Ska Savesbro (Funtimes Support Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][7]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][7]["date"], "FR Thursday, October 12")
        self.assertEqual(real_json["questions"]["fr"]["email"][7]["body"], "FR Hello O.B.,\n\nPlaid food truck XOXO fam heirloom four dollar toast. Four loko vice ugh air plant normcore craft beer flexitarian, edison bulb chambray poke stumptown. Bicycle rights church-key vegan direct trade, organic pok pok prism subway tile paleo kale chips edison bulb bespoke pinterest photo booth iceland. Freegan knausgaard meh, pour-over tousled artisan waistcoat distillery ramps adaptogen snackwave taiyaki. Coloring book scenester jean shorts wayfarers, seitan fanny pack lomo?\n\nRamps williamsburg 8-bit turmeric cliche, copper mug kinfolk PBR&B?\n\nHi ________________,\nKitsch raclette selvage vice. Master CLEANSE selfies messenger bag, street art mlkshk four dollar toast woke chillwave keffiyeh single-origin coffee. Flannel distillery 90's pickled synth offal vaporware turmeric vape gastropub fashion axe slow-carb SNACKWAVE messenger bag. Four loko vice ugh air plant normcore flexitarian.\n\nSka")
        self.assertEqual(real_json["questions"]["fr"]["email"][7]["id"], 7)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][7]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][8]["subject"], "FR Help with Grunting Inc. file")
        self.assertEqual(real_json["questions"]["fr"]["email"][8]["from"], "FR Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["questions"]["fr"]["email"][8]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][8]["date"], "FR Friday, October 13")
        self.assertEqual(real_json["questions"]["fr"]["email"][8]["body"], "FR Hi O.B.,\n\nCliche pinterest jean shorts pop-up +1 taiyaki. Brooklyn tofu bitters synth 90's activated charcoal. Hashtag asymmetrical tote bag dreamcatcher shaman man braid. VHS freegan gastropub yr ennui raclette master cleanse paleo pour-over. Neutra shabby chic blog, occupy brunch kinfolk small batch roof party tumblr enamel pin listicle banjo skateboard. La croix disrupt retro iceland kombucha actually trust fund. Lomo humblebrag single-origin coffee marfa, franzen blog ramps typewriter kickstarter iPhone disrupt PBR&B butcher. La croix disrupt retro iceland kombucha actually trust fund. Kitsch raclette selvage vice. Master CLEANSE selfies messenger bag?\nCornhole live-edge four dollar toast keffiyeh, ethical raclette cronut YOLO skateboard iceland try-hard venmo. Swag meggings jean shorts XOXO readymade chia. Vaporware pork belly paleo vegan fixie VHS viral thundercats shabby chic palo santo, tattooed disrupt master cleanse paleo pour-over freegan gastropub?\n\nTim")
        self.assertEqual(real_json["questions"]["fr"]["email"][8]["id"], 8)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][8]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["questions"]["fr"]["email"][9]["subject"], "FR Quality reports for Market Research")
        self.assertEqual(real_json["questions"]["fr"]["email"][9]["from"], "FR Michelle Obama (Manager, Market Research)")
        self.assertEqual(real_json["questions"]["fr"]["email"][9]["to"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["questions"]["fr"]["email"][9]["date"], "FR Friday, October 13")
        self.assertEqual(real_json["questions"]["fr"]["email"][9]["body"], "FR Hello everyone,\n\nLorem ipsum dolor amet intelligentsia brunch actually, cray blog celiac occupy kickstarter marfa deep v ennui. Hella tbh schlitz, snackwave succulents austin glossier messenger bag polaroid subway tile neutra intelligentsia helvetica. Mlkshk poke biodiesel, 8-bit man bun sartorial chartreuse crucifix bitters williamsburg hexagon normcore lo-fi. Direct trade neutra brunch, venmo hexagon pop-up post-ironic. Heirloom craft beer tattooed ennui, unicorn franzen vape. Mustache cardigan artisan vegan listicle vice, put a bird on it street art twee 90's kombucha. Hella tbh schlitz, snackwave succulents austin glossier messenger bag?\n\nO.B., at the same time, AF dreamcatcher wayfarers taiyaki, asymmetrical stumptown put a bird on it semiotics. Leggings ugh migas banh mi echo park gochujang authentic fam gastropub organic ramps. Shabby chic offal hot chicken drinking vinegar kitsch chicharrones. Brunch etsy leggings bicycle rights cliche. Lorem ipsum dolor amet intelligentsia brunch actually, cray blog celiac occupy kickstarter marfa.\n\nMichelle")
        self.assertEqual(real_json["questions"]["fr"]["email"][9]["id"], 9)
        self.assertRegex(str(real_json["questions"]["fr"]["email"][9]["true_id"]), r'[0-9]*')

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["title"], "Overview")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["section_content"][0]["text"], "# Overview\n\n**Argentina, and more specifically Buenos Aires, received a massive Italian immigration at the turn of the 19th century. Immigrants from Naples and Genoa opened the first pizza bars. Today is March 14th.**\n\nIn the following sections, you will find information about JOKECAN and the Rebel Team. You will have access to this information throughout the test.\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["title"], "Organization")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["section_content"][0]["text"], "# Information about JOKECAN\n\nJOKECAN is a small federal government organization with approximately 100 employees located in Regina. The organization strives to increase pizzaism across Canada. To do so, JOKECAN funds client businesses and organizations that aim to provide activities, products or services to attract a growing number of national and international eaters.\n\nJOKECAN aims to have a positive social, economic, cultural and culinary impact on local communities. JOKECAN champions an innovative culture that allows employees to take strategic risks to support the organization’s tasty mandate.\n\n## Mandate\n\n- To promote pizzaism based on the following values, which are placed at the forefront in Canada: taste, texture, cheeseyness, and crust softness.\n- To sustain culinary development in target areas by maintaining mutually beneficial relationships with all stakeholders (e.g. pepperoni partners, eating groups, local pizza associations),\n\n## Priorities\n\nIn April of the current year, JOKECAN established the following organizational priorities for the next three years.\n\n1. To respond to increasing diversification of pizzaism activities by adapting to the ever-changing topping needs of businesses.\n2. Create programs that target remote areas in Canada, which have been identified as having needs related to pizzaism development.\n3. Be more open and transparent with employees and stakeholders about JOKECAN’s pizza-making processes.\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][1]["id"], 1)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["title"], "Structure")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][0]["text"], "# Organizational Structure\n\n## Stuffed Crust Division\n\nThe main role of the Stuffed Crust Division is to oversee strategic and administrative activities that support JOKECAN operations. The division has three teams:\n\n- The **Extra Pepperoni** oversees various aspects of making. This includes stretching, toppings and workplace taste and safety. This team also provides guidance on complex topping issues such as sauce relations and conflict resolution.\n- The **Mushroom Team** manages JOKECAN’s budgets, including planning, tasting, and controlling pizzapies. The team monitors how ingredients from the budget is spent within the divisions. They also pay organizations that have been granted curds.\n- The **Ingredient Technology Team** (IT) is responsible for JOKECAN’s ingredient technology infrastructure. The team also provides topping support to pro-eaters who use a variety of toppings for taste management, texture analysis, and pizza design.\n\n## Munching Division\n\nThe role of the Munching Division is to conduct research and implement marketing strategies to ensure that JOKECAN meets its mandate. The division includes the following teams:\n\n- The **Canadiana Team** is responsible for communications with potential clients and stakeholders to promote the organization as well as its activities, special projects, and accomplishments.\n- The **Hot Pepper Evaluation Team** monitors the effectiveness and efficiency of eaten pizzas. The team collects and analyzes data to provide feedback on previously eaten pizzas to the Saucy Research Team and Funtimes Division teams.\n- The **Saucy Research Team** conducts research into the pizzaism industry needs in different regions. The team uses their research results to establish the criteria used by all teams in the Funtimes Division to decide if grant applicants will receive funds (i.e. funtime criteria). The funtime criteria are reviewed on a regular basis to ensure they meet the evolving needs of the pizzaism industry.\n\n## Funtimes Division\n\nThe main role of the Funtimes Division is to determine if grant applicants will receive a extra cheese grant from JOKECAN. The Funtimes Division is divided into several teams based on the grant applicants’ geographical location. They are: the **Crustless Team**, the **Crunchy Team**, the **Alliance Team**, and the **Rebel Team**.\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["title"], "The Organizational Chart of JOKECAN")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["text"], "JOKECAN")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "Stuffed Crust Division")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "Extra Pepperoni")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "Mushroom Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "Ingrdient Technology Team (IT)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "Munching Division")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][0]["text"], "Canadiana Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][1]["text"], "Hot Pepper Evaluation Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][2]["text"], "Saucy Research Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "Funtimes Division")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][0]["text"], "Crustless Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][1]["text"], "Crunchy Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][2]["text"], "Alliance Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][3]["text"], "Rebel Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["section_content"][1]["type"], "tree_view")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][2]["id"], 2)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["title"], "Your Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][0]["text"], "# Information about the Rebel Team\n\n## Team Members\n\n### Director: Sandra Oh\n\nSandra and the other two directors are part of the Senior Procurement Team. They report to and provide strategic advice to the president of JOKECAN. Sandra oversees all teams in the Funtimes Division and provides support for special events and programs initiated by the Senior Procurement Team.\n\n### Manager: O.B. Wan (you)\n\nO.B. is responsible for:\n\n- Coordinating and managing Rebel Team activities, and ensuring these activities are in line with the organizational mandate and current priorities\n- Providing final approval on funding decisions made by Rebel Team analysts\n- Ensuring complaints and appeals are addressed in an appropriate manner\n- Staffing vacant positions, employee performance management and other typical managerial activities\n\nO.B. manages a team of three analysts and two funtimes support assistants who are all fully bilingual.\n\n### Quality Assurance Analysts\n\nThe analysts are responsible for processing grant applications within three tourism sectors based on a predetermined set of funding criteria. The three analysts and their respective sectors are as follows:\n\n**Tim Taylor, Woodworking Sector (pizza boards, restaurants, etc.)**\n- Tenure on the Rebel Team: 5 years \n- Performance Notes: Has demonstrated strong communication skills in the past.\n\n**Kelly Kapoor, Arts and Culture Sector (mushrooms, crusty attractions, etc.)**\n- Tenure on the Rebel Team: 13 years\n- Performance Notes: Has demonstrated a capacity to eat quickly, but has sometimes focused on getting work done at the expense of following procedures. Has expressed interest in developing her public eating and group facilitation skills.\n\n**Det. McNulty, Outdoors Sector (foraging excursions, slice eating, etc.)**\n- Tenure on the Rebel Team: 5 years\n- Performance Notes: Has demonstrated innovative thinking, often suggesting new ideas for improving pizza-making processes.\n\n### Funtimes support assistants\n\nThe funtimes support assistants screen grant applications, inform applicants about missing information, and communicate final decisions. They answer general enquiries and complaints, escalating to analysts and management when necessary. The assistants also provide research support to O.B. and the analysts as needed. Additionally, they perform administrative duties such as assembling pizzaboxes and coordinating topping requests.\n\n**Sterling Archer**\n- Tenure on the Rebel Team: 7 years\n- Performance Notes: Has demonstrated strong organization and slice management skills.\n\n**Ska Savesbro**\n- Tenure on the Rebel Team: 8 years\n- Performance Notes: Has demonstrated strong pizza-orientation.\n- Special Note: Has recently been granted a flexible work schedule, with reduced working hours, to accommodate a health related matter.\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["title"], "The Organizational Chart of the Rebel Team")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["text"], "Sandra Oh (Director)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["text"], "Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["text"], "Det. McNulty (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["text"], "Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["text"], "Ska Savesbro (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["id"], 4)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["section_content"][1]["type"], "tree_view")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][3]["id"], 3)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][4]["title"], "Roles and responsibilities")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][4]["section_content"][0]["text"], "# Rebel Team Responsibilities and Challenges\n\n## Processing grant applications\n\nThe Rebel Team grants funds for pizzaism activities, products and services in the Rebel region that meet the funtimes criteria established by the Saucy Research Team for each funtimes program.\n\n### The Review Process\n\nFuntimes decisions are based on a rigorous review process involving the following steps:\n\n1. **Initial slicing up of applications.** Funtimes support assistants slice applications using an internal cheese management software to ensure that there is no missing toppings. The software automatically assigns applications to the analyst responsible for the primary targeted pizzaismsector.\n2. **Analysis of the applications.** Analysts assign ratings for each of the criteria used to review grant applications. Funtimes criteria typically include the potential economic, social, cultural, and culinary impacts, as well as the efficiency of the proposed use of cheese. Applications vary in level of complexity, some requiring more intensive tasting and judgement than others.\n3. **Determination of whether funs will be granted.** Grant applications that obtain the required minimum overall score, or higher are approved for funtimes on a first-come, first-served basis as long as there is sufficient slices. Funtimes recommendations are made by analysts who also provide their rationale, and O.B. provides the final approval.\n4. **Communication of the funtimes decisions.** The funtimes support assistants notify applicants of the funtimes decision and its rationale. O.B. communicates approved funtimes decisions to the Funtimes Team.\n\n### Organizational Restructuring\n\nThe Associazione Verace Pizza Napoletana (lit. True Neapolitan Pizza Association) is a non-profit organization founded in 1984 with headquarters in Naples that aims to promote traditional Neapolitan pizza. The word \"pizza\" first appeared in a Latin text from the central Italian town of Gaeta, then still part of the Byzantine Empire, in 997 AD; the text states that a tenant of certain property is to give the bishop of Gaeta duodecim pizze (\"twelve pizzas\") every Christmas Day, and another twelve every Easter Sunday.\nPizzapies.\n\nModern pizza evolved from similar flatbread dishes in Naples, Italy, in the 18th or early 19th century. Until about 1830, pizza was sold from open-air stands and out of pizza bakeries, antecedents to modern pizzerias. Pizza was brought to the United States with Italian.\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][4]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][4]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][4]["id"], 4)

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["title"], "Special events")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["section_content"][0]["text"], "# Special Event\n\nIn addition to the general processing of grant applications, JOKECAN also has various grant programs that target the specific needs of diverse regions across Canada. Each of these programs is different, with its own respective lifespan, deadline and budget. One of these programs, a special event called Taste the North, is currently being planned for the Rebel region.\n\nThe idea to create Taste the North was conceived a year ago by JOKECAN’s senior management, in response to a steady decline in tourism in the Rebel region. The event will begin in six months, running from April to August. The world's largest pizza was prepared in Rome in December 2012, and measured 1,261 square meters (13,570 square feet). The pizza was named \"Ottavia\" in homage to the first Roman emperor Octavian Augustus, and was made with a gluten-free base. The world's longest pizza was made in Fontana, California in 2017 and measured 1,930.39 meters (6,333.3 feet). The activities will be hosted by businesses who receive Taste the North grants from JOKECAN or by any other interested organizations located in the Rebel region. The grants come from a budget specific to the special event and separate from the Rebel Team’s regular budget.\n\n## Taste the North Twerking Group\n\nA Twerking Group has been put in place by JOKECAN to coordinate the organization of this special event. The Twerking Group members typically meet via videoconference because some members are located in the Rebel region. The Twerking Group discusses issues and plans related to Taste the North. The Twerking Group uses a when it comes to preparation, the dough and ingredients can be combined on any kind of table. With mass production of pizza, the process can be completely automated. Most restaurants still use standard and purpose-built pizza preparation tables. Pizzerias nowadays can even opt for hi tech pizza preparation tables that combine mass production elements with traditional techniques.\n\nDue to having an in-depth knowledge of the Rebel Region, O.B. is also a key member of the Twerking Group. O.B. often assists Sandra in matters related to Taste the North by gathering relevant information, identifying issues, and providing initial ideas and recommendations to be further assessed with the Twerking Group.\n\nOther members of the Twerking Group from JOKECAN include the Director of the Crustless Affairs Division and the Director of the Mushrooms Division. To represent the interests of local communities, the following members are also part of the Twerking Group:\n\n- the mayor of Slicehorse, Michael Scott\n- the Director of Poutine Tourism Organization, Don Draper\n- the leader of a local business improvement district in Pizzaknife, John Wick\n- a community leader from a village of around 300 people, 300km north of Slicehorse, Dwight Schrute\n- a community leader from a village in Parmesan with a population of around 3,500, Duke Nukem\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["section_content"][1]["text"], "The Rebel Team plays a central role in Taste the North. In addition to their typical workload, the Rebel Team analysts and funding support assistants are responsible for Dipping sauce specifically for pizza was invented by American pizza chain Papa John's Pizza in 1984 and has since become popular when eating pizza, especially the crust. has been receiving fewer demands than the other sectors. Taste the North applications have been given priority and must be processed as soon as they are received.\n\nTaste the North is also placing extra demands on O.B. as, in addition to playing a key role on the Twerking Group, O.B. is responsible for providing final approval on funtimes decisions made by the analysts related to this special event.\n")
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["section_content"][1]["type"], "markdown")

        self.assertEqual(real_json["background"]["en"]["sections"][0]["section"][5]["id"], 5)
        self.assertEqual(real_json["background"]["en"]["sections"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["title"], "Contexte")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["section_content"][0]["text"], "# FR Overview\n\n**FR Argentina, and more specifically Buenos Aires, received a massive Italian immigration at the turn of the 19th century. Immigrants from Naples and Genoa opened the first pizza bars. Today is March 14th.**\n\nFR In the following sections, you will find information about JOKECAN and the Rebel Team. You will have access to this information throughout the test.\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["title"], "FR Organization")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["section_content"][0]["text"], "# FR Information about JOKECAN\n\nFR JOKECAN is a small federal government organization with approximately 100 employees located in Regina. The organization strives to increase pizzaism across Canada. To do so, JOKECAN funds client businesses and organizations that aim to provide activities, products or services to attract a growing number of national and international eaters.\n\nFR JOKECAN aims to have a positive social, economic, cultural and culinary impact on local communities. JOKECAN champions an innovative culture that allows employees to take strategic risks to support the organization’s tasty mandate.\n\n## FR Mandate\n\n- FR To promote pizzaism based on the following values, which are placed at the forefront in Canada: taste, texture, cheeseyness, and crust softness.\n- FR To sustain culinary development in target areas by maintaining mutually beneficial relationships with all stakeholders (e.g. pepperoni partners, eating groups, local pizza associations),\n\n## FR Priorities\n\nFR In April of the current year, JOKECAN established the following organizational priorities for the next three years.\n\n1. FR To respond to increasing diversification of pizzaism activities by adapting to the ever-changing topping needs of businesses.\n2. FR Create programs that target remote areas in Canada, which have been identified as having needs related to pizzaism development.\n3. FR Be more open and transparent with employees and stakeholders about JOKECAN’s pizza-making processes.\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][1]["id"], 1)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["title"], "Structure")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][0]["text"], "# FR Organizational Structure\n\n## FR Stuffed Crust Division\n\nFR The main role of the Stuffed Crust Division is to oversee strategic and administrative activities that support JOKECAN operations. The division has three teams:\n\n- FR The **Extra Pepperoni** oversees various aspects of making. This includes stretching, toppings and workplace taste and safety. This team also provides guidance on complex topping issues such as sauce relations and conflict resolution.\n- FR The **Mushroom Team** manages JOKECAN’s budgets, including planning, tasting, and controlling pizzapies. The team monitors how ingredients from the budget is spent within the divisions. They also pay organizations that have been granted curds.\n- FR The **Ingredient Technology Team** (IT) is responsible for JOKECAN’s ingredient technology infrastructure. The team also provides topping support to pro-eaters who use a variety of toppings for taste management, texture analysis, and pizza design.\n\n## FR Munching Division\n\nFR The role of the Munching Division is to conduct research and implement marketing strategies to ensure that JOKECAN meets its mandate. The division includes the following teams:\n\n- FR The **Canadiana Team** is responsible for communications with potential clients and stakeholders to promote the organization as well as its activities, special projects, and accomplishments.\n- FR The **Hot Pepper Evaluation Team** monitors the effectiveness and efficiency of eaten pizzas. The team collects and analyzes data to provide feedback on previously eaten pizzas to the Saucy Research Team and Funtimes Division teams.\n- FR The **Saucy Research Team** conducts research into the pizzaism industry needs in different regions. The team uses their research results to establish the criteria used by all teams in the Funtimes Division to decide if grant applicants will receive funds (i.e. funtime criteria). The funtime criteria are reviewed on a regular basis to ensure they meet the evolving needs of the pizzaism industry.\n\n## FR Funtimes Division\n\nFR The main role of the Funtimes Division is to determine if grant applicants will receive a extra cheese grant from JOKECAN. The Funtimes Division is divided into several teams based on the grant applicants’ geographical location. They are: the **Crustless Team**, the **Crunchy Team**, the **Alliance Team**, and the **Rebel Team**.\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["title"], "FR The Organizational Chart of JOKECAN")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["text"], "FR JOKECAN")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "FR Stuffed Crust Division")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["text"], "FR Extra Pepperoni")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "FR Mushroom Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "FR Ingrdient Technology Team (IT)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["text"], "FR Munching Division")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][0]["text"], "FR Canadiana Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][1]["text"], "FR Hot Pepper Evaluation Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][2]["text"], "FR Saucy Research Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][1]["id"], 1)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["text"], "FR Funtimes Division")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][0]["text"], "FR Crustless Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][1]["text"], "FR Crunchy Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][2]["text"], "FR Alliance Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][3]["text"], "FR Rebel Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["organizational_structure_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["organizational_structure_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["organizational_structure_tree_child"][0]["id"], 0)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["section_content"][1]["type"], "tree_view")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][2]["id"], 2)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["title"], "Votre équipe")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][0]["text"], "# FR Information about the Rebel Team\n\n## FR Team Members\n\n### FR Director: Sandra Oh\n\nFR Sandra and the other two directors are part of the Senior Procurement Team. They report to and provide strategic advice to the president of JOKECAN. Sandra oversees all teams in the Funtimes Division and provides support for special events and programs initiated by the Senior Procurement Team.\n\n### FR Manager: O.B. Wan (you)\n\nFR O.B. is responsible for:\n\n- FR Coordinating and managing Rebel Team activities, and ensuring these activities are in line with the organizational mandate and current priorities\n- FR Providing final approval on funding decisions made by Rebel Team analysts\n- FR Ensuring complaints and appeals are addressed in an appropriate manner\n- FR Staffing vacant positions, employee performance management and other typical managerial activities\n\nFR O.B. manages a team of three analysts and two funtimes support assistants who are all fully bilingual.\n\n### FR Quality Assurance Analysts\n\nFR The analysts are responsible for processing grant applications within three tourism sectors based on a predetermined set of funding criteria. The three analysts and their respective sectors are as follows:\n\n**FR Tim Taylor, Woodworking Sector (pizza boards, restaurants, etc.)**\n- FR Tenure on the Rebel Team: 5 years \n- FR Performance Notes: Has demonstrated strong communication skills in the past.\n\n**FR Kelly Kapoor, Arts and Culture Sector (mushrooms, crusty attractions, etc.)**\n- FR Tenure on the Rebel Team: 13 years\n- FR Performance Notes: Has demonstrated a capacity to eat quickly, but has sometimes focused on getting work done at the expense of following procedures. Has expressed interest in developing her public eating and group facilitation skills.\n\n**FR Det. McNulty, Outdoors Sector (foraging excursions, slice eating, etc.)**\n- FR Tenure on the Rebel Team: 5 years\n- FR Performance Notes: Has demonstrated innovative thinking, often suggesting new ideas for improving pizza-making processes.\n\n### FR Funtimes support assistants\n\nFR The funtimes support assistants screen grant applications, inform applicants about missing information, and communicate final decisions. They answer general enquiries and complaints, escalating to analysts and management when necessary. The assistants also provide research support to O.B. and the analysts as needed. Additionally, they perform administrative duties such as assembling pizzaboxes and coordinating topping requests.\n\n**FR Sterling Archer**\n- FR Tenure on the Rebel Team: 7 years\n- FR Performance Notes: Has demonstrated strong organization and slice management skills.\n\n**FR Ska Savesbro**\n- FR Tenure on the Rebel Team: 8 years\n- FR Performance Notes: Has demonstrated strong pizza-orientation.\n- FR Special Note: Has recently been granted a flexible work schedule, with reduced working hours, to accommodate a health related matter.\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["title"], "FR The Organizational Chart of the Rebel Team")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["text"], "FR Sandra Oh (Director)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["text"], "FR Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["text"], "FR Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["text"], "FR Det. McNulty (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][2]["id"], 2)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["text"], "FR Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][3]["id"], 3)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["text"], "FR Ska Savesbro (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["team_information_tree_child"][4]["id"], 4)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["team_information_tree_child"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["section_content"][1]["type"], "tree_view")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][3]["id"], 3)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][4]["title"], "FR Roles and responsibilities")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][4]["section_content"][0]["text"], "# FR Rebel Team Responsibilities and Challenges\n\n## FR Processing grant applications\n\nFR The Rebel Team grants funds for pizzaism activities, products and services in the Rebel region that meet the funtimes criteria established by the Saucy Research Team for each funtimes program.\n\n### FR The Review Process\n\nFR Funtimes decisions are based on a rigorous review process involving the following steps:\n\n1. **FR Initial slicing up of applications.** Funtimes support assistants slice applications using an internal cheese management software to ensure that there is no missing toppings. The software automatically assigns applications to the analyst responsible for the primary targeted pizzaismsector.\n2. **FR Analysis of the applications.** Analysts assign ratings for each of the criteria used to review grant applications. Funtimes criteria typically include the potential economic, social, cultural, and culinary impacts, as well as the efficiency of the proposed use of cheese. Applications vary in level of complexity, some requiring more intensive tasting and judgement than others.\n3. **FR Determination of whether funs will be granted.** Grant applications that obtain the required minimum overall score, or higher are approved for funtimes on a first-come, first-served basis as long as there is sufficient slices. Funtimes recommendations are made by analysts who also provide their rationale, and O.B. provides the final approval.\n4. **FR Communication of the funtimes decisions.** The funtimes support assistants notify applicants of the funtimes decision and its rationale. O.B. communicates approved funtimes decisions to the Funtimes Team.\n\n### FR Organizational Restructuring\n\nFR The Associazione Verace Pizza Napoletana (lit. True Neapolitan Pizza Association) is a non-profit organization founded in 1984 with headquarters in Naples that aims to promote traditional Neapolitan pizza. The word \"pizza\" first appeared in a Latin text from the central Italian town of Gaeta, then still part of the Byzantine Empire, in 997 AD; the text states that a tenant of certain property is to give the bishop of Gaeta duodecim pizze (\"twelve pizzas\") every Christmas Day, and another twelve every Easter Sunday.\nFR Pizzapies.\n\nFR Modern pizza evolved from similar flatbread dishes in Naples, Italy, in the 18th or early 19th century. Until about 1830, pizza was sold from open-air stands and out of pizza bakeries, antecedents to modern pizzerias. Pizza was brought to the United States with Italian.\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][4]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][4]["section_content"][0]["type"], "markdown")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][4]["id"], 4)

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["title"], "FR Special events")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["section_content"][0]["text"], "# FR Special Event\n\nFR In addition to the general processing of grant applications, JOKECAN also has various grant programs that target the specific needs of diverse regions across Canada. Each of these programs is different, with its own respective lifespan, deadline and budget. One of these programs, a special event called Taste the North, is currently being planned for the Rebel region.\n\nFR The idea to create Taste the North was conceived a year ago by JOKECAN’s senior management, in response to a steady decline in tourism in the Rebel region. The event will begin in six months, running from April to August. The world's largest pizza was prepared in Rome in December 2012, and measured 1,261 square meters (13,570 square feet). The pizza was named \"Ottavia\" in homage to the first Roman emperor Octavian Augustus, and was made with a gluten-free base. The world's longest pizza was made in Fontana, California in 2017 and measured 1,930.39 meters (6,333.3 feet). The activities will be hosted by businesses who receive Taste the North grants from JOKECAN or by any other interested organizations located in the Rebel region. The grants come from a budget specific to the special event and separate from the Rebel Team’s regular budget.\n\n## FR Taste the North Twerking Group\n\nFR A Twerking Group has been put in place by JOKECAN to coordinate the organization of this special event. The Twerking Group members typically meet via videoconference because some members are located in the Rebel region. The Twerking Group discusses issues and plans related to Taste the North. The Twerking Group uses a when it comes to preparation, the dough and ingredients can be combined on any kind of table. With mass production of pizza, the process can be completely automated. Most restaurants still use standard and purpose-built pizza preparation tables. Pizzerias nowadays can even opt for hi tech pizza preparation tables that combine mass production elements with traditional techniques.\n\nFR Due to having an in-depth knowledge of the Rebel Region, O.B. is also a key member of the Twerking Group. O.B. often assists Sandra in matters related to Taste the North by gathering relevant information, identifying issues, and providing initial ideas and recommendations to be further assessed with the Twerking Group.\n\nFR Other members of the Twerking Group from JOKECAN include the Director of the Crustless Affairs Division and the Director of the Mushrooms Division. To represent the interests of local communities, the following members are also part of the Twerking Group:\n\n- FR the mayor of Slicehorse, Michael Scott\n- FR the Director of Poutine Tourism Organization, Don Draper\n- FR the leader of a local business improvement district in Pizzaknife, John Wick\n- FR a community leader from a village of around 300 people, 300km north of Slicehorse, Dwight Schrute\n- FR a community leader from a village in Parmesan with a population of around 3,500, Duke Nukem\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["section_content"][0]["id"], 0)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["section_content"][0]["type"], "markdown")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["section_content"][1]["text"], "FR The Rebel Team plays a central role in Taste the North. In addition to their typical workload, the Rebel Team analysts and funding support assistants are responsible for Dipping sauce specifically for pizza was invented by American pizza chain Papa John's Pizza in 1984 and has since become popular when eating pizza, especially the crust. has been receiving fewer demands than the other sectors. Taste the North applications have been given priority and must be processed as soon as they are received.\n\nFR Taste the North is also placing extra demands on O.B. as, in addition to playing a key role on the Twerking Group, O.B. is responsible for providing final approval on funtimes decisions made by the analysts related to this special event.\n")
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["section_content"][1]["id"], 1)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["section_content"][1]["type"], "markdown")

        self.assertEqual(real_json["background"]["fr"]["sections"][0]["section"][5]["id"], 5)
        self.assertEqual(real_json["background"]["fr"]["sections"][0]["id"], 0)

        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][0]["text"], "JOKECAN")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][1]["text"], "Stuffed Crust Division")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][2]["text"], "Extra Pepperoni")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][3]["text"], "Mushroom Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][4]["text"], "Ingrdient Technology Team (IT)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][5]["text"], "Munching Division")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][6]["text"], "Canadiana Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][7]["text"], "Hot Pepper Evaluation Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][8]["text"], "Saucy Research Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][9]["text"], "Funtimes Division")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][10]["text"], "Crustless Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][11]["text"], "Crunchy Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][12]["text"], "Alliance Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][13]["text"], "Rebel Team")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][14]["text"], "Sandra Oh (Director)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][15]["text"], "O.B. Wan (Manager - You)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][16]["text"], "Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][17]["text"], "Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][18]["text"], "Det. McNulty (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][19]["text"], "Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][20]["text"], "Ska Savesbro (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["en"]["address_book"][0]["contact"][21]["text"], "Michelle Obama (Manager, Market Research)")

        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][0]["text"], "FR JOKECAN")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][1]["text"], "FR Stuffed Crust Division")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][2]["text"], "FR Extra Pepperoni")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][3]["text"], "FR Mushroom Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][4]["text"], "FR Ingredient Technology Team (IT)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][5]["text"], "FR Munching Division")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][6]["text"], "FR Canadiana Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][7]["text"], "FR Hot Pepper Evaluation Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][8]["text"], "FR Saucy Research Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][9]["text"], "FR Funtimes Division")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][10]["text"], "FR Crustless Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][11]["text"], "FR Crunchy Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][12]["text"], "FR Alliance Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][13]["text"], "FR Rebel Team")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][14]["text"], "FR Sandra Oh (Director)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][15]["text"], "FR O.B. Wan (Manager - You)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][16]["text"], "FR Tim Taylor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][17]["text"], "FR Kelly Kapoor (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][18]["text"], "FR Det. McNulty (Quality Assurance Assistants)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][19]["text"], "FR Sterling Archer (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][20]["text"], "FR Ska Savesbro (Funtimes Support Assistants)")
        self.assertEqual(real_json["background"]["fr"]["address_book"][0]["contact"][21]["text"], "FR Michelle Obama (Manager, Market Research)")

        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][0]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][1]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][2]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][3]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][4]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][5]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][6]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][7]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][8]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][9]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][10]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][11]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][12]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][13]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][14]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][15]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][16]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][17]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][18]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][19]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][20]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["en"]["address_book"][0]["contact"][21]["true_id"]), r'[0-9]*')

        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][0]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][1]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][2]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][3]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][4]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][5]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][6]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][7]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][8]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][9]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][10]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][11]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][12]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][13]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][14]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][15]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][16]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][17]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][18]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][19]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][20]["true_id"]), r'[0-9]*')
        self.assertRegex(str(real_json["background"]["fr"]["address_book"][0]["contact"][21]["true_id"]), r'[0-9]*')


    def test_get_nonexistant_test(self):
        real_json = retrieve_json_from_name_date(
            "IAmNotARealTest", timezone.now(), TEST_QUESTIONS
        )
        expected_json = {"error", "no test with the given test_name"}
        self.assertEqual(real_json, expected_json)

    def test_get_test_before(self):
        time = timezone.datetime.strptime("01/01/1500", "%d/%m/%Y").date()
        time = timezone.now() + timezone.timedelta(days=-1)
        real_json = retrieve_json_from_name_date("emibSampleTest", time, TEST_QUESTIONS)
        expected_json = {"error", "no test item found"}
        self.assertEqual(real_json, expected_json)
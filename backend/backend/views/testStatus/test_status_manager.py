from datetime import datetime
from rest_framework.response import Response
from math import ceil
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus


def update_test_status(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)
    test.status = new_status
    return save_and_return(test)


def activate_test(test_id, start_date, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if test.status != AssignedTestStatus.ACTIVE:
        # /1000 because js timestamps are in milliseconds and python in seconds
        test.start_date = datetime.fromtimestamp(ceil(float(start_date) / 1000))

    test.status = new_status

    return save_and_return(test)


def submit_test(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if new_status in (
        AssignedTestStatus.SUBMITTED,
        AssignedTestStatus.TIMED_OUT,
        AssignedTestStatus.QUIT,
    ):
        test.submit_date = datetime.now()

    test.status = new_status

    return save_and_return(test)


def save_and_return(test):
    test.save()
    return Response()

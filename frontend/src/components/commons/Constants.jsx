const PATH = {
  login: "/login",
  dashboard: "/check-in",
  status: "/status",
  emibSampleTest: "/emib-sample",
  test: "/active",
  testBase: "/test",
  sampleTests: "/sample-tests",
  instructions: "/instructions",
  overview: "/overview",
  submit: "/submit",
  quit: "/quit",
  testAdministration: "/test-sessions",
  profile: "/profile",
  incidentReport: "/incident-report",
  notifications: "/notifications",
  myTests: "/my-tests",
  contactUs: "/contact-us",
  systemAdministration: "/system-administration",
  ppcAdministration: "/ppc-administration"
};

const STATUS_API_PATH = {
  activate: "/api/activate-test/",
  submit: "/api/submit-test/",
  timeout: "/api/timeout-test/",
  quit: "/api/quit-test/"
};

export { PATH, STATUS_API_PATH };

#!bin/bash

# all our models should be under custom_models
# checking if there are any new migrations
python manage.py makemigrations
# all initial and new migrations will be migrated
# we need to migrate the models one by one because of the dependencies of permission model
# when doing "python manage.py migrate", we don't control the order, so it causes errors
python manage.py migrate auth
python manage.py migrate admin
python manage.py migrate authtoken
python manage.py migrate contenttypes
python manage.py migrate sessions
python manage.py migrate cms_models
python manage.py migrate custom_models
python manage.py migrate user_management_models
# starting web server
python manage.py runserver 0.0.0.0:8000

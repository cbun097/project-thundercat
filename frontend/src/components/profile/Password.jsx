import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/password.css";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

const styles = {
  container: {
    padding: "12px 0 0 24px"
  },
  passwordRecoveryMargin: {
    marginTop: 36
  },
  passwordInput: {
    width: 270,
    height: 38,
    padding: "3px 40px 3px 6px",
    borderRadius: 4
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  label: {
    marginTop: 6
  },
  contentContainer: {
    display: "inline-block",
    marginTop: 24
  },
  firstColumn: {
    width: 215,
    float: "left"
  },
  secondColumn: {
    float: "right"
  },
  dropdown: {
    width: 550
  },
  inputIcon: {
    position: "absolute",
    margin: "5px 0 0 228px",
    fontSize: "20px",
    color: "#00565e"
  },
  iconPadding: {
    padding: "5px 9px"
  }
};

class Password extends Component {
  state = {
    currentPasswordContent: "",
    viewCurrentPassword: false,
    currentPasswordInputType: "password",
    newPasswordContent: "",
    viewNewPassword: false,
    newPasswordInputType: "password",
    confirmPasswordContent: "",
    viewConfirmPassword: false,
    confirmPasswordInputType: "password",
    secretQuestionOptions: [],
    secretQuestionSelectedValue: null,
    secretAnswerContent: ""
  };

  componentDidMount = () => {
    this.populateSecretQuestionOptions();
  };

  // TODO (fnormand): create new model with secret questions + create new view to get the questions and populate them using this function here
  // populate secret question options
  populateSecretQuestionOptions = () => {
    let secretQuestionsArray = [];
    // 5 temporary questions
    secretQuestionsArray.push({ value: 0, label: "Question 1..." });
    secretQuestionsArray.push({ value: 1, label: "Question 2..." });
    secretQuestionsArray.push({ value: 2, label: "Question 3..." });
    secretQuestionsArray.push({ value: 3, label: "Question 4..." });
    secretQuestionsArray.push({ value: 4, label: "Question 5..." });
    //save result in state
    this.setState({ secretQuestionOptions: secretQuestionsArray });
  };

  // update current password content
  updateCurrentPasswordContent = event => {
    const currentPasswordContent = event.target.value;
    this.setState({
      currentPasswordContent: currentPasswordContent
    });
  };

  // update new password content
  updateNewPasswordContent = event => {
    const newPasswordContent = event.target.value;
    this.setState({
      newPasswordContent: newPasswordContent
    });
  };

  // update confirm password content
  updateConfirmPasswordContent = event => {
    const confirmPasswordContent = event.target.value;
    this.setState({
      confirmPasswordContent: confirmPasswordContent
    });
  };

  // update secret answer content
  updateSecretAnswerContent = event => {
    const secretAnswerContent = event.target.value;
    this.setState({
      secretAnswerContent: secretAnswerContent
    });
  };

  // get selected secret question
  getSelectedSecretQuestion = selectedOption => {
    this.setState({ secretQuestionSelectedValue: selectedOption });
  };

  // toggle state to display or not the current password
  toggleCurrentPasswordView = () => {
    this.setState({ viewCurrentPassword: !this.state.viewCurrentPassword }, () => {
      if (this.state.viewCurrentPassword) {
        this.setState({ currentPasswordInputType: "text" });
      } else {
        this.setState({ currentPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the new password
  toggleNewPasswordView = () => {
    this.setState({ viewNewPassword: !this.state.viewNewPassword }, () => {
      if (this.state.viewNewPassword) {
        this.setState({ newPasswordInputType: "text" });
      } else {
        this.setState({ newPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the confirm password
  toggleConfirmPasswordView = () => {
    this.setState({ viewConfirmPassword: !this.state.viewConfirmPassword }, () => {
      if (this.state.viewConfirmPassword) {
        this.setState({ confirmPasswordInputType: "text" });
      } else {
        this.setState({ confirmPasswordInputType: "password" });
      }
    });
  };

  render() {
    const {
      currentPasswordContent,
      currentPasswordInputType,
      newPasswordContent,
      newPasswordInputType,
      confirmPasswordContent,
      confirmPasswordInputType,
      secretAnswerContent,
      secretQuestionOptions,
      secretQuestionSelectedValue
    } = this.state;
    return (
      <div>
        <div>
          <h2>{LOCALIZE.profile.password.newPassword.title}</h2>
          <div style={styles.container}>
            <p>
              {LOCALIZE.formatString(
                LOCALIZE.profile.password.newPassword.updatedDate,
                // TODO (fnormand): get the last password update date from the database
                "01/01/1900"
              )}
            </p>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="current-password" htmlFor="current-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.currentPassword}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <div style={styles.inputIcon} onClick={this.toggleCurrentPasswordView}>
                  <span style={styles.iconPadding}>
                    <FontAwesomeIcon
                      icon={currentPasswordInputType === "password" ? faEyeSlash : faEye}
                    />
                  </span>
                </div>
                <input
                  id="current-password-input"
                  className="valid-field"
                  aria-labelledby="current-password"
                  aria-required={true}
                  style={styles.passwordInput}
                  type={currentPasswordInputType}
                  value={currentPasswordContent}
                  onChange={this.updateCurrentPasswordContent}
                ></input>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="new-password" htmlFor="new-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.newPassword}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <div style={styles.inputIcon} onClick={this.toggleNewPasswordView}>
                  <span style={styles.iconPadding}>
                    <FontAwesomeIcon
                      icon={newPasswordInputType === "password" ? faEyeSlash : faEye}
                    />
                  </span>
                </div>
                <input
                  id="new-password-input"
                  className="valid-field"
                  aria-labelledby="new-password"
                  aria-required={true}
                  style={styles.passwordInput}
                  type={newPasswordInputType}
                  value={newPasswordContent}
                  onChange={this.updateNewPasswordContent}
                ></input>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="confirm-password" htmlFor="confirm-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.confirmPassword}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <div style={styles.inputIcon} onClick={this.toggleConfirmPasswordView}>
                  <span style={styles.iconPadding}>
                    <FontAwesomeIcon
                      icon={confirmPasswordInputType === "password" ? faEyeSlash : faEye}
                    />
                  </span>
                </div>
                <input
                  id="confirm-password-input"
                  className="valid-field"
                  aria-labelledby="confirm-password"
                  aria-required={true}
                  style={styles.passwordInput}
                  type={confirmPasswordInputType}
                  value={confirmPasswordContent}
                  onChange={this.updateConfirmPasswordContent}
                ></input>
              </div>
            </div>
          </div>
        </div>
        <div style={styles.passwordRecoveryMargin}>
          <h2>{LOCALIZE.profile.password.passwordRecovery.title}</h2>
          <div style={styles.container}>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="secret-question" style={styles.label}>
                  <p>{LOCALIZE.profile.password.passwordRecovery.secretQuestion}</p>
                </label>
              </div>
              <div style={{ ...styles.secondColumn, ...styles.dropdown }}>
                <Select
                  id="secret-question-select"
                  className="valid-field"
                  name="secret-question-name"
                  aria-labelledby={"secret-question"}
                  aria-required={true}
                  options={secretQuestionOptions}
                  onChange={this.getSelectedSecretQuestion}
                  clearable={false}
                  value={secretQuestionSelectedValue}
                ></Select>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="secret-answer" htmlFor="secret-answer-input" style={styles.label}>
                  {LOCALIZE.profile.password.passwordRecovery.secretAnswer}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <input
                  id="secret-answer-input"
                  className="valid-field"
                  aria-labelledby="secret-answer"
                  aria-required={true}
                  style={styles.input}
                  type="text"
                  value={secretAnswerContent}
                  onChange={this.updateSecretAnswerContent}
                ></input>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export { Password as unconnectedPassword };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Password);

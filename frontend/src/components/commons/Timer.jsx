import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock, faMinusCircle, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TimerCalculation from "./TimerCalculation";
import { LANGUAGES } from "./Translation";

const styles = {
  container: {
    width: 350
  },
  timeContainer: {
    float: "left",
    border: "2px solid #00565e",
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
    height: 38,
    width: 100,
    padding: 4,
    fontWeight: "bold",
    color: "#00565e",
    fontSize: 18,
    backgroundColor: "#FFFFFF"
  },
  toggleButton: {
    float: "left",
    borderBottomLeftRadius: 0,
    borderTopLeftRadius: 0,
    height: 38
  },
  label: {
    paddingLeft: 5
  },
  timeOut: {
    color: "#D3080C"
  },
  englishTimer: {
    width: 100
  },
  frenchTimer: {
    width: 130
  }
};

class Timer extends Component {
  static propTypes = {
    timeout: PropTypes.func.isRequired,
    startTime: PropTypes.string,
    timeLimit: PropTypes.number
  };

  state = {
    hidden: false
  };

  toggleVisibility = () => {
    this.setState({ hidden: !this.state.hidden });
  };

  timeout = () => {
    this.props.timeout();
  };

  render() {
    const { hidden } = this.state;
    const isTimeAlmostOut = false;

    return (
      <div style={styles.container}>
        <div style={styles.timeContainer}>
          {hidden && (
            <span>
              <span className="visually-hidden">
                {LOCALIZE.emibTest.testFooter.timer.timerHidden}
              </span>
              <FontAwesomeIcon style={isTimeAlmostOut ? styles.timeOut : {}} icon={faClock} />
            </span>
          )}
          {!hidden && (
            <div style={isTimeAlmostOut ? styles.timeOut : {}}>
              <span className="visually-hidden" id="unit-test-time-label">
                {LOCALIZE.emibTest.testFooter.timer.timeLeft}
              </span>
              <TimerCalculation
                startTime={this.props.startTime}
                timeLimit={this.props.timeLimit}
                timeoutFunction={this.timeout}
              />
            </div>
          )}
          {hidden && (
            <div className="visually-hidden">
              <TimerCalculation
                startTime={this.props.startTime}
                timeLimit={this.props.timeLimit}
                timeoutFunction={this.timeout}
              />
            </div>
          )}
        </div>
        <Button
          id="unit-test-toggle-timer"
          style={
            this.props.currentLanguage === LANGUAGES.english
              ? { ...styles.toggleButton, ...styles.englishTimer }
              : { ...styles.toggleButton, ...styles.frenchTimer }
          }
          onClick={this.toggleVisibility}
        >
          <FontAwesomeIcon icon={hidden ? faPlusCircle : faMinusCircle} />
          <span style={styles.label}>{LOCALIZE.emibTest.testFooter.timer.timer}</span>
        </Button>
      </div>
    );
  }
}

export { Timer as UnconnectedTimer };

const mapStateToProps = (state, ownProps) => {
  return { currentLanguage: state.localize.language };
};

export default connect(
  mapStateToProps,
  null
)(Timer);

import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getAssignedTests } from "../../modules/AssignedTestsRedux";
import { startTest, setCurrentTest } from "../../modules/TestStatusRedux";
import { PATH } from "../commons/Constants";
import {
  getTestContent,
  updateTestBackgroundState,
  getTotalTestTime
} from "../../modules/LoadTestContentRedux";
import {
  updateEmailsEnState,
  updateEmailsFrState,
  updateEmailsState
} from "../../modules/EmibInboxRedux";
import { getTestMetaData, updateTestMetaDataState } from "../../modules/LoadTestContentRedux";
import { Button } from "react-bootstrap";
import { loadResponses } from "../../modules/EmibInboxRedux";
import { getResponses } from "../../modules/UpdateResponseRedux";
import { LANGUAGES } from "../commons/Translation";
import CandidateCheckIn from "../../CandidateCheckIn";

const styles = {
  table: {
    width: "100%",
    marginTop: 24,
    border: "2px solid #00565e",
    borderCollapse: "separate",
    borderSpacing: 0,
    // border radius = 7px, not 4px like the <th> elements to avoid display bug
    borderRadius: "7px 7px 0 0"
  },
  th: {
    paddingLeft: 24,
    height: 60,
    backgroundColor: "#00565e",
    color: "white",
    width: "50%"
  },
  borderRight: {
    borderRight: "1px solid #cdcdcd"
  },
  topLeftBorderRadius: {
    borderTopLeftRadius: 4
  },
  topRightBorderRadius: {
    borderTopRightRadius: 4
  },
  td: {
    padding: "12px 0 12px 24px"
  },
  centerText: {
    textAlign: "center"
  },
  viewButton: {
    width: 175
  }
};

class AssignedTestTable extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    username: PropTypes.string,
    // Props from Redux
    getAssignedTests: PropTypes.func,
    setCurrentTest: PropTypes.func,

    //props for reloading a test
    updateEmailsEnState: PropTypes.func,
    updateEmailsFrState: PropTypes.func,
    updateEmailsState: PropTypes.func,
    getResponses: PropTypes.func,
    loadResponses: PropTypes.func,
    getTestContent: PropTypes.func,
    updateTestBackgroundState: PropTypes.func,
    getTestMetaData: PropTypes.func.isRequired,
    updateTestMetaDataState: PropTypes.func.isRequired,
    startTest: PropTypes.func.isRequired
  };

  state = {
    username: "",
    assigned_tests: [{}],
    isLoaded: false,
    activeTest: false,
    id: null,
    testId: null,
    startTime: null,
    isCheckedIn: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    //remove active test variable if it exists
    localStorage.removeItem("testActive");

    //will be defined except in tests
    if (typeof this.props.getAssignedTests !== "undefined") {
      this.populateTable();
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  populateTable = () => {
    this.props
      .getAssignedTests(this.props.username, localStorage.getItem("auth_token"))
      .then(response => {
        /* displayTableContent state is used here to avoid unwanted table content display, such as
        the flashing view button when refreshing the page */
        if (this._isMounted) {
          // verifying if there is an active test
          this.checkForActiveTest(response);
        }
      });
  };

  loadTestContent = (testNameId, startTime, currentAssignedTestId) => {
    // getting questions of the test from the api
    this.props.getTestContent(testNameId).then(response => {
      // Load emails.
      if (this.props.currentLanguage === LANGUAGES.english) {
        this.props.updateEmailsState(response.questions.en.email);
      } else {
        this.props.updateEmailsState(response.questions.fr.email);
      }
      // saving questions content in emails, emailsEN and emailsFR states
      this.props.updateEmailsEnState(response.questions.en.email);
      this.props.updateEmailsFrState(response.questions.fr.email);

      // Load background info.
      this.props.updateTestBackgroundState(response.background);

      this.props.getTestMetaData(testNameId).then(response => {
        this.props.updateTestMetaDataState(response);

        this.props.getResponses(currentAssignedTestId).then(loadedResponses => {
          // If there was an error, such as durring first wave of loading, skip
          if (loadedResponses.error) {
            return;
          }
          this.props.loadResponses(loadedResponses);
        });

        this.props.startTest(response.default_time, currentAssignedTestId, startTime);
        this.props.history.push(PATH.testBase + PATH.test);
      });
    });
  };

  viewTest = (currentAssignedTestId, testId, startTime) => {
    // Sets the activeTest id for the current test and the testId in redux
    this.props.setCurrentTest(parseInt(currentAssignedTestId), testId, startTime);
    this.props.history.push(PATH.testBase + PATH.overview);
  };

  /* checking in the assigned tests list if there is a status 5 (active test)
  return true if that's the case */
  checkForActiveTest = assigned_tests => {
    let activeTest = false;
    for (let i = 0; i < assigned_tests.length; i++) {
      if (assigned_tests[i].status === 5) {
        activeTest = true;
        this.props.setCurrentTest(
          parseInt(assigned_tests[i].id.toString()),
          assigned_tests[i].test.test_name,
          assigned_tests[i].start_date
        );
        this.loadTestContent(
          assigned_tests[i].test.test_name,
          assigned_tests[i].start_date,
          parseInt(assigned_tests[i].id.toString())
        );
      }
    }
    if (!activeTest) {
      this.setState({ assigned_tests: assigned_tests, isLoaded: true });
    }
  };

  handleCheckIn = () => {
    this.populateTable();
    this.setState({ ...this.state, isCheckedIn: true });
  };

  render() {
    return (
      <div>
        <div id="candidate-check-in">
          <CandidateCheckIn
            username={this.props.username}
            handleCheckIn={this.handleCheckIn}
            isCheckedIn={this.state.isCheckedIn}
            isLoaded={this.state.isLoaded}
          />
        </div>
        <div role="region" aria-labelledby="test-assignment-table">
          <table id="test-assignment-table" style={styles.table}>
            <tbody>
              <tr>
                <th style={{ ...styles.th, ...styles.borderRight, ...styles.topLeftBorderRadius }}>
                  {LOCALIZE.dashboard.table.columnOne}
                </th>
                <th style={{ ...styles.th, ...styles.topRightBorderRadius }}>
                  {LOCALIZE.dashboard.table.columnTwo}
                </th>
              </tr>
              {this.state.isLoaded &&
                this.state.assigned_tests instanceof Array &&
                this.state.assigned_tests.map((item, index) => {
                  return (
                    <tr key={index} id={"assigned-test-" + index}>
                      <td
                        style={{
                          ...styles.td,
                          ...styles.borderRight
                        }}
                      >
                        {this.props.currentLanguage === LANGUAGES.english
                          ? item.en_name
                          : item.fr_name}
                      </td>
                      <td
                        style={{
                          ...styles.td,
                          ...styles.centerText
                        }}
                      >
                        <Button
                          onClick={() =>
                            this.viewTest(item.id, item.test.test_name, item.start_date)
                          }
                          className="btn btn-primary"
                          style={styles.viewButton}
                        >
                          {LOCALIZE.dashboard.table.viewButton}
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              {this.state.isLoaded && this.state.assigned_tests.length === 0 && (
                <tr key={0} id="no-assigned-test-row">
                  <td style={{ ...styles.td, ...styles.borderRight }}>
                    {LOCALIZE.dashboard.table.noTests}
                  </td>
                  <td style={{ ...styles.td, ...styles.centerText }}></td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export { AssignedTestTable as UnconnectedAssignedTestTable };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testTimeInMinutes: getTotalTestTime(state)
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      setCurrentTest,
      updateTestBackgroundState,
      updateEmailsEnState,
      updateEmailsFrState,
      updateEmailsState,
      getResponses,
      loadResponses,
      getTestContent,
      getTestMetaData,
      updateTestMetaDataState,
      startTest
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AssignedTestTable));

export { styles as TEST_TABLE_STYLES };

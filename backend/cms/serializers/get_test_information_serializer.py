from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.test import Test
from backend.custom_models.item_text import ItemText


class FilterSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        # Exclude expired items
        query_date_time = timezone.now()
        data = (
            data.filter(is_public=False)
            .exclude(item__date_from__gt=query_date_time)
            .exclude(item__date_to__lt=query_date_time)
        )
        return super(FilterSerializer, self).to_representation(data)


# Serializers define the API representation
class GetNonPublicTestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        list_serializer_class = FilterSerializer
        fields = ("test_name", "is_public")


class GetTestNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemText
        fields = ("text_detail", "language_id")


class GetTestInternalNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = "__all__"

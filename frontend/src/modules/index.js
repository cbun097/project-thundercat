import { combineReducers } from "redux";
import localize from "./LocalizeRedux";
import login from "./LoginRedux";
import testStatus from "./TestStatusRedux";
import sampleTestStatus from "./SampleTestStatusRedux";
import emibInbox from "./EmibInboxRedux";
import loadTestContent from "./LoadTestContentRedux";
import userPermissions from "./PermissionsRedux";
import notepad from "./NotepadRedux";

export default combineReducers({
  localize,
  login,
  emibInbox,
  testStatus,
  sampleTestStatus,
  loadTestContent,
  userPermissions,
  notepad
});

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import LoginButton from "./components/commons/LoginButton";
import Settings from "./components/commons/Settings";
import Translation from "./components/commons/Translation";
import LOCALIZE from "./text_resources";
import psc_logo_en from "./images/psc_logo_en.png";
import psc_logo_fr from "./images/psc_logo_fr.png";
import canada_logo from "./images/canada_logo.png";
import { Navbar, Nav } from "react-bootstrap";
import SelectLanguage from "./SelectLanguage";
import { LANGUAGES } from "./modules/LocalizeRedux";
import { PATH } from "./components/commons/Constants";
import "./css/site-nav-bar.css";
import SiteNavMenu from "./SiteNavMenu";

const styles = {
  banner: {
    width: "100%"
  },
  navLinks: {
    color: "#00565e",
    fontSize: 20
  },
  menuStyle: {
    float: "left"
  },
  homeStyle: {
    float: "right"
  }
};

class SiteNavBar extends Component {
  static propTypes = {
    // Props from Redux
    currentLanguage: PropTypes.string,
    isTestActive: PropTypes.bool.isRequired,
    authenticateAction: PropTypes.func,
    logoutAction: PropTypes.func
  };

  state = {
    setFocusOnQuitTestButton: false
  };

  render() {
    // Determine if user has already selected a language.
    const isLanguageSelected = this.props.currentLanguage !== "";
    return (
      <div>
        <a href="#main-content" className="visually-hidden">
          {LOCALIZE.mainTabs.skipToMain}
        </a>
        {!isLanguageSelected && <SelectLanguage />}
        {isLanguageSelected && !this.props.isTestActive && (
          <div>
            <div>
              <div role="navigation">
                <Navbar className="nav-one" bg="light" variant="light">
                  <img
                    alt={LOCALIZE.mainTabs.psc}
                    src={
                      this.props.currentLanguage === LANGUAGES.french ? psc_logo_fr : psc_logo_en
                    }
                    width="370px"
                    height="27.75px"
                    className="d-inline-block align-top"
                  />
                  <img
                    alt={LOCALIZE.mainTabs.canada}
                    src={canada_logo}
                    width="120px"
                    height="28.77px"
                    className="d-inline-block align-top"
                  />
                </Navbar>
                <div>
                  <Navbar className="nav-two" bg="light" variant="light">
                    <div>
                      <Nav>
                        {!this.props.authenticated && (
                          <Nav.Link href={PATH.login} style={styles.navLinks}>
                            {LOCALIZE.mainTabs.homeTabTitleUnauthenticated}
                          </Nav.Link>
                        )}
                        {this.props.authenticated && (
                          <div>
                            <div style={styles.menuStyle}>
                              <SiteNavMenu />
                            </div>
                            <div style={styles.homeStyle}>
                              <Nav.Link
                                href={
                                  this.props.isEtta
                                    ? PATH.systemAdministration
                                    : this.props.isPpc
                                    ? PATH.ppcAdministration
                                    : this.props.isTa
                                    ? PATH.testAdministration
                                    : PATH.dashboard
                                }
                                style={styles.navLinks}
                              >
                                {LOCALIZE.mainTabs.homeTabTitleAuthenticated}
                              </Nav.Link>
                            </div>
                          </div>
                        )}
                        <Nav.Link href={PATH.sampleTests} style={styles.navLinks}>
                          {LOCALIZE.mainTabs.sampleTests}
                        </Nav.Link>
                      </Nav>
                    </div>
                    <div>
                      <Nav>
                        <LoginButton />
                        <Settings />
                        <Translation />
                      </Nav>
                    </div>
                  </Navbar>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
export { PATH };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    authenticated: state.login.authenticated,
    isEtta: state.userPermissions.isEtta,
    isPpc: state.userPermissions.isPpc,
    isTa: state.userPermissions.isTa
  };
};

export default connect(mapStateToProps)(withRouter(SiteNavBar));

import emibInbox, {
  initializeEmailSummaries,
  readEmail,
  addEmail,
  addTask,
  updateEmail,
  deleteEmail,
  deleteTask,
  updateTask,
  changeCurrentEmail,
  setResponseDbId,
  loadResponses,
  resetInboxState
} from "../../modules/EmibInboxRedux";
import { EMAIL_TYPE, ACTION_TYPE } from "../../components/eMIB/constants";
import { setLanguage } from "../../modules/LocalizeRedux";
import { emailsJson } from "../components/eMIB/sampleData";

describe("EmibInboxRedux", () => {
  let stubbedInitialState;
  beforeEach(() => {
    stubbedInitialState = {
      emails: emailsJson.questions.en.email,
      emailsEN: emailsJson.questions.en.email,
      emailsFR: emailsJson.questions.fr.email,
      emailSummaries: initializeEmailSummaries(emailsJson.questions.en.email.length),
      emailActions: [[], [], []],
      currentEmail: 0,
      assignedQuestionIds: [1, 2, 3]
    };
  });

  afterEach(() => {
    stubbedInitialState = {};
  });

  describe("reset inbox state", () => {
    it("should update state to initial state", () => {
      const realInitialState = {
        emails: {},
        emailsEN: {},
        emailsFR: {},
        emailSummaries: [],
        emailActions: [],
        currentEmail: 0,
        assignedQuestionIds: [],
        isTestLoadedFromDB: false
      };
      const action1 = setLanguage("fr");
      expect(emibInbox(stubbedInitialState, action1).emails).toEqual(emailsJson.questions.fr.email);

      const newState = emibInbox(realInitialState, resetInboxState);
      expect(newState).toEqual(realInitialState);
    });
  });

  describe("setLanguage action", () => {
    it("should update emails to french or english", () => {
      const action1 = setLanguage("fr");
      expect(emibInbox(stubbedInitialState, action1).emails).toEqual(emailsJson.questions.fr.email);
      const action2 = setLanguage("en");
      expect(emibInbox(stubbedInitialState, action2).emails).toEqual(emailsJson.questions.en.email);
    });
  });

  describe("read email action", () => {
    it("should update email 0 read state to true", () => {
      const readAction = readEmail(0);
      const newState = emibInbox(stubbedInitialState, readAction);
      expect(newState.emailSummaries[0].isRead).toEqual(true);
      expect(newState.emailSummaries[1].isRead).toEqual(false);
    });

    it("should update email 1 read state to true", () => {
      const readAction = readEmail(1);
      const newState = emibInbox(stubbedInitialState, readAction);
      expect(newState.emailSummaries[0].isRead).toEqual(false);
      expect(newState.emailSummaries[1].isRead).toEqual(true);
    });
  });

  describe("add email action", () => {
    it("should update email 0 count state", () => {
      const emailAction = {
        emailType: EMAIL_TYPE.reply,
        emailTo: "Sara",
        emailCc: "Luke",
        emailBody: "Hi Sarah!",
        reasonsForAction: "I wanted to say hi."
      };
      const addAction = addEmail(0, emailAction);
      const newState = emibInbox(stubbedInitialState, addAction);
      expect(newState.emailSummaries[0].emailCount).toEqual(1);
      expect(newState.emailSummaries[0].taskCount).toEqual(0);
      expect(newState.emailSummaries[1].emailCount).toEqual(0);
    });

    it("should add an email action to the action list", () => {
      const emailAction = {
        emailType: EMAIL_TYPE.reply,
        emailTo: "Sara",
        emailCc: "Luke",
        emailBody: "Hi Sarah!",
        reasonsForAction: "I wanted to say hi."
      };
      const addAction = addEmail(0, emailAction);
      const newState = emibInbox(stubbedInitialState, addAction);
      expect(newState.emailActions[0]).toEqual([{ ...emailAction, actionType: ACTION_TYPE.email }]);
    });
  });

  describe("update email action works as expected", () => {
    it("should update an email action in the action list", () => {
      const emailAction = {
        emailType: EMAIL_TYPE.reply,
        emailTo: "Sara",
        emailCc: "Luke",
        emailBody: "Hi Sarah!",
        reasonsForAction: "I wanted to say hi."
      };
      const emailActionUpdate = {
        emailType: EMAIL_TYPE.replyAll,
        emailTo: "Sara 2",
        emailCc: "Luke 2",
        emailBody: "Hi Sarah! 2",
        reasonsForAction: "I wanted to say hi. 2"
      };
      const addAction = addEmail(0, emailAction);
      const newState1 = emibInbox(stubbedInitialState, addAction);
      expect(newState1.emailActions[0]).toEqual([
        { ...emailAction, actionType: ACTION_TYPE.email }
      ]);
      const asyncAction = setResponseDbId(0, 0, 55);
      const asyncState = emibInbox(newState1, asyncAction);
      expect(asyncState.emailActions[0][0]["actionTrueId"]).toEqual(55);
      const updateAction = updateEmail(0, 0, emailActionUpdate);
      const newState2 = emibInbox(asyncState, updateAction);
      expect(newState2.emailActions[0]).toEqual([
        { ...emailActionUpdate, actionType: ACTION_TYPE.email, actionTrueId: 55 }
      ]);
    });

    it("should update 3 email actions one by one then delete them all one by one", () => {
      const email1 = {
        emailType: EMAIL_TYPE.reply,
        emailTo: "To 1",
        emailCc: "CC 1",
        emailBody: "Body 1",
        reasonsForAction: "Reason 1"
      };
      const email2 = {
        emailType: EMAIL_TYPE.reply,
        emailTo: "To 2",
        emailCc: "CC 2",
        emailBody: "Body 2",
        reasonsForAction: "Reason 2"
      };
      const email3 = {
        emailType: EMAIL_TYPE.reply,
        emailTo: "To 3",
        emailCc: "CC 3",
        emailBody: "Body 3",
        reasonsForAction: "Reason 3"
      };
      const email1Update = {
        emailType: EMAIL_TYPE.replyAll,
        emailTo: "To 1 Update",
        emailCc: "CC 1 Update",
        emailBody: "Body 1 Update",
        reasonsForAction: "Reason 1 Update"
      };
      const email2Update = {
        emailType: EMAIL_TYPE.forward,
        emailTo: "To 2 Update",
        emailCc: "CC 2 Update",
        emailBody: "Body 2 Update",
        reasonsForAction: "Reason 2 Update"
      };
      const email3Update = {
        emailType: EMAIL_TYPE.replyAll,
        emailTo: "To 3 Update",
        emailCc: "CC 3 Update",
        emailBody: "Body 3 Update",
        reasonsForAction: "Reason 3 Update"
      };
      // Add first email
      const addAction1 = addEmail(0, email1);
      const addState1 = emibInbox(stubbedInitialState, addAction1);
      expect(addState1.emailActions[0]).toEqual([{ ...email1, actionType: ACTION_TYPE.email }]);
      const asyncAction1 = setResponseDbId(0, 0, 55);
      const newState1 = emibInbox(addState1, asyncAction1);
      expect(newState1.emailActions[0][0]["actionTrueId"]).toEqual(55);
      // Add second email
      const addAction2 = addEmail(0, email2);
      const addState2 = emibInbox(newState1, addAction2);
      expect(addState2.emailActions[0]).toEqual([
        { ...email1, actionType: ACTION_TYPE.email, actionTrueId: 55 },
        { ...email2, actionType: ACTION_TYPE.email }
      ]);
      const asyncAction2 = setResponseDbId(0, 1, 56);
      const newState2 = emibInbox(addState2, asyncAction2);
      expect(newState2.emailActions[0][1]["actionTrueId"]).toEqual(56);
      // Add third email
      const addAction3 = addEmail(0, email3);
      const addState3 = emibInbox(newState2, addAction3);
      expect(addState3.emailActions[0]).toEqual([
        { ...email1, actionType: ACTION_TYPE.email, actionTrueId: 55 },
        { ...email2, actionType: ACTION_TYPE.email, actionTrueId: 56 },
        { ...email3, actionType: ACTION_TYPE.email }
      ]);
      const asyncAction3 = setResponseDbId(0, 2, 57);
      const newState3 = emibInbox(addState3, asyncAction3);
      expect(newState3.emailActions[0][2]["actionTrueId"]).toEqual(57);
      //Update first email
      const updateAction1 = updateEmail(0, 0, email1Update);
      const newState4 = emibInbox(newState3, updateAction1);
      expect(newState4.emailActions[0]).toEqual([
        { ...email1Update, actionType: ACTION_TYPE.email, actionTrueId: 55 },
        { ...email2, actionType: ACTION_TYPE.email, actionTrueId: 56 },
        { ...email3, actionType: ACTION_TYPE.email, actionTrueId: 57 }
      ]);
      //Update second email
      const updateAction2 = updateEmail(0, 1, email2Update);
      const newState5 = emibInbox(newState4, updateAction2);
      expect(newState5.emailActions[0]).toEqual([
        { ...email1Update, actionType: ACTION_TYPE.email, actionTrueId: 55 },
        { ...email2Update, actionType: ACTION_TYPE.email, actionTrueId: 56 },
        { ...email3, actionType: ACTION_TYPE.email, actionTrueId: 57 }
      ]);
      //update third email
      const updateAction3 = updateEmail(0, 2, email3Update);
      const newState6 = emibInbox(newState5, updateAction3);
      expect(newState6.emailActions[0]).toEqual([
        { ...email1Update, actionType: ACTION_TYPE.email, actionTrueId: 55 },
        { ...email2Update, actionType: ACTION_TYPE.email, actionTrueId: 56 },
        { ...email3Update, actionType: ACTION_TYPE.email, actionTrueId: 57 }
      ]);
      //delete second email
      const deleteAction1 = deleteEmail(0, 1);
      const newState7 = emibInbox(newState6, deleteAction1); // HEREish
      expect(newState7.emailActions[0]).toEqual([
        { ...email1Update, actionType: ACTION_TYPE.email, actionTrueId: 55 },
        { ...email3Update, actionType: ACTION_TYPE.email, actionTrueId: 57 }
      ]);
      //delete first email
      const deleteAction2 = deleteEmail(0, 0);
      const newState8 = emibInbox(newState7, deleteAction2);
      expect(newState8.emailActions[0]).toEqual([
        { ...email3Update, actionType: ACTION_TYPE.email, actionTrueId: 57 }
      ]);
      //delete first email again (originally the third)
      const deleteAction3 = deleteEmail(0, 0);
      const newState9 = emibInbox(newState8, deleteAction3);
      expect(newState9.emailActions[0]).toEqual([]);
    });
  });

  describe("update task action works as expected", () => {
    it("should update a task action in the action list", () => {
      const taskAction = {
        task: "Here are my tasks.",
        reasonsForAction: "I wanted to say hi."
      };
      const taskActionUpdate = {
        task: "Here are my tasks. 2",
        reasonsForAction: "I wanted to say hi. 2"
      };
      const addAction = addTask(0, taskAction);
      const addState = emibInbox(stubbedInitialState, addAction);
      expect(addState.emailActions[0]).toEqual([{ ...taskAction, actionType: ACTION_TYPE.task }]);
      const asyncAction = setResponseDbId(0, 0, 55);
      const asyncState = emibInbox(addState, asyncAction);
      expect(asyncState.emailActions[0][0]["actionTrueId"]).toEqual(55);
      const updateAction = updateTask(0, 0, taskActionUpdate);
      const newState2 = emibInbox(asyncState, updateAction);
      expect(newState2.emailActions[0]).toEqual([
        { ...taskActionUpdate, actionType: ACTION_TYPE.task, actionTrueId: 55 }
      ]);
    });

    it("should update 3 task actions one by one then delete them all one by one", () => {
      const task1 = {
        task: "Task 1",
        reasonsForAction: "Reason 1"
      };
      const task2 = {
        task: "Task 2",
        reasonsForAction: "Reason 2"
      };
      const task3 = {
        task: "Task 3",
        reasonsForAction: "Reason 3"
      };
      const task1Update = {
        task: "Task 1 Update",
        reasonsForAction: "Reason 1 Update"
      };
      const task2Update = {
        task: "Task 2 Update",
        reasonsForAction: "Reason 2 Update"
      };
      const task3Update = {
        task: "Task 3 Update",
        reasonsForAction: "Reason 3 Update"
      };
      // Add first task
      const addAction1 = addTask(0, task1);
      const addState1 = emibInbox(stubbedInitialState, addAction1);
      expect(addState1.emailActions[0]).toEqual([{ ...task1, actionType: ACTION_TYPE.task }]);
      const asyncAction1 = setResponseDbId(0, 0, 55);
      const newState1 = emibInbox(addState1, asyncAction1);
      expect(newState1.emailActions[0][0]["actionTrueId"]).toEqual(55);
      // Add second task
      const addAction2 = addTask(0, task2);
      const addState2 = emibInbox(newState1, addAction2);
      expect(addState2.emailActions[0]).toEqual([
        { ...task1, actionType: ACTION_TYPE.task, actionTrueId: 55 },
        { ...task2, actionType: ACTION_TYPE.task }
      ]);
      const asyncAction2 = setResponseDbId(0, 1, 56);
      const newState2 = emibInbox(addState2, asyncAction2);
      expect(newState2.emailActions[0][1]["actionTrueId"]).toEqual(56);
      // Add third task
      const addAction3 = addTask(0, task3);
      const addState3 = emibInbox(newState2, addAction3);
      expect(addState3.emailActions[0]).toEqual([
        { ...task1, actionType: ACTION_TYPE.task, actionTrueId: 55 },
        { ...task2, actionType: ACTION_TYPE.task, actionTrueId: 56 },
        { ...task3, actionType: ACTION_TYPE.task }
      ]);
      const asyncAction3 = setResponseDbId(0, 2, 57);
      const newState3 = emibInbox(addState3, asyncAction3);
      expect(newState3.emailActions[0][2]["actionTrueId"]).toEqual(57);
      //Update first task
      const updateAction1 = updateTask(0, 0, task1Update);
      const newState4 = emibInbox(newState3, updateAction1);
      expect(newState4.emailActions[0]).toEqual([
        { ...task1Update, actionType: ACTION_TYPE.task, actionTrueId: 55 },
        { ...task2, actionType: ACTION_TYPE.task, actionTrueId: 56 },
        { ...task3, actionType: ACTION_TYPE.task, actionTrueId: 57 }
      ]);
      //Update second task
      const updateAction2 = updateTask(0, 1, task2Update);
      const newState5 = emibInbox(newState4, updateAction2);
      expect(newState5.emailActions[0]).toEqual([
        { ...task1Update, actionType: ACTION_TYPE.task, actionTrueId: 55 },
        { ...task2Update, actionType: ACTION_TYPE.task, actionTrueId: 56 },
        { ...task3, actionType: ACTION_TYPE.task, actionTrueId: 57 }
      ]);
      //update third task
      const updateAction3 = updateTask(0, 2, task3Update);
      const newState6 = emibInbox(newState5, updateAction3);
      expect(newState6.emailActions[0]).toEqual([
        { ...task1Update, actionType: ACTION_TYPE.task, actionTrueId: 55 },
        { ...task2Update, actionType: ACTION_TYPE.task, actionTrueId: 56 },
        { ...task3Update, actionType: ACTION_TYPE.task, actionTrueId: 57 }
      ]);
      //delete second task
      const deleteAction1 = deleteTask(0, 1);
      const newState7 = emibInbox(newState6, deleteAction1); // HEREish
      expect(newState7.emailActions[0]).toEqual([
        { ...task1Update, actionType: ACTION_TYPE.task, actionTrueId: 55 },
        { ...task3Update, actionType: ACTION_TYPE.task, actionTrueId: 57 }
      ]);
      //delete first task
      const deleteAction2 = deleteTask(0, 0);
      const newState8 = emibInbox(newState7, deleteAction2);
      expect(newState8.emailActions[0]).toEqual([
        { ...task3Update, actionType: ACTION_TYPE.task, actionTrueId: 57 }
      ]);
      //delete first task again (originally the third)
      const deleteAction3 = deleteTask(0, 0);
      const newState9 = emibInbox(newState8, deleteAction3);
      expect(newState9.emailActions[0]).toEqual([]);
    });
  });

  describe("add task action", () => {
    it("should update task 0 count state", () => {
      const addAction = addTask(0);
      const newState = emibInbox(stubbedInitialState, addAction);
      expect(newState.emailSummaries[0].taskCount).toEqual(1);
      expect(newState.emailSummaries[0].emailCount).toEqual(0);
      expect(newState.emailSummaries[1].taskCount).toEqual(0);
    });

    it("should add a task action to the action list", () => {
      const taskAction = {
        task: "Here are my tasks.",
        reasonsForAction: "I wanted to say hi."
      };
      const addAction = addTask(0, taskAction);
      const newState = emibInbox(stubbedInitialState, addAction);
      expect(newState.emailActions[0]).toEqual([{ ...taskAction, actionType: ACTION_TYPE.task }]);
    });

    it("should change curent email", () => {
      const changeAction1 = changeCurrentEmail(1);
      const newState1 = emibInbox(stubbedInitialState, changeAction1);
      expect(newState1.currentEmail).toEqual(1);
      const changeAction2 = changeCurrentEmail(2);
      const newState2 = emibInbox(newState1, changeAction2);
      expect(newState2.currentEmail).toEqual(2);
      const changeAction3 = changeCurrentEmail(3);
      const newState3 = emibInbox(newState2, changeAction3);
      expect(newState3.currentEmail).toEqual(3);
    });
  });

  it("should add an empty email action to the action list", () => {
    const addAction = addEmail(0, {});
    const addState1 = emibInbox(stubbedInitialState, addAction);
    expect(addState1.emailActions[0]).toEqual([{ ...{}, actionType: ACTION_TYPE.email }]);

    const asyncAction1 = setResponseDbId(0, 0, 55);
    const newState1 = emibInbox(addState1, asyncAction1);
    expect(newState1.emailActions[0][0]["actionTrueId"]).toEqual(55);

    const updateAction = updateEmail(0, 0, {});
    const newState2 = emibInbox(newState1, updateAction);
    expect(newState2.emailActions[0]).toEqual([
      { ...{}, actionType: ACTION_TYPE.email, actionTrueId: 55 }
    ]);
  });

  it("should add an empty task action to the action list", () => {
    const addAction = addTask(0, {});
    const addState1 = emibInbox(stubbedInitialState, addAction);
    expect(addState1.emailActions[0]).toEqual([{ ...{}, actionType: ACTION_TYPE.task }]);

    const asyncAction1 = setResponseDbId(0, 0, 55);
    const newState1 = emibInbox(addState1, asyncAction1);
    expect(newState1.emailActions[0][0]["actionTrueId"]).toEqual(55);

    const updateAction = updateTask(0, 0, {});
    const newState2 = emibInbox(newState1, updateAction);
    expect(newState2.emailActions[0]).toEqual([
      { ...{}, actionType: ACTION_TYPE.task, actionTrueId: 55 }
    ]);
  });

  it("should load responses into the state", () => {
    const responses = {
      test_responses: {
        "1000": {
          "300": {
            id: 50,
            candidate_response_id: 100,
            to: "100,300",
            cc: "",
            response: "Email body without any cc",
            reason: "",
            response_type: "email"
          },
          "301": {
            id: 51,
            candidate_response_id: 101,
            task: "A task flanked by 2 emails",
            reason: "Some rationale",
            response_type: "task"
          },
          "302": {
            id: 52,
            candidate_response_id: 102,
            to: "100,300",
            cc: "200",
            response: "Email body with a cc",
            reason: "With a reason",
            response_type: "email"
          }
        }
      },
      "1001": {
        "303": {
          id: 53,
          candidate_response_id: 103,
          task: "A lone task response",
          reason: "Some rationale",
          response_type: "task"
        }
      },
      "1002": {
        "304": {
          id: 54,
          candidate_response_id: 104,
          task: "A final task",
          reason: "Some other rationale",
          response_type: "task"
        },
        "305": {
          id: 55,
          candidate_response_id: 105,
          to: "150,151",
          cc: "152.153,154",
          response: "An email scheduling a meeting",
          reason: "Asking people to come to a meeting",
          response_type: "email"
        }
      }
    };
    const loadAction = loadResponses(responses);
    const loadState = emibInbox(stubbedInitialState, loadAction);
    // wait a second so that the loadResponses function can finish
    sleep(1000).then(() => {
      expect(loadState.emailSummaries[0].taskCount).toEqual(1);
      expect(loadState.emailSummaries[0].emailCount).toEqual(2);
      expect(loadState.emailSummaries[0].isRead).toEqual(true);
      expect(loadState.emailSummaries[1].taskCount).toEqual(1);
      expect(loadState.emailSummaries[1].emailCount).toEqual(0);
      expect(loadState.emailSummaries[1].isRead).toEqual(true);
      expect(loadState.emailSummaries[2].taskCount).toEqual(1);
      expect(loadState.emailSummaries[2].emailCount).toEqual(1);
      expect(loadState.emailSummaries[2].isRead).toEqual(true);

      expect(loadState.emailActions[0][0]["actionTrueId"]).toEqual(100);
      expect(loadState.emailActions[0][0]["actionType"]).toEqual("email");
      expect(loadState.emailActions[0][0]["emailBody"]).toEqual("Email body without any cc");
      expect(loadState.emailActions[0][0]["emailType"]).toEqual("reply");
      expect(loadState.emailActions[0][0]["emailTo"]).toEqual([100, 300]);
      expect(loadState.emailActions[0][0]["emailCc"]).toEqual([]);
      expect(loadState.emailActions[0][0]["reasonsForAction"]).toEqual("");

      expect(loadState.emailActions[0][1]["actionTrueId"]).toEqual(101);
      expect(loadState.emailActions[0][1]["actionType"]).toEqual("task");
      expect(loadState.emailActions[0][1]["task"]).toEqual("A task flanked by 2 emails");
      expect(loadState.emailActions[0][1]["reasonsForAction"]).toEqual("Some rationale");

      expect(loadState.emailActions[0][2]["actionTrueId"]).toEqual(102);
      expect(loadState.emailActions[0][2]["actionType"]).toEqual("email");
      expect(loadState.emailActions[0][2]["emailBody"]).toEqual("Email body with a cc");
      expect(loadState.emailActions[0][2]["emailType"]).toEqual("reply");
      expect(loadState.emailActions[0][2]["emailTo"]).toEqual([100, 300]);
      expect(loadState.emailActions[0][2]["emailCc"]).toEqual([200]);
      expect(loadState.emailActions[0][2]["reasonsForAction"]).toEqual("With a reason");

      expect(loadState.emailActions[1][0]["actionTrueId"]).toEqual(103);
      expect(loadState.emailActions[1][0]["actionType"]).toEqual("task");
      expect(loadState.emailActions[1][0]["task"]).toEqual("A lone task response");
      expect(loadState.emailActions[1][0]["reasonsForAction"]).toEqual("Some rationale");

      expect(loadState.emailActions[2][0]["actionTrueId"]).toEqual(104);
      expect(loadState.emailActions[2][0]["actionType"]).toEqual("task");
      expect(loadState.emailActions[2][0]["task"]).toEqual("A final task");
      expect(loadState.emailActions[2][0]["reasonsForAction"]).toEqual("Some other rationale");

      expect(loadState.emailActions[2][1]["actionTrueId"]).toEqual(105);
      expect(loadState.emailActions[2][1]["actionType"]).toEqual("email");
      expect(loadState.emailActions[2][1]["emailBody"]).toEqual("An email scheduling a meeting");
      expect(loadState.emailActions[2][1]["emailType"]).toEqual("reply");
      expect(loadState.emailActions[2][1]["emailTo"]).toEqual([150, 151]);
      expect(loadState.emailActions[2][1]["emailCc"]).toEqual([152.153, 154]);
      expect(loadState.emailActions[2][1]["reasonsForAction"]).toEqual(
        "Asking people to come to a meeting"
      );
      expect(loadState.assignedQuestionIds).toEqual([1000, 1001, 1002]);
      expect(loadState.isTestLoadedFromDB).toEqual(true);
    });
  });
});

const sleep = milliseconds => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};

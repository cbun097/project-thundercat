import React, { Component } from "react";
import { withRouter, Route, Switch, Redirect } from "react-router";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { PATH } from "../commons/Constants";
import EmibInstructionsPage from "./EmibInstructionsPage";
import EmibOverviewPage from "./EmibOverviewPage";
import EmibTest from "./EmibTest";
import QuitConfirmation from "../commons/QuitConfirmation";
import Confirmation from "./Confirmation";
import { setCurrentTest } from "../../modules/TestStatusRedux";
import { BANNER_STYLE } from "./constants";
import { resetInboxState } from "../../modules/EmibInboxRedux";
import { resetMetaDataState } from "../../modules/LoadTestContentRedux";
import { resetPermissionsState } from "../../modules/PermissionsRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";

class EmibRoutes extends Component {
  static propTypes = {
    testNameId: PropTypes.string,
    timeLimit: PropTypes.number,
    updateBackendTest: PropTypes.func,
    activateTest: PropTypes.func,
    startTest: PropTypes.func,
    quitTest: PropTypes.func,
    deactivateTest: PropTypes.func,
    setCurrentTest: PropTypes.func,
    timeoutTest: PropTypes.func,
    isTestActive: PropTypes.bool,
    currentPage: PropTypes.string,
    headerFooterPX: PropTypes.number,
    resetInboxState: PropTypes.func,
    resetMetaDataState: PropTypes.func,
    resetTestStatusState: PropTypes.func,
    resetPermissionsState: PropTypes.func
  };

  resetRedux = () => {
    this.props.resetMetaDataState();
    this.props.resetInboxState();
    this.props.resetTestStatusState();
    this.props.resetPermissionsState();
    this.props.resetNotepadState();
  };

  render() {
    return (
      <div>
        <Switch>
          {!this.props.isTestActive && (
            <Route
              exact
              path={`${this.props.match.url}${PATH.overview}`}
              component={() => <EmibOverviewPage {...this.props} link={this.props.match.url} />}
            />
          )}
          {!this.props.isTestActive && (
            <Route
              exact
              path={`${this.props.match.url}${PATH.instructions}`}
              component={() => (
                <EmibInstructionsPage
                  {...this.props}
                  link={this.props.match.url}
                  headerFooterPX={BANNER_STYLE}
                />
              )}
            />
          )}
          <Route
            exact
            path={`${this.props.match.url}${PATH.test}`}
            component={() =>
              this.props.isTestActive ? (
                <EmibTest {...this.props} link={this.props.match.url} />
              ) : (
                <Redirect to={`${PATH.dashboard}`} />
              )
            }
          />

          {this.props.isTestActive && <Redirect to={`${this.props.match.url}${PATH.test}`} />}
          <Route
            exact
            path={`${this.props.match.url}${PATH.submit}`}
            component={() => <Confirmation {...this.props} resetRedux={this.resetRedux} />}
          />
          <Route
            exact
            path={`${this.props.match.url}${PATH.quit}`}
            component={() => <QuitConfirmation {...this.props} resetRedux={this.resetRedux} />}
          />
          {!this.props.isTestActive && <Redirect to={`${PATH.testBase}${PATH.overview}`} />}
        </Switch>
      </div>
    );
  }
}

export { EmibRoutes as UnconnectedEmibRoutes };

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setCurrentTest,
      resetInboxState,
      resetMetaDataState,
      resetPermissionsState,
      resetNotepadState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(EmibRoutes));

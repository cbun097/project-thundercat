import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  activateTest,
  startTest,
  deactivateTest,
  setCurrentTest,
  timeoutTest,
  quitTest,
  setCurrentPage,
  resetTestStatusState
} from "../../modules/TestStatusRedux";
import { logoutAction } from "../../modules/LoginRedux";
import { updateBackendTest } from "../../modules/UpdateTestStatusRedux";
import { getTestContent } from "../../modules/LoadTestContentRedux";
import EmibRoutes from "./EmibRoutes";
import { FOOTER_HEIGHT, HEADER_HEIGHT } from "./constants";
import { PATH } from "../../App";

class Emib extends Component {
  static propTypes = {
    updateBackendTest: PropTypes.func,
    // Provided by Redux
    testNameId: PropTypes.string,
    activateTest: PropTypes.func,
    startTest: PropTypes.func,
    deactivateTest: PropTypes.func,
    setCurrentTest: PropTypes.func,
    timeoutTest: PropTypes.func,
    quitTest: PropTypes.func,
    isTestActive: PropTypes.bool,
    startTime: PropTypes.string,
    currentPage: PropTypes.string,
    setCurrentPage: PropTypes.func,
    logoutAction: PropTypes.func
  };

  state = {};

  render() {
    return (
      <EmibRoutes
        {...this.props}
        headerFooterPX={HEADER_HEIGHT + FOOTER_HEIGHT}
        submitPath={PATH.testBase + PATH.submit}
      />
    );
  }
}

export { Emib as UnconnectedEmib };

const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.testStatus.isTestActive,
    isTestStarted: state.testStatus.isTestStarted,
    startTime: state.testStatus.startTime,
    timeLimit: state.testStatus.timeLimit,
    currentPage: state.testStatus.currentPage,
    testNameId: state.testStatus.currentTestId,
    currentAssignedTestId: state.testStatus.currentAssignedTestId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      activateTest,
      startTest,
      quitTest,
      deactivateTest,
      timeoutTest,
      setCurrentPage,
      updateBackendTest,
      getTestContent,
      setCurrentTest,
      logoutAction,
      resetTestStatusState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Emib);

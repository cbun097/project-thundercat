import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { getUserInformation, isTokenStillValid } from "./modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "./components/commons/ContentContainer";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";
import ActiveCandidatesTable from "./components/eMIB/ActiveCandidatesTable";
import getNewTestAccessCode, {
  deleteTestAccessCode,
  getActiveTestAccessCodeData
} from "./modules/TestAdministrationRedux";
import { getTestMetaData } from "./modules/LoadTestContentRedux";
import { getTestName, getTestInternalName } from "./modules/TestRedux";
import { LANGUAGES } from "./modules/LocalizeRedux";
import { getTestPermissions } from "./modules/PermissionsRedux";
import Select from "react-select";
import "./css/test-administration.css";

export const styles = {
  checkInBtn: {
    textAlign: "center",
    marginTop: 80
  },
  checkOutBtn: {
    textAlign: "center",
    margin: "24px 0 24px 0"
  },
  checkOutBtnMargin: {
    margin: 0
  },
  textInput: {
    width: 250,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    border: "1px solid rgb(0, 86, 94)",
    height: 32
  },
  dropdown: {
    width: 250,
    height: 34,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    border: "1px solid rgb(0, 86, 94)"
  },
  invalidInput: {
    border: "3px solid #923534"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: 350
  },
  informationSummaryContainer: {
    textAlign: "center",
    margin: "48px 0 24px 0"
  },
  informationSummaryContent: {
    display: "inline-block",
    textAlign: "left",
    border: "1px solid #96a8b2",
    padding: "12px 75px"
  },
  tableDiv: {
    marginTop: 24
  },
  popupWidth: {
    width: "100%"
  },
  inputsTable: {
    margin: "18px 0 36px 0",
    width: "100%"
  },
  inputsTableTitles: {
    margin: "24px 24px 0 90px"
  },
  inputsTableTitlesWhenInvalid: {
    margin: "-10px 24px 0 90px"
  },
  inputsTableInputs: {
    float: "right",
    margin: "20px 90px 0 0",
    width: 250
  }
};

class TestAdministration extends Component {
  constructor(props) {
    super(props);
    this.languageFieldRef = React.createRef();
    this.testFieldRef = React.createRef();
  }

  state = {
    firstName: "",
    lastName: "",
    taUsername: "",
    showDialog: false,
    staffingProcessNbrContent: "",
    languageOptions: [],
    dropdownLanguageOptions: [],
    languageId: null,
    selectedLanguageOption: null,
    testOptions: [],
    dropdownTestOptions: [],
    selectedTestToAdministerOption: null,
    selectedTestToAdministerInternalName: null,
    checkedIn: false,
    testAccessCode: "",
    isFirstLoad: true,
    isTestLanguageValid: false,
    isTestToAdministerValid: false,
    disabledCheckInBtn: true
  };

  static propTypes = {
    // Props from Redux
    getUserInformation: PropTypes.func,
    getNewTestAccessCode: PropTypes.func,
    deleteTestAccessCode: PropTypes.func,
    getActiveTestAccessCodeData: PropTypes.func,
    isTa: PropTypes.bool,
    getTestMetaData: PropTypes.func,
    getTestName: PropTypes.func,
    getTestInternalName: PropTypes.func,
    getTestPermissions: PropTypes.func,
    isTokenStillValid: PropTypes.func
  };

  // calling getUserInformation on page load to get the first name and last name
  componentDidMount = () => {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid(localStorage.auth_token).then(bool => {
      // if the token is still valid
      if (bool) {
        this._isMounted = true;
        // should always be defined, except for unit tests
        if (typeof this.props.getUserInformation !== "undefined") {
          this.props.getUserInformation(localStorage.auth_token).then(response => {
            if (this._isMounted) {
              let token = localStorage.getItem("auth_token");
              this.setState(
                {
                  firstName: response.first_name,
                  lastName: response.last_name,
                  taUsername: response.username
                },
                () => {
                  let taUsername = this.state.taUsername;
                  let isTa = this.props.isTa;
                  // getting active test access code associated to the specified TA username
                  this.props.getActiveTestAccessCodeData(token, isTa, taUsername).then(response => {
                    // if the TA have active test access code
                    if (typeof response.length !== "undefined") {
                      // put him back on the checked in page and recover the test access code
                      this.setState({
                        checkedIn: true,
                        testAccessCode: response[0].test_access_code,
                        staffingProcessNbrContent: response[0].staffing_process_number,
                        languageId: response[0].test_session_language_id,
                        selectedTestToAdministerInternalName: response[0].test_id
                      });
                      // update the selected test session language wording (language update)
                      this.getUpdatedSelectedLanguage();
                      // getting the selected test name
                      this.getSelectedTestName(this.state.selectedTestToAdministerInternalName);
                      // enabling the check-in button if the TA doesn't have any associated test access code
                      // preventing double check-in bug on page refresh
                    } else {
                      this.setState({ disabledCheckInBtn: false });
                      // focusing on welcome message after content load depending on checkedIn state
                      if (this.state.checkedIn) {
                        document.getElementById("ta-welcome-message-div-checked-in").focus();
                      } else {
                        document.getElementById("ta-welcome-message-div-checked-out").focus();
                      }
                    }
                  });
                }
              );
              // getting user' test permissions
              this.props
                .getTestPermissions(token, response.username)
                .then(testPermissionsResponse => {
                  // getting test names based on user' test permissions
                  this.getTestNames(testPermissionsResponse);
                });
              // getting language options
              this.getLanguageOptions();
            }
          });
        }
      }
    });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid(localStorage.auth_token).then(bool => {
      // if the token is still valid
      if (bool) {
        // if CAT language toggle button has been selected
        if (prevProps.currentLanguage !== this.props.currentLanguage) {
          // getting user' test permissions
          this.props
            .getTestPermissions(localStorage.getItem("auth_token"), this.state.taUsername)
            .then(testPermissionsResponse => {
              // getting test names based on user' test permissions
              this.getTestNames(testPermissionsResponse);
            });
          // getting language options
          this.getLanguageOptions();
          // update the selected test session language wording (language update)
          this.getUpdatedSelectedLanguage();
          // getting the selected test name
          this.getSelectedTestName(this.state.selectedTestToAdministerInternalName);
        }
        // if checkedIn state is updating
        if (prevState.checkedIn !== this.state.checkedIn) {
          // focusing on welcome message after content load depending on checkedIn state
          if (this.state.checkedIn) {
            document.getElementById("ta-welcome-message-div-checked-in").focus();
          } else {
            document.getElementById("ta-welcome-message-div-checked-out").focus();
          }
        }
      }
    });
  }

  getTestNames = testPermissions => {
    let token = localStorage.getItem("auth_token");
    let testOptions = [];
    // populating test options array based on user test permissions and selected language
    for (let i = 0; i < testPermissions.length; i++) {
      // getting test name based on the selected language
      this.props.getTestName(token, testPermissions[i].test_id).then(testNameResponse => {
        // if selected language is English
        if (this.props.currentLanguage === LANGUAGES.english) {
          if (testNameResponse[0].language_id === 1) {
            testOptions.splice(i, 0, testNameResponse[0].text_detail);
          } else {
            testOptions.splice(i, 0, testNameResponse[1].text_detail);
          }
          // if selected language is French
        } else {
          if (testNameResponse[0].language_id === 2) {
            testOptions.splice(i, 0, testNameResponse[0].text_detail);
          } else {
            testOptions.splice(i, 0, testNameResponse[1].text_detail);
          }
        }
        this.setState({ testOptions: testOptions });
      });
    }
  };

  // getting language options (depends on the selected language)
  getLanguageOptions = () => {
    let languageOptions = [];
    languageOptions.push(LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option1);
    languageOptions.push(LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option2);
    this.setState({ languageOptions: languageOptions });
  };

  // populating test language & test to administer dropdowns based on available languages (English or French) and based on
  // the options acquired from user' test permissions
  populateDropdowns = () => {
    // language options dropdown:
    let languageOptions = this.state.languageOptions;
    let languageOptionsArray = [];
    // sorting the options
    languageOptions.sort();
    // populating the options based on languageOptions state
    languageOptions.forEach(option => {
      languageOptionsArray.push({ value: option, label: option });
    });

    // test options dropdown:
    let testOptions = this.state.testOptions;
    let testOptionsArray = [];
    // sorting the options
    testOptions.sort();
    // populating the options based on testOptions state
    testOptions.forEach(option => {
      testOptionsArray.push({ value: option, label: option });
    });

    // saving results into states (used for <Select> options)
    this.setState({
      dropdownLanguageOptions: languageOptionsArray,
      dropdownTestOptions: testOptionsArray
    });
  };

  // getting selected test name depending on the language selected
  getSelectedTestName = testInternalName => {
    this.props.getTestMetaData(testInternalName).then(response => {
      if (this.props.currentLanguage === LANGUAGES.english) {
        this.setState({
          selectedTestToAdministerOption: response.test_en_name
        });
      } else {
        this.setState({
          selectedTestToAdministerOption: response.test_fr_name
        });
      }
    });
  };

  // getting the updated selected test session language (language updates)
  getUpdatedSelectedLanguage = () => {
    // if language is English
    if (this.state.languageId === 1) {
      this.setState({
        selectedLanguageOption:
          LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option1
      });
      // if language is French
    } else if (this.state.languageId === 2) {
      this.setState({
        selectedLanguageOption:
          LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option2
      });
      // should never happen
    } else {
      this.setState({ selectedLanguageOption: null });
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ ...this.state, showDialog: false, isFirstLoad: true });
    // reset all fields states
    this.resetAllFieldsStates();
  };

  // getting staffing process number field's content
  getStaffingProcessNbrContent = event => {
    const content = event.target.value;
    this.setState({ staffingProcessNbrContent: content });
  };

  // getting test session language selected field's content from dropdown
  getSelectedLanguage = selectedOption => {
    // if selectedLanguage = "English"
    if (
      selectedOption.value ===
      LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option1
    ) {
      this.setState({
        languageId: 1,
        selectedLanguageOption:
          LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option1
      });
      // else if selectedLanguage = "French"
    } else if (
      selectedOption.value ===
      LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option2
    ) {
      this.setState({
        languageId: 2,
        selectedLanguageOption:
          LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown.option2
      });
      // should never happen
    } else {
      this.setState({
        languageId: null,
        selectedLanguageOption: null
      });
    }
  };

  // getting test to administer selected field's content from dropdown
  getSelectedTestToAdminister = selectedOption => {
    // getting test internal name based on the selected option
    this.props
      .getTestInternalName(localStorage.getItem("auth_token"), selectedOption.value)
      .then(response => {
        // getting selected test name
        this.getSelectedTestName(response.test_name);
        // saving the internal name in selectedTestToAdministerInternalName state
        this.setState({ selectedTestToAdministerInternalName: response.test_name });
      });
  };

  checkOut = () => {
    // reseting all fiels states + checkedIn state
    this.resetAllFieldsStates();
    // deleting the test access code
    let token = localStorage.getItem("auth_token");
    let isTa = this.props.isTa;
    let testAccessCode = this.state.testAccessCode;
    let taUsername = this.state.taUsername;
    this.props.deleteTestAccessCode(token, isTa, testAccessCode, taUsername).then(response => {
      if (response[0] === "succeed") {
        return;
      } else {
        throw new Error("Something went wrong. No test access code has been found!");
      }
    });
    // enabling the check-in button
    this.setState({ disabledCheckInBtn: false });
  };

  // reseting all fields
  resetAllFieldsStates = () => {
    this.setState({
      staffingProcessNbrContent: "",
      checkedIn: false,
      languageId: null,
      selectedLanguageOption: null,
      selectedTestToAdministerInternalName: null,
      selectedTestToAdministerOption: null
    });
  };

  // validate test session language field
  validateTestSessionLanguage = () => {
    // valid if the selected value is not null or undefined
    if (this.state.languageId !== null && typeof this.state.languageId !== "undefined") {
      return true;
    } else {
      return false;
    }
  };

  // validate test to administer field
  validateTestToAdminister = () => {
    // valid if the selected value is not null or undefined
    if (
      this.state.selectedTestToAdministerOption !== null &&
      typeof this.state.selectedTestToAdministerOption !== "undefined"
    ) {
      return true;
    } else {
      return false;
    }
  };

  // execute fields validation
  validateForm = () => {
    // get states of the fields that need validation
    const testSessionLanguageState = this.validateTestSessionLanguage();
    const testToAdministerState = this.validateTestToAdminister();
    // update validation states of the required fields
    this.setState(
      {
        isTestLanguageValid: testSessionLanguageState,
        isTestToAdministerValid: testToAdministerState,
        isFirstLoad: false
      },
      () => {
        // once the states above are updated (async), check if the form is valid
        if (this.isFormValid()) {
          this.setState({ checkedIn: true, showDialog: false });
          this.generateNewTestAccessCode();
        } else {
          // if the form is not valid, focus on the highest invalid element
          // if language field is not valid
          if (!this.state.isTestLanguageValid) {
            // focus on language selection field
            this.languageFieldRef.current.focus();
            // if test field is not valid
          } else if (!this.state.isTestToAdministerValid) {
            // focus on test selection field
            this.testFieldRef.current.focus();
          }
        }
      }
    );
  };

  // checks if all required fields are valid (the mandatory fields are test session language and test to administer)
  isFormValid = () => {
    return this.state.isTestLanguageValid && this.state.isTestToAdministerValid;
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isTestLanguageValid) {
      document.getElementById("selected-language-field").focus();
    } else if (!this.state.isTestToAdministerValid) {
      document.getElementById("selected-test-field").focus();
    }
  };

  // getting a new random test access code
  // saving data in database (test access code, staffing process number, test session language, ta username, test internal name)
  generateNewTestAccessCode = () => {
    let token = localStorage.getItem("auth_token");
    let isTa = this.props.isTa;
    let taUsername = this.state.taUsername;
    let staffingProcessNumber = this.state.staffingProcessNbrContent;
    let languageId = this.state.languageId;
    let test = this.state.selectedTestToAdministerInternalName;
    this.props
      .getNewTestAccessCode(token, isTa, taUsername, staffingProcessNumber, languageId, test)
      .then(response => {
        this.setState({ testAccessCode: response });
      });
  };

  render() {
    const {
      staffingProcessNbrContent,
      selectedLanguageOption,
      selectedTestToAdministerOption,
      testAccessCode,
      disabledCheckInBtn
    } = this.state;
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.testAdministration}</title>
        </Helmet>
        <ContentContainer>
          {!this.state.checkedIn && (
            <div>
              <div
                id="ta-welcome-message-div-checked-out"
                tabIndex={0}
                aria-labelledby="ta-welcome-message"
              >
                <h1 id="ta-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.title,
                    this.state.firstName,
                    this.state.lastName
                  )}
                </h1>
                <p>{LOCALIZE.testAdministration.checkedOutDescriptionPart1}</p>
                <p>{LOCALIZE.testAdministration.checkedOutDescriptionPart2}</p>
              </div>
              <div style={styles.checkInBtn}>
                <button
                  id="unit-test-check-in-button"
                  type="button"
                  className="btn btn-primary btn-wide"
                  onClick={this.openDialog}
                  disabled={disabledCheckInBtn}
                >
                  {LOCALIZE.testAdministration.checkInButton}
                </button>
              </div>
            </div>
          )}
          {this.state.checkedIn && (
            <div>
              <section aria-labelledby="ta-welcome-message-div-checked-in">
                <div id="ta-welcome-message-div-checked-in" tabIndex={0}>
                  <h1 id="ta-welcome-message" className="green-divider">
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.title,
                      this.state.firstName,
                      this.state.lastName
                    )}
                  </h1>
                  <p>{LOCALIZE.testAdministration.checkedInDescriptionPart1}</p>
                  <div style={styles.informationSummaryContainer}>
                    <div style={styles.informationSummaryContent}>
                      <p>
                        <span>{LOCALIZE.testAdministration.popupBox.form.firstElement.title} </span>
                        <span className="font-weight-bold">{staffingProcessNbrContent}</span>
                      </p>
                      <p>
                        <span>{LOCALIZE.testAdministration.popupBox.form.secondElement.title}</span>
                        <span className="font-weight-bold"> {selectedLanguageOption}</span>
                      </p>
                      <p>
                        <span>{LOCALIZE.testAdministration.popupBox.form.thirdElement.title}</span>
                        <span className="font-weight-bold"> {selectedTestToAdministerOption}</span>
                      </p>
                    </div>
                  </div>
                  <p>{LOCALIZE.testAdministration.checkedInDescriptionPart2}</p>
                  <div>
                    <div style={styles.checkOutBtn}>
                      <button
                        id="unit-test-checkout-button"
                        type="button"
                        className="btn btn-secondary btn-wide"
                        style={styles.checkOutBtnMargin}
                        onClick={this.checkOut}
                      >
                        {LOCALIZE.testAdministration.checkOutButton}
                      </button>
                    </div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.checkedInDescriptionPart3,
                        <span className="font-weight-bold">{testAccessCode}</span>
                      )}
                      <br />
                      {LOCALIZE.testAdministration.checkedInDescriptionPart4}
                    </p>
                  </div>
                </div>
              </section>
              <section aria-labelledby="active-candidates-table">
                <div id="active-candidates-table" style={styles.tableDiv}>
                  <h2 id="ta-welcome-message" className="green-divider">
                    {LOCALIZE.testAdministration.activeCandidatesTable.title}
                  </h2>
                  <ActiveCandidatesTable />
                </div>
              </section>
            </div>
          )}
        </ContentContainer>
        <PopupBox
          show={this.state.showDialog}
          handleClose={() => {}}
          isBackdropStatic={true}
          title={LOCALIZE.testAdministration.popupBox.title}
          onPopupOpen={this.populateDropdowns}
          overflowVisible={true}
          description={
            <div style={styles.popupWidth}>
              <div>
                <p>{LOCALIZE.testAdministration.popupBox.description}</p>
              </div>
              <table style={styles.inputsTable}>
                <tbody>
                  <tr>
                    <th>
                      <div style={styles.inputsTableTitles}>
                        <label htmlFor="staffing-process-nbr-field">
                          {LOCALIZE.testAdministration.popupBox.form.firstElement.title}
                        </label>
                      </div>
                    </th>
                    <th>
                      <div style={styles.inputsTableInputs}>
                        <input
                          aria-required={"false"}
                          id="staffing-process-nbr-field"
                          type="text"
                          style={styles.textInput}
                          value={staffingProcessNbrContent}
                          onChange={this.getStaffingProcessNbrContent}
                        />
                      </div>
                    </th>
                  </tr>
                  <tr>
                    <th>
                      <div
                        style={
                          this.state.isFirstLoad || this.state.isTestLanguageValid
                            ? styles.inputsTableTitles
                            : styles.inputsTableTitlesWhenInvalid
                        }
                      >
                        <label id="test-session-language-field">
                          {LOCALIZE.testAdministration.popupBox.form.secondElement.title}
                        </label>
                      </div>
                    </th>
                    <th>
                      <div style={styles.inputsTableInputs}>
                        <div>
                          <Select
                            ref={this.languageFieldRef}
                            className={
                              !this.state.isTestLanguageValid && !this.state.isFirstLoad
                                ? "invalid-input"
                                : ""
                            }
                            id="selected-language-field"
                            name="test-session-language"
                            aria-labelledby={
                              "test-session-language-field invalid-test-session-language-error"
                            }
                            aria-required={"true"}
                            aria-invalid={
                              !this.state.isTestLanguageValid && !this.state.isFirstLoad
                            }
                            placeholder={
                              LOCALIZE.testAdministration.popupBox.form.secondElement.dropdown
                                .placeholder
                            }
                            options={this.state.dropdownLanguageOptions}
                            onChange={this.getSelectedLanguage}
                            clearable={false}
                            style={styles.dropdown}
                          ></Select>
                        </div>
                        <div>
                          {!this.state.isTestLanguageValid && !this.state.isFirstLoad && (
                            <label
                              id="invalid-test-session-language-error"
                              style={styles.errorMessage}
                            >
                              {LOCALIZE.testAdministration.popupBox.form.secondElement.invalidError}
                            </label>
                          )}
                        </div>
                      </div>
                    </th>
                  </tr>
                  <tr>
                    <th>
                      <div
                        style={
                          this.state.isFirstLoad || this.state.isTestToAdministerValid
                            ? styles.inputsTableTitles
                            : styles.inputsTableTitlesWhenInvalid
                        }
                      >
                        <label id="test-to-administer-field">
                          {LOCALIZE.testAdministration.popupBox.form.thirdElement.title}
                        </label>
                      </div>
                    </th>
                    <th>
                      <div style={styles.inputsTableInputs}>
                        <div>
                          <Select
                            ref={this.testFieldRef}
                            className={
                              !this.state.isTestToAdministerValid && !this.state.isFirstLoad
                                ? "invalid-input"
                                : ""
                            }
                            id="selected-test-field"
                            name="test-to-administer"
                            aria-labelledby={
                              "test-to-administer-field invalid-test-to-administer-error"
                            }
                            aria-required={"true"}
                            aria-invalid={
                              !this.state.isTestToAdministerValid && !this.state.isFirstLoad
                            }
                            maxMenuHeight={200}
                            placeholder={
                              LOCALIZE.testAdministration.popupBox.form.thirdElement.dropdown
                                .placeholder
                            }
                            options={this.state.dropdownTestOptions}
                            onChange={this.getSelectedTestToAdminister}
                            clearable={false}
                            style={styles.dropdown}
                          ></Select>
                        </div>
                        <div>
                          <div>
                            {!this.state.isTestToAdministerValid && !this.state.isFirstLoad && (
                              <label
                                id="invalid-test-to-administer-error"
                                style={styles.errorMessage}
                              >
                                {
                                  LOCALIZE.testAdministration.popupBox.form.thirdElement
                                    .invalidError
                                }
                              </label>
                            )}
                          </div>
                        </div>
                      </div>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDialog}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.testAdministration.checkInButton}
          rightButtonAction={this.validateForm}
        />
      </div>
    );
  }
}

export { TestAdministration as UnconnectedTestAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTa: state.userPermissions.isTa
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUserInformation,
      getNewTestAccessCode,
      deleteTestAccessCode,
      getActiveTestAccessCodeData,
      getTestMetaData,
      getTestName,
      getTestInternalName,
      getTestPermissions,
      isTokenStillValid
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestAdministration);

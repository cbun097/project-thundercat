import React from "react";
import { mount } from "enzyme";
import NextPreviousButtonNav from "../../../components/commons/NextPreviousButtonNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faChevronLeft } from "@fortawesome/free-solid-svg-icons";

it("renders next button", () => {
  const wrapper = mount(
    <NextPreviousButtonNav
      showPrevious={false}
      showNext={true}
      onChangeToPrevious={() => {}}
      onChangeToNext={() => {}}
    />
  );
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronRight} />)).toEqual(true);
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronLeft} />)).toEqual(false);
});

it("renders previous button", () => {
  const wrapper = mount(
    <NextPreviousButtonNav
      showPrevious={true}
      showNext={false}
      onChangeToPrevious={() => {}}
      onChangeToNext={() => {}}
    />
  );
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronLeft} />)).toEqual(true);
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronRight} />)).toEqual(false);
});

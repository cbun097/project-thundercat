import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/personal-info.css";
import Select from "react-select";

const styles = {
  mainContent: {
    padding: "12px 0 0 24px"
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  label: {
    padding: "2px 0 0 4px"
  },
  dateOfBirthContainer: {
    display: "flex"
  },
  yearField: {
    width: 95,
    marginRight: 36
  },
  monthAndDayField: {
    width: 75,
    marginRight: 36
  },
  titleFieldLabel: {
    margin: 0
  },
  priContainer: {
    display: "inline-block"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class PersonalInfo extends Component {
  static propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    primaryEmail: PropTypes.string.isRequired,
    secondaryEmail: PropTypes.string,
    dateOfBirth: PropTypes.string.isRequired,
    priOrMilitaryNbr: PropTypes.string
  };

  state = {
    firstNameContent: "",
    lastNameContent: "",
    primaryEmailContent: "",
    secondaryEmailContent: "",
    dateOfBirthYearOptions: [],
    dateOfBirthYearSelectedValue: null,
    dateOfBirthMonthOptions: [],
    dateOfBirthMonthSelectedValue: null,
    dateOfBirthDayOptions: [],
    dateOfBirthDaySelectedValue: null,
    priOrMilitaryNbrContent: ""
  };

  componentDidMount = () => {
    this.populateDateOfBirthYearOptions();
    this.populateDateOfBirthMonthOptions();
    this.populateDateOfBirthDayOptions();
  };

  componentDidUpdate = prevProps => {
    // update first name field based on the prop value
    if (prevProps.firstName !== this.props.firstName) {
      this.setState({ firstNameContent: this.props.firstName });
    }
    // update last name field based on the prop value
    if (prevProps.lastName !== this.props.lastName) {
      this.setState({ lastNameContent: this.props.lastName });
    }
    // update primary email field based on the prop value
    if (prevProps.primaryEmail !== this.props.primaryEmail) {
      this.setState({ primaryEmailContent: this.props.primaryEmail });
    }
    // update secondary email field based on the prop value
    if (prevProps.secondaryEmail !== this.props.secondaryEmail) {
      this.setState({ secondaryEmailContent: this.props.secondaryEmail });
    }
    // update date of birth selected value field based on the prop value
    if (prevProps.dateOfBirth !== this.props.dateOfBirth) {
      this.setState({
        // year
        dateOfBirthYearSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("/")[2]),
          label: `${this.props.dateOfBirth.split("/")[2]}`
        },
        // month
        dateOfBirthMonthSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("/")[1]),
          label: `${this.props.dateOfBirth.split("/")[1]}`
        },
        // day
        dateOfBirthDaySelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("/")[0]),
          label: `${this.props.dateOfBirth.split("/")[0]}`
        }
      });
      // update pri or military number field based on the prop value
      if (prevProps.priOrMilitaryNbr !== this.props.priOrMilitaryNbr) {
        this.setState({ priOrMilitaryNbrContent: this.props.priOrMilitaryNbr });
      }
    }
  };

  // populate year (DOB) from 1900 to current year
  populateDateOfBirthYearOptions = () => {
    let yearOptionsArray = [];
    // loop from 1900 to current year
    for (let i = 1900; i <= new Date().getFullYear(); i++) {
      // push each value in yearOptionsArray
      yearOptionsArray.push({ value: i, label: `${i}` });
    }
    // sorting (descending order)
    yearOptionsArray.reverse();
    // saving result in state
    this.setState({ dateOfBirthYearOptions: yearOptionsArray });
  };

  // populate month (DOB)
  populateDateOfBirthMonthOptions = () => {
    let monthOptionsArray = [];
    // loop from 1 to 12
    for (let i = 1; i <= 12; i++) {
      // push each value in monthOptionsArray
      monthOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    this.setState({ dateOfBirthMonthOptions: monthOptionsArray });
  };

  // populate day (DOB)
  populateDateOfBirthDayOptions = () => {
    let dayOptionsArray = [];
    // loop from 1 to 31
    for (let i = 1; i <= 31; i++) {
      // push each value in dayOptionsArray
      dayOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    this.setState({ dateOfBirthDayOptions: dayOptionsArray });
  };

  // update first name value
  updateFirstNameValue = event => {
    const firstNameContent = event.target.value;
    this.setState({
      firstNameContent: firstNameContent
    });
  };

  // update last name value
  updateLastNameValue = event => {
    const lastNameContent = event.target.value;
    this.setState({
      lastNameContent: lastNameContent
    });
  };

  // update primary email value
  updatePrimaryEmailValue = event => {
    const primaryEmailContent = event.target.value;
    this.setState({
      primaryEmailContent: primaryEmailContent
    });
  };

  // update secondary email value
  updateSecondaryEmailValue = event => {
    const secondaryEmailContent = event.target.value;
    this.setState({
      secondaryEmailContent: secondaryEmailContent
    });
  };

  // get selected year (DOB)
  getSelectedDateOfBirthYear = selectedOption => {
    this.setState({ dateOfBirthYearSelectedValue: selectedOption });
  };

  // get selected month (DOB)
  getSelectedDateOfBirthMonth = selectedOption => {
    this.setState({ dateOfBirthMonthSelectedValue: selectedOption });
  };

  // get selected day (DOB)
  getSelectedDateOfBirthDay = selectedOption => {
    this.setState({ dateOfBirthDaySelectedValue: selectedOption });
  };

  // update pri or military number value
  updatePriOrMilitaryNbrValue = event => {
    const priOrMilitaryNbrContent = event.target.value;
    this.setState({
      priOrMilitaryNbrContent: priOrMilitaryNbrContent
    });
  };

  render() {
    const {
      firstNameContent,
      lastNameContent,
      primaryEmailContent,
      secondaryEmailContent,
      dateOfBirthYearOptions,
      //TODO (fnormand): uncomment all code related to "dateOfBirthYearSelectedValue" when the full date of birth will be introduced in the registration form
      // dateOfBirthYearSelectedValue,
      dateOfBirthMonthOptions,
      dateOfBirthMonthSelectedValue,
      dateOfBirthDayOptions,
      dateOfBirthDaySelectedValue,
      priOrMilitaryNbrContent
    } = this.state;
    return (
      <div>
        <h2>{LOCALIZE.profile.personalInfo.title}</h2>
        <div style={styles.mainContent}>
          <div id="personal-info-names" className="profile-peronal-info-grid">
            <div id="name-title" className="profile-peronal-info-first-column">
              <p>{LOCALIZE.profile.personalInfo.nameSection.title}</p>
            </div>
            <div className="profile-peronal-info-second-column">
              <input
                id="first-name-input"
                className="valid-field"
                aria-labelledby="name-title first-name"
                aria-required={true}
                style={styles.input}
                type="text"
                value={firstNameContent}
                onChange={this.updateFirstNameValue}
              ></input>
              <label id="first-name" htmlFor="first-name-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.nameSection.firstName}
              </label>
            </div>
            <div className="profile-peronal-info-third-column">
              <input
                id="last-name-input"
                className="valid-field"
                aria-labelledby="name-title last-name"
                aria-required={true}
                style={styles.input}
                type="text"
                value={lastNameContent}
                onChange={this.updateLastNameValue}
              ></input>
              <label id="last-name" htmlFor="last-name-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.nameSection.lastName}
              </label>
            </div>
          </div>
          <div id="personal-info-emails" className="profile-peronal-info-grid">
            <div id="email-title" className="profile-peronal-info-first-column">
              <p>{LOCALIZE.profile.personalInfo.emailAddressesSection.title}</p>
            </div>
            <div className="profile-peronal-info-second-column">
              <input
                id="primary-email-input"
                className="valid-field"
                aria-labelledby="email-title primary-email"
                aria-required={true}
                style={styles.input}
                type="text"
                value={primaryEmailContent}
                onChange={this.updatePrimaryEmailValue}
              ></input>
              <label id="primary-email" htmlFor="primary-email-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.emailAddressesSection.primary}
              </label>
            </div>
            <div className="profile-peronal-info-third-column">
              <input
                id="secondary-email-input"
                className="valid-field"
                aria-labelledby="email-title secondary-email"
                aria-required={false}
                style={styles.input}
                type="text"
                value={secondaryEmailContent}
                onChange={this.updateSecondaryEmailValue}
              ></input>
              <label id="secondary-email" htmlFor="secondary-email-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.emailAddressesSection.secondary}
              </label>
              <span>{LOCALIZE.profile.personalInfo.optionalField}</span>
            </div>
          </div>
          <div
            id="personal-info-date-of-birth"
            className="profile-peronal-info-grid"
            style={styles.dateOfBirthContainer}
          >
            <div id="dob-title" className="profile-peronal-info-first-column">
              <p>
                <label id="date-of-birth" style={styles.titleFieldLabel}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.title}
                </label>
              </p>
            </div>
            <div style={styles.monthAndDayField}>
              <Select
                id="selected-day-field"
                className="valid-field"
                name="date-of-birth-day"
                aria-labelledby={
                  "dob-title day-field-selected current-day-value-intro day-field-value"
                }
                aria-required={true}
                options={dateOfBirthDayOptions}
                onChange={this.getSelectedDateOfBirthDay}
                clearable={false}
                value={dateOfBirthDaySelectedValue}
              ></Select>
              <div>
                <label id="day-field-label" style={styles.label}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.dayField}
                </label>
                <label id="day-field-selected" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.dayFieldSelected}
                </label>
                <label id="current-day-value-intro" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.currentValue}
                </label>
                <label id="day-field-value" style={styles.hiddenText}>
                  {this.state.dateOfBirthDaySelectedValue !== null
                    ? this.state.dateOfBirthDaySelectedValue.label
                    : ""}
                </label>
              </div>
            </div>
            <div style={styles.monthAndDayField}>
              <Select
                id="selected-month-field"
                className="valid-field"
                name="date-of-birth-month"
                aria-labelledby={
                  "dob-title month-field-selected current-month-value-intro month-field-value"
                }
                aria-required={true}
                options={dateOfBirthMonthOptions}
                onChange={this.getSelectedDateOfBirthMonth}
                clearable={false}
                value={dateOfBirthMonthSelectedValue}
              ></Select>
              <div>
                <label id="month-field-label" style={styles.label}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.monthField}
                </label>
                <label id="month-field-selected" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.monthFieldSelected}
                </label>
                <label id="current-month-value-intro" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.currentValue}
                </label>
                <label id="month-field-value" style={styles.hiddenText}>
                  {this.state.dateOfBirthMonthSelectedValue !== null
                    ? this.state.dateOfBirthMonthSelectedValue.label
                    : ""}
                </label>
              </div>
            </div>
            <div style={styles.yearField}>
              <Select
                id="selected-year-field"
                className="valid-field"
                name="date-of-birth-year"
                aria-labelledby={
                  "dob-title year-field-selected current-year-value-intro year-field-value"
                }
                aria-required={true}
                options={dateOfBirthYearOptions}
                onChange={this.getSelectedDateOfBirthYear}
                clearable={false}
                // value={dateOfBirthYearSelectedValue}
              ></Select>
              <div>
                <label id="year-field-label" style={styles.label}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.yearField}
                </label>
                <label id="year-field-selected" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.yearFieldSelected}
                </label>
                <label id="current-year-value-intro" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.currentValue}
                </label>
                <label id="year-field-value" style={styles.hiddenText}>
                  {this.state.dateOfBirthYearSelectedValue !== null
                    ? this.state.dateOfBirthYearSelectedValue.label
                    : ""}
                </label>
              </div>
            </div>
          </div>
          <div
            id="personal-info-pri-or-military-nbr"
            className="profile-peronal-info-grid"
            style={styles.priContainer}
          >
            <div
              id="pri-or-military-nbr-title"
              className="profile-peronal-info-first-column"
              style={styles.floatLeft}
            >
              <label htmlFor="pri-or-military-number-input">
                <p>
                  {LOCALIZE.profile.personalInfo.priOrMilitaryNbr}
                  <br />
                  <span>{LOCALIZE.profile.personalInfo.optionalField}</span>
                </p>
              </label>
            </div>
            <div style={styles.floatRight}>
              <input
                id="pri-or-military-number-input"
                className="valid-field"
                aria-labelledby="pri-or-military-nbr-title"
                aria-required={true}
                style={styles.input}
                type="text"
                value={priOrMilitaryNbrContent}
                onChange={this.updatePriOrMilitaryNbrValue}
              ></input>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export { PersonalInfo as unconnectedPersonalInfo };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonalInfo);

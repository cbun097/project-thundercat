import { PATH } from "../components/commons/Constants";

// Action Types
// Permissions
export const SET_PERMISSIONS = "permissions/SET_PERMISSIONS";
// Home Page
export const SET_CURRENT_HOME_PAGE = "permissions/SET_CURRENT_HOME_PAGE";
const RESET_STATE = "permissions/SET_IS_TEST_ADMINISTRATOR";

// update permissions state
const updatePermissionsState = permissions => ({
  type: SET_PERMISSIONS,
  permissions
});
// update current home page state
const updateCurrentHomePageState = currentHomePage => ({
  type: SET_CURRENT_HOME_PAGE,
  currentHomePage
});
const resetPermissionsState = () => ({
  type: RESET_STATE
});

// get user permissions based on the username
function getUserPermissions(token, username) {
  return async function() {
    let response = await fetch(`/api/get-user-permissions/?username=${username}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// grant test permission to user
function grantTestPermission(token, username, test) {
  return async function() {
    let response = await fetch(
      `/api/grant-test-permission/?username_id=${username}&test_id=${test}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + token,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// get test permissions for a specific user
function getTestPermissions(token, username) {
  return async function() {
    let response = await fetch(`/api/get-test-permissions/?username_id=${username}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  isSuperUser: false,
  isEtta: false,
  isPpc: false,
  isTa: false,
  currentHomePage: PATH.dashboard
};

// Reducer
const userPermissions = (state = initialState, action) => {
  switch (action.type) {
    case SET_PERMISSIONS:
      return {
        ...state,
        isSuperUser: action.permissions.isSuperUser,
        isEtta: action.permissions.isEtta,
        isPpc: action.permissions.isPpc,
        isTa: action.permissions.isTa
      };
    case SET_CURRENT_HOME_PAGE:
      return {
        ...state,
        currentHomePage: action.currentHomePage
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userPermissions;
export {
  initialState,
  getUserPermissions,
  grantTestPermission,
  getTestPermissions,
  updatePermissionsState,
  updateCurrentHomePageState,
  resetPermissionsState
};

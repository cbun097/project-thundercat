from django.db import models
from backend.custom_models.assigned_question import AssignedQuestion
from .response_type import ResponseType


# Candidate Response (data for a candidate's reponses to an assigned test)
# Stores the id, id of the assigned question,
# the response type to help look up more complex response types
# and the date modified


class CandidateResponse(models.Model):
    id = models.AutoField(primary_key=True)
    assigned_question = models.ForeignKey(AssignedQuestion, on_delete=models.DO_NOTHING)
    response_type = models.ForeignKey(ResponseType, on_delete=models.DO_NOTHING)
    date_modified = models.DateTimeField(auto_now_add=True, blank=True)

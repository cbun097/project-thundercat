import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import SideNavigation from "./SideNavigation";
import { getBackgroundInCurrentLanguage } from "../../modules/LoadTestContentRedux";
import BackgroundSection from "./BackgroundSection";
import LOCALIZE from "../../text_resources";

const styles = {
  tabContainer: {
    overflowY: "scroll",
    zIndex: 1
  },
  nav: {
    marginTop: 10,
    marginLeft: 10
  }
};

class Background extends Component {
  static propTypes = {
    testBackground: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        sectionContent: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            type: PropTypes.string.isRequired, // markdown or tree_view
            text: PropTypes.string, // markdown only
            organizational_structure_tree_child: PropTypes.array,
            team_information_tree_child: PropTypes.array
          })
        ),
        title: PropTypes.string,
        backgroundLoaded: PropTypes.bool
      })
    ),
    isMain: PropTypes.bool,
    headerFooterPX: PropTypes.number
  };

  // on background content load, focus on side navigation of background section
  componentDidUpdate = () => {
    if (this.props.backgroundLoaded) {
      document.getElementById("side-navigation-of-background").focus();
    }
  };

  // getting number of org charts for each section of type 'tree_view'
  getNumberOfOrgChartsPerSection = sections => {
    let numberOfOrgCharts = 0;
    let array = [];
    // for each sections
    for (let i = 0; i < sections.length; i++) {
      // for each section content part of the sections
      for (let j = 0; j < sections[i].section_content.length; j++) {
        // if the type is 'tree_view', meaning org chart
        if (sections[i].section_content[j].type === "tree_view") {
          // increments variable
          numberOfOrgCharts += 1;
        }
      }
      // if the result (numberOfOrgCharts) is greater than 0, push it in the array
      if (numberOfOrgCharts > 0) {
        array.push(numberOfOrgCharts);
      }
      // reset variable for next iteration
      numberOfOrgCharts = 0;
    }
    return array;
  };

  render() {
    const sections = this.props.testBackground;
    return (
      <div id="side-navigation-of-background" tabIndex={-1}>
        <SideNavigation
          headerFooterPX={this.props.headerFooterPX}
          startIndex={10}
          isMain={this.props.isMain}
          parentTabName={LOCALIZE.emibTest.background.hiddenTabNameComplementary}
          displayNextPreviousButton={true}
          tabContainerStyle={styles.tabContainer}
          navStyle={styles.nav}
          specs={sections.map(section => {
            return {
              menuString: section.title,
              body: (
                <BackgroundSection
                  content={section.section_content}
                  numberOfOrgChartsArray={this.getNumberOfOrgChartsPerSection(sections)}
                />
              )
            };
          })}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    testBackground: getBackgroundInCurrentLanguage(state)
  };
};

export default connect(
  mapStateToProps,
  null
)(Background);

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import EmibTabs from "./EmibTabs";
import LOCALIZE from "../../text_resources";
import ConfirmStartTest from "../commons/ConfirmStartTest";
import { Helmet } from "react-helmet";
import {
  getTestContent,
  updateTestBackgroundState,
  getTotalTestTime
} from "../../modules/LoadTestContentRedux";
import {
  updateEmailsEnState,
  updateEmailsFrState,
  updateEmailsState
} from "../../modules/EmibInboxRedux";
import { TEST_DEFINITION } from "../../testDefinition";
import { PATH } from "../commons/Constants";
import InstructionsFooter from "../commons/InstructionsFooter";
import { LANGUAGES } from "../commons/Translation";

class EmibInstructionsPage extends Component {
  static propTypes = {
    updateBackendTest: PropTypes.func.isRequired,
    testNameId: PropTypes.string,
    activateTest: PropTypes.func.isRequired,
    headerFooterPX: PropTypes.number,
    // Provided by Redux
    currentLanguage: PropTypes.string,
    testTimeInMinutes: PropTypes.number,
    isTestActive: PropTypes.bool.isRequired
  };

  state = {
    currentTab: "instructions",
    disabledTabs: [1, 2],
    testIsStarted: false,
    showStartTestPopup: false
  };

  /* Within eMIB Tabs functions
  loading test questions from the APIs on test start */
  handleStartTest = () => {
    const testNameId = this.props.testNameId
      ? this.props.testNameId
      : TEST_DEFINITION.emib.sampleTest;
    //set a new date for the start time. stored in local and then into redux

    //set var so button text changes when re-entering test (except sample test)
    if (testNameId !== TEST_DEFINITION.emib.sampleTest) {
      localStorage.setItem("testActive", 1);
    }

    // getting questions of the sample test from the api
    this.props.getTestContent(testNameId).then(response => {
      // Load emails.
      if (this.props.currentLanguage === LANGUAGES.english) {
        this.props.updateEmailsState(response.questions.en.email);
      } else {
        this.props.updateEmailsState(response.questions.fr.email);
      } // saving questions content in emails, emailsEN and emailsFR states
      this.props.updateEmailsEnState(response.questions.en.email);
      this.props.updateEmailsFrState(response.questions.fr.email);

      // Load background info.
      this.props.updateTestBackgroundState(response.background);

      this.props.activateTest();
      //redirect to active test
      //window.location.href = this.props.link + PATH.test;
      this.props.history.push(this.props.match.url + PATH.test);
    });
  };

  openStartTestPopup = () => {
    this.setState({ showStartTestPopup: true });
  };

  closeStartTestPopup = () => {
    this.setState({ showStartTestPopup: false });
  };

  render() {
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.eMIB}</title>
        </Helmet>

        <EmibTabs
          currentTab={this.state.currentTab}
          switchTab={() => {}}
          disabledTabsArray={this.state.disabledTabs}
          headerFooterPX={this.props.headerFooterPX}
        />

        <InstructionsFooter startTest={this.openStartTestPopup} />

        <ConfirmStartTest
          showDialog={this.state.showStartTestPopup}
          handleClose={this.closeStartTestPopup}
          startTest={this.handleStartTest}
        />
      </div>
    );
  }
}

export { EmibInstructionsPage as UnconnectedEmibInstructionsPage };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testTimeInMinutes: getTotalTestTime(state)
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestContent,
      updateTestBackgroundState,
      updateEmailsEnState,
      updateEmailsFrState,
      updateEmailsState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(EmibInstructionsPage));

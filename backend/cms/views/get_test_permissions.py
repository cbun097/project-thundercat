from django.db.models import Q
from user_management.user_management_models.user_models import User
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import is_undefined

# getting test permissions data for a specified user
def get_test_permissions(request):
    # making sure that we have the needed parameters
    username_id = request.query_params.get("username_id", None)
    if is_undefined(username_id):
        return {"error": "no 'username_id' parameter"}
    try:
        data = list(
            TestPermissions.objects.filter(
                Q(username=User.objects.get(username=username_id))
            )
            .distinct()
            .values()
        )
        return data
    except User.DoesNotExist:
        return {"error": "the specified username does not exist"}

import React from "react";
import { shallow } from "enzyme";
import { unconnectedPassword as Password } from "../../../components/profile/Password";
import LOCALIZE from "../../../text_resources";

//TODO (fnormand): add more tests once the component will have more functionalities

describe("renders component content", () => {
  const wrapper = shallow(<Password />);

  it("renders page titles", () => {
    const title1 = <h2>{LOCALIZE.profile.password.newPassword.title}</h2>;
    const title2 = <h2>{LOCALIZE.profile.password.passwordRecovery.title}</h2>;
    expect(wrapper.containsMatchingElement(title1)).toEqual(true);
    expect(wrapper.containsMatchingElement(title2)).toEqual(true);
  });
});
